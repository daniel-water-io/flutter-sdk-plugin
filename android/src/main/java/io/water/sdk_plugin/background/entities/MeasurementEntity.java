package io.water.sdk_plugin.background.entities;

import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import org.json.JSONArray;

public class MeasurementEntity {
    static final boolean _ROUND_RESULT_TO_TEN = true;

    private final long timeStamp;
    private final int distance;
    private int std;
    private String bottleName;
    private final int amountInBottle;
    private final boolean isRefill;
    private int lastMeasurement;

    public MeasurementEntity(DeviceLog deviceLog) {
        timeStamp = deviceLog.getTimeStamp();
        distance = deviceLog.getMeasurement();
        std = deviceLog.getExtraData();
        amountInBottle = getCurrentMl(distance);
        isRefill = isRefill(amountInBottle);
    }

    private boolean isRefill(int currentMeasurement) {
        //load last measurement
        lastMeasurement = (int) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.INTEGER, "last_measurement");

        return currentMeasurement > lastMeasurement;
    }

    public static int getCurrentMl(int distance) {
        String measuresJson = (String) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.STRING, "measures");
        String distancesJson = (String) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.STRING, "distances");

        JSONArray measuresJsonArr;
        JSONArray distancesJsonArr;
        try {
            measuresJsonArr = new JSONArray(measuresJson);
            distancesJsonArr = new JSONArray(distancesJson);

            int[] measures = new int[measuresJsonArr.length()];
            double[] distances = new double[distancesJsonArr.length()];

            for (int i = 0; i < measuresJsonArr.length(); i++) {
                measures[i] = measuresJsonArr.getInt(i);
                distances[i] = distancesJsonArr.getDouble(i);
            }

            return calcBottleAmount(measures, distances, distance);
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public int getAmountInBottle() {
        return amountInBottle;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public boolean isRefill() {
        return isRefill;
    }

    public int getLastMeasurement() {
        return lastMeasurement;
    }

    //    public static int getDistanceFromML(int value) {
//        //TODO
//        return -1;
//    }

    private static int calcBottleAmount(int[] measures, double[] dists, int distance) {
        if (dists == null || measures == null) {
            return -1;
        }

        int result = 0;

        if (distance >= dists[dists.length - 1])
            return measures[measures.length - 1];

        if (distance <= dists[0]) return measures[0];

        for (int i = 0; i < dists.length - 1; i++) {
            if (distance >= dists[i] && distance < dists[i + 1]) {
                int measuresDiff = measures[i + 1] - measures[i];
                double distancesDiff = dists[i + 1] - dists[i];
                double distanceFromKnownValue = distance - dists[i];
                double temp = (distanceFromKnownValue * measuresDiff) / (distancesDiff);
                result = (int) (measures[i] + temp);

                break;
            }
        }

        if (_ROUND_RESULT_TO_TEN) {
            return (int) (Math.round(result / 10) * 10);
        }

        return result;
    }

}

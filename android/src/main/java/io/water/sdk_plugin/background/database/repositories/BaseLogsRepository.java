package io.water.sdk_plugin.background.database.repositories;

import android.content.Context;

import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.List;

import io.water.sdk_plugin.background.database.CapLogsDao;
import io.water.sdk_plugin.background.database.LogsDatabase;
import io.water.sdk_plugin.background.entities.DeviceLogDB;


public class BaseLogsRepository {

    private final CapLogsDao mLogsDao;
    private static BaseLogsRepository ourInstance = null;
    private final String LAST_ID_FLUTTER_UPDATED = "LAST_ID_FLUTTER_UPDATED";
    private final String LAST_CAP_STATUS = "LAST_CAP_STATUS";

    public synchronized static BaseLogsRepository getInstance(Context context) {
        if (ourInstance == null)
            ourInstance = new BaseLogsRepository(context);
        return ourInstance;
    }

    private BaseLogsRepository(Context context) {
        LogsDatabase database = LogsDatabase.getInstance(context);
        mLogsDao = database.logsDao();
    }

    public long[] insert(DeviceLogDB... deviceLogDBS) {
        return mLogsDao.insert(deviceLogDBS);
    }

    public void updateCapStatus(boolean isOpen) {
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.BOOLEAN, isOpen, LAST_CAP_STATUS);
    }

    public boolean isCapOpen() {
        Boolean isCapOpen = (Boolean) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.BOOLEAN, LAST_CAP_STATUS);
        if (isCapOpen == null)
            isCapOpen = false;

        return isCapOpen;
    }

    public void deleteAllLogs() {
        mLogsDao.deleteAllLogs();
    }

    public List<DeviceLogDB> getAllTypeLogsMissedList(String opCodeEvent, Long lastIDRead) {
        return mLogsDao.getAllTypeLogsMissedList(opCodeEvent, lastIDRead);
    }

    public Long getMaxIDMissedEvents(String opCodeEvent) {
        return mLogsDao.getLastIDMissedEvents(opCodeEvent);
    }

    public List<DeviceLogDB> getAllTypeLogsList(String opCodeEvent, Long startDate, Long endDate) {
        return mLogsDao.getAllTypeLogsList(opCodeEvent, startDate, endDate);
    }

    public List<DeviceLogDB> getAllLogsListByExtraData3(String opCodeEvent, Long startDate, Long endDate, String extraData3) {
        return mLogsDao.getAllLogsListByExtraData3(opCodeEvent, startDate, endDate, extraData3);
    }

    public void updateDeviceLogDB(DeviceLogDB deviceLogDB) {
        mLogsDao.updateDeviceLogDB(deviceLogDB);
    }

    public void updateIdFlutterRead(long lastIdRead) {
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.LONG, lastIdRead, LAST_ID_FLUTTER_UPDATED);
    }

    public Long getLastIdRead() {
        return (Long) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.LONG, LAST_ID_FLUTTER_UPDATED);
    }

//    public List<DeviceLogDB> getAllLogsList(Long startDate, Long endDate) {
//        return mLogsDao.getAllLogsList(startDate, endDate);
//    }
//
//    public List<DeviceLogDB> getAllLogsMissedList(Long startDate, Long endDate) {
//        return mLogsDao.getAllLogsMissedList(startDate, endDate);
//    }
//    public void delete(DeviceLogDB deviceLogDBS) {
//        mLogsDao.delete(deviceLogDBS);
//    }
//
//    public void deleteById(long id) {
//        mLogsDao.deleteById(id);
//    }
//    public List<DeviceLogDB> getAllLogsList() {
//        return mLogsDao.getAllLogsList();
//    }
//
//    public List<DeviceLogDB> getAllLogsMissedList() {
//        return mLogsDao.getAllLogsMissedList();
//    }
//
//    public List<DeviceLogDB> getAllTypeLogsMissedList(String opCodeEvent) {
//        return mLogsDao.getAllTypeLogsMissedList(opCodeEvent);
//    }
}

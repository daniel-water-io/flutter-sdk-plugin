package io.water.sdk_plugin.background.utilities;

import android.util.Log;

import com.water.water_io_sdk.storage.database.logs.sdk_messaging.LogsType;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.SDKLogsRepository;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.entities.SDKLog;

public class LifeCycleApp {

    private static LifeCycleApp ourInstance = null;
    private boolean isFlutterReadData = false;
    private final SDKLogsRepository logsRepository = new SDKLogsRepository();
    private String lastMethodName="NONE";

    public synchronized static LifeCycleApp getInstance() {
        if (ourInstance == null)
            ourInstance = new LifeCycleApp();
        return ourInstance;
    }

    private LifeCycleApp() {
    }

    public void updateFlutterObserve(boolean isFlutterReadData, String methodName) {
        //TODO  save in the storage log with method called, for example onStart
        this.isFlutterReadData = isFlutterReadData;
        this.lastMethodName=methodName;
        Log.v("LOSING_EVENTS", "updateFlutterObserve - " + isFlutterReadData + " , (" + methodName + ")");
        SDKLog sdkLog = new SDKLog(LogsType.SDK_FLOW, "LifeCycle", methodName, "IsFlutterReadData = "+isFlutterReadData, "", "");
        logsRepository.insert(sdkLog);
    }

    public String getLastMethodName() {
        return lastMethodName;
    }

    public boolean isFlutterReadData() {
        return isFlutterReadData;
    }

}

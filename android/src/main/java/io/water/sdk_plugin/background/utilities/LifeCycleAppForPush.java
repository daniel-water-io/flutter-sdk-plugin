package io.water.sdk_plugin.background.utilities;

import android.util.Log;

public class LifeCycleAppForPush {

    private static LifeCycleAppForPush ourInstance = null;
    private boolean isAppShow = false;

    public synchronized static LifeCycleAppForPush getInstance() {
        if (ourInstance == null)
            ourInstance = new LifeCycleAppForPush();
        return ourInstance;
    }

    private LifeCycleAppForPush() {
    }

    public void updateAppVisibility(boolean isAppShow) {
//        Log.v("LifeCycleAppForPush", "isAppShow " + isAppShow);
        // TODO save in the storage log with method called, for example onStart
        this.isAppShow = isAppShow;
    }

    public boolean isAppShow() {
        return isAppShow;
    }

}

package io.water.sdk_plugin.background.utilities;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.network.entities.requests.firebase.sdk_messaging.ErrorSDK;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.LogsType;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.SDKLogsRepository;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.entities.SDKLog;

import java.util.ArrayList;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.CapStateChannelObserver;

public class CapLoggerHandler {

    private static CapLoggerHandler ourInstance = null;
    private final CapStateChannelObserver capStateChannelObserver;
    private final LiveData observable;

    public synchronized static CapLoggerHandler getInstance(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData observableLogs, LiveData<Map.Entry<String, Integer>> liveDataBatteryPercent) {
        if (ourInstance == null) {
            ourInstance = new CapLoggerHandler(binaryMessenger, owner, observableLogs, liveDataBatteryPercent);
        }
        return ourInstance;
    }

    private CapLoggerHandler(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData liveData, LiveData liveDataBatteryPercent) {
        this.observable = liveData;
        if (binaryMessenger == null || owner == null || liveData == null) {
            Log.e("LOSING_EVENTS", "Ctor CapLoggerHandler - binaryMessenger is null? " + (binaryMessenger == null) + " , owner is null?" + (owner == null));
            SDKLog sdkLog = new SDKLog(LogsType.app_critical,"binaryMessenger is null?" + (binaryMessenger == null) + " , owner is null?" + (owner == null),  "Ctor CapLoggerHandler","Able losing logs", LifeCycleApp.getInstance().getLastMethodName());
            new SDKLogsRepository().insert(sdkLog);
            new ErrorSDK(sdkLog.toDashboardJson(), System.currentTimeMillis(), LogsType.app_critical.name().toLowerCase()).sendRequest();
        }
        capStateChannelObserver = new CapStateChannelObserver(binaryMessenger, owner, observable, liveDataBatteryPercent);
    }

    public synchronized void onChangeManually(ArrayList<DeviceLog> deviceLogs) {
        if (!LifeCycleApp.getInstance().isFlutterReadData()) {
            if (deviceLogs == null) {
                Log.e("LOSING_EVENTS", "onChangeManually - deviceLogs is null! , isFlutterReadData = " + LifeCycleApp.getInstance().isFlutterReadData() + ", events = " + deviceLogs.toString());
                SDKLog sdkLog = new SDKLog(LogsType.app_critical,  deviceLogs.toString(), "deviceLogs is null","Losing logs", "", LifeCycleApp.getInstance().getLastMethodName());
                new SDKLogsRepository().insert(sdkLog);
                new ErrorSDK(sdkLog.toDashboardJson(), System.currentTimeMillis(), LogsType.app_critical.name().toLowerCase()).sendRequest();
                return;
            }
            if (observable == null) {
                Log.e("LOSING_EVENTS", "onChangeManually - observable is null! , isFlutterReadData = " + LifeCycleApp.getInstance().isFlutterReadData() + ", events = " + deviceLogs.toString());
                SDKLog sdkLog = new SDKLog(LogsType.app_critical,  deviceLogs.toString(), "observable is null","Losing logs", "", LifeCycleApp.getInstance().getLastMethodName());
                new SDKLogsRepository().insert(sdkLog);
                new ErrorSDK(sdkLog.toDashboardJson(), System.currentTimeMillis(), LogsType.app_critical.name().toLowerCase()).sendRequest();
                return;
            }
            new Handler(Looper.getMainLooper()).post(() -> {
                if (deviceLogs.size() == 0) {
                    return;
                }
                SDKLog sdkLog = new SDKLog(LogsType.SDK_FLOW, "Read logs in minimized", "", LifeCycleApp.getInstance().getLastMethodName(), "", "Log size:" + deviceLogs.size());
                new SDKLogsRepository().insert(sdkLog);
                capStateChannelObserver.onChanged(deviceLogs);
            });
        }
    }

}

package io.water.sdk_plugin.background.database.repositories;

import android.util.Log;

import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.ble.utilities.CapConfig;
import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.network.utilities.SimpleAsyncTask;
import com.water.water_io_sdk.reminder_local_push.entities.Reminder;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.water.sdk_plugin.background.entities.DeviceLogDB;
import io.water.sdk_plugin.background.utilities.LifeCycleApp;


public class VitaminsRepository {
    private static final String TAG = VitaminsRepository.class.getSimpleName();

    public static final String PILL_TAKEN_KEY = "PILL_TAKEN";
    private final BaseLogsRepository mRepository;
    private static VitaminsRepository ourInstance = null;

    public synchronized static VitaminsRepository getInstance() {
        if (ourInstance == null)
            ourInstance = new VitaminsRepository();
        return ourInstance;
    }

    private VitaminsRepository() {
        mRepository = BaseLogsRepository.getInstance(ModuleApp.getContext());
    }

    private ArrayList<DeviceLogDB> getAllTypeLogsList(Long startDate, Long endDate) {
        return (ArrayList<DeviceLogDB>) mRepository.getAllTypeLogsList("C", startDate, endDate);
    }

    public Long getMaxIDMissedEvents(String opCodeEvent) {
        return mRepository.getMaxIDMissedEvents(opCodeEvent);
    }

    public ArrayList<DeviceLogDB> getAllPillTakenLogsList(Long startDate, Long endDate) {
        return (ArrayList<DeviceLogDB>) mRepository.getAllLogsListByExtraData3("C", startDate, endDate, PILL_TAKEN_KEY);
    }

    public ArrayList<DeviceLogDB> getAllMissedCloseLogsList() {
        Long lastIDUpdate = mRepository.getLastIdRead();
        if (lastIDUpdate == null)
            lastIDUpdate = -1L;

        String eventMissed = "C";
        Long maxID = mRepository.getMaxIDMissedEvents(eventMissed);
        if (maxID != null) {
            if (lastIDUpdate > maxID) {
                lastIDUpdate = maxID;
                mRepository.updateIdFlutterRead(lastIDUpdate);
            }
        }

        // Log.v("MissssSm","lastIDUpdate=> "+lastIDUpdate);

        ArrayList<DeviceLogDB> logsMissed = (ArrayList<DeviceLogDB>) mRepository.getAllTypeLogsMissedList(eventMissed, lastIDUpdate);
        if (logsMissed != null && logsMissed.size() != 0) {
            long lastIdEventRead = logsMissed.get(logsMissed.size() - 1).getId();
            mRepository.updateIdFlutterRead(lastIdEventRead);
        }
        return logsMissed;
    }

    public long insertDeviceLogDB(DeviceLogDB deviceLogDB) {
        return mRepository.insert(deviceLogDB)[0];
    }


    public void handleCloseAndPillTakenEventClient(DeviceLog log) {
        new SimpleAsyncTask(() -> handleCloseAndPillTakenEvent(log, mCallback)).execute();
    }

    public void handleReminderReport(DeviceLog log) {
        if (isFlutterReadData())
            return;

        new WIONetwork.Analytics().userLog(
                getBuilder("1",
                        log.getTimeStamp(),
                        "reminder",
                        "cap",
                        "Reminder")
        );
    }

    public void handlePreReminderReport(DeviceLog log) {
        if (isFlutterReadData())
            return;
        new WIONetwork.Analytics().userLog(
                getBuilder("1",
                        log.getTimeStamp(),
                        "pre_reminder",
                        "cap",
                        "Pre reminder")
        );
    }

    public void handleOpenReport(DeviceLog log) {
        mRepository.updateCapStatus(true);
        if (isFlutterReadData())
            return;

        new WIONetwork.Analytics().userLog(
                getBuilder("1",
                        log.getTimeStamp(),
                        "cap",
                        "cap",
                        "Opened Cap")
        );
    }

    private boolean isFlutterReadData() {
        return LifeCycleApp.getInstance().isFlutterReadData();
    }

    private void handleCloseAndPillTakenEvent(
            final DeviceLog log,
            LogicVitaminsCallback callback
    ) {
        mRepository.updateCapStatus(false);
        final boolean isFlutterRead = isFlutterReadData();
        final DeviceLogDB deviceLogDB = new DeviceLogDB(log, isFlutterRead);
        final long[] ids = mRepository.insert(deviceLogDB);
        deviceLogDB.setId(ids[0]);
        // for (int i = 0; i < ids.length; i++) {
        //     Log.v("MissssSm", "Insert isFlutterRead=> " + isFlutterRead + " ID=> " + ids[i]);
        // }

        if (isFlutterRead) {
            return;
        }

        new WIONetwork.Analytics().userLog(
                getBuilder("0",
                        log.getTimeStamp(),
                        "cap",
                        "cap",
                        "Closed Cap")
        );

        boolean isTaken = callback.isPillsTakenLogic(deviceLogDB.getTimeStamp());

        if (isTaken) {
            Log.v(TAG, "EVENT IS TAKEN!!!!!");
            callback.sentAnalytics(new WIONetwork.Analytics(), log);
            updatePillTaken(deviceLogDB);
        } else {
            Log.v(TAG, "EVENT IS DISCARDED!!!");
        }
    }

    private void updatePillTaken(DeviceLogDB deviceLogDB) {
        //update DB extara data3 with TAKEN
        deviceLogDB.setExtraData3(PILL_TAKEN_KEY);
        mRepository.updateDeviceLogDB(deviceLogDB);
    }

    private final LogicVitaminsCallback mCallback = new LogicVitaminsCallback() {
        @Override
        public boolean isPillsTakenLogic(long eventTimestamp) {

            return !shouldEventBeDiscarded(eventTimestamp);
        }

        @Override
        public void sentAnalytics(WIONetwork.Analytics analytics, DeviceLog log) {
            analytics.userLog(
                    getBuilder("1",
                            log.getTimeStamp(),
                            "pill_taken",
                            "cap",
                            "Pill taken")
            );
        }
    };


    private boolean shouldEventBeDiscarded(long eventTimestamp) {
        ArrayList<DeviceLogDB> eventDayEntries = getDailyPillTakenForDate(eventTimestamp);
        Reminder[] todayReminders = CapConfig.getInstance().reminderConfig().getReminders();

        if (eventDayEntries.size() == 0) {
            return false;
        }

        if (eventDayEntries.size() >= todayReminders.length) {
            return true;
        }

        ArrayList<Integer> intervalAnchors = new ArrayList<>();
        intervalAnchors.add(0); // adding 00:00 as first anchor

        for (int i = 0; i < todayReminders.length - 1; i++) {
            Reminder rem = todayReminders[i];
            Reminder nextRem = todayReminders[i + 1];

            int remMinsSM = rem.getHourAndMinutesAsSecond();
            int remsDiff = nextRem.getHourAndMinutesAsSecond() - remMinsSM;
            final int tweenAnchor = (remsDiff / 2) + remMinsSM;

            intervalAnchors.add(tweenAnchor);
        }

        intervalAnchors.add(23 * 60 + 59); // Adding midnight as last day anchor..

        final int eventMinsSinceMidnight = getMinsSinceMidnightFromTimestamp(eventTimestamp);
        int eventAnchorIndex = 0;

        for (int i = 0; i < intervalAnchors.size(); i++) {
            int anchor = intervalAnchors.get(i);

            if (eventMinsSinceMidnight < anchor) {
                eventAnchorIndex = i;
                break;
            }
        }

        boolean intakeForIntervalWasTakenAlready = false;

        for (int i = 0; i < eventDayEntries.size(); i++) {
            DeviceLogDB intake = eventDayEntries.get(i);
            int intakeMsM = getMinsSinceMidnightFromTimestamp(intake.getTimeStamp());

            if (intakeMsM > intervalAnchors.get(eventAnchorIndex - 1) &&
                    intakeMsM <= intervalAnchors.get(eventAnchorIndex)) {
                intakeForIntervalWasTakenAlready = true;
                break;
            }
        }

        return intakeForIntervalWasTakenAlready;
    }

    private int getMinsSinceMidnightFromTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        return calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
    }

    private ArrayList<DeviceLogDB> getDailyPillTakenForDate(long eventTimestamp) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(eventTimestamp);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long startDayTimestamp = calendar.getTimeInMillis();

        // next day
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        long endDayTimestamp = calendar.getTimeInMillis();


        return getAllPillTakenLogsList(startDayTimestamp, endDayTimestamp);
    }

    private AnalyticsEvent.Builder getBuilder(String data, long timeStamp, String type, String source, String text) {
        List<String> deviceMac = SharedPrefsModuleHelper.getInstance().getMacAddress();
        String deviceMacAddress = " ";
        if (deviceMac.size() > 0) {
            deviceMacAddress = deviceMac.get(0);
        }


        return new AnalyticsEvent.Builder()
                .setData(data)
                .setTime(timeStamp)
                .setType(type)
                .setSource(source)
                .setText(text)
                .setDeviceID(deviceMacAddress)
                .build();
    }

    public void handleResetReport(DeviceLog log) {
        if (isFlutterReadData())
            return;
        new WIONetwork.Analytics().userLog(
                getBuilder("0",
                        log.getTimeStamp(),
                        "cap",
                        "cap",
                        "Reset Cap")
        );
    }

    public void deleteAllLogs() {
        mRepository.deleteAllLogs();
    }

    interface LogicVitaminsCallback {
        boolean isPillsTakenLogic(long eventTimestamp);

        void sentAnalytics(WIONetwork.Analytics analytics, DeviceLog log);
    }

}

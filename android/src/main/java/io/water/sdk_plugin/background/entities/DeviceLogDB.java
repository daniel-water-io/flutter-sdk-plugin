package io.water.sdk_plugin.background.entities;

import com.water.water_io_sdk.ble.entities.DeviceLog;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import io.water.sdk_plugin.background.utilities.ConstantsDB;


@Entity(tableName = ConstantsDB.CAP_LOGS_TABLE)
public class DeviceLogDB extends DeviceLog {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private long timeStampInsertDB;
    private boolean isFlutterReadData;
    private String extraData3;


    public DeviceLogDB(){}

    public DeviceLogDB(DeviceLog log, boolean isFlutterReadData) {
        super(log.getEventType());
        setMeasurement(log.getMeasurement());
        setTimeStamp(log.getTimeStamp());
        setEventDesc(log.getEventDesc());
        setMacAddress(log.getMacAddress());
        setExtraData(log.getExtraData());
        setExtraData2(log.getExtraData2());
        this.timeStampInsertDB = System.currentTimeMillis();
        this.isFlutterReadData = isFlutterReadData;
    }

    public DeviceLogDB(String eventType, long timeStamp, String macAddress) {
        super(eventType, timeStamp, macAddress);
    }

    public DeviceLogDB(String event, long timeStamp, int measurement, int std) {
        super(event, timeStamp, measurement, std);
    }

    public DeviceLogDB(String event, long timeStamp, int someMeasurement, String desc) {
        super(event, timeStamp, someMeasurement, desc);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimeStampInsertDB() {
        return timeStampInsertDB;
    }

    public void setTimeStampInsertDB(long timeStampInsertDB) {
        this.timeStampInsertDB = timeStampInsertDB;
    }

    public boolean isFlutterReadData() {
        return isFlutterReadData;
    }

    public void setFlutterReadData(boolean flutterReadData) {
        isFlutterReadData = flutterReadData;
    }

    public String getExtraData3() {
        return extraData3;
    }

    public void setExtraData3(String extraData3) {
        this.extraData3 = extraData3;
    }

    public DeviceLogDB(String event, long timeStamp) {
        super(event, timeStamp);
    }

    public DeviceLogDB(String event, long timeStamp, boolean isFlutterReadData) {
        super(event, timeStamp);
        this.isFlutterReadData = isFlutterReadData;
    }

    public DeviceLogDB(String event) {
        super(event);
    }
}

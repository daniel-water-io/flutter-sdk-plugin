package io.water.sdk_plugin.background.database;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;

import io.water.sdk_plugin.background.entities.DeviceLogDB;
import io.water.sdk_plugin.background.utilities.ConstantsDB;

import static io.water.sdk_plugin.background.utilities.ConstantsDB.CAP_LOGS_TABLE;


@Database(entities = {DeviceLogDB.class}, version = ConstantsDB.DATABASE_LOG_VERSION, exportSchema = false)
public abstract class LogsDatabase extends RoomDatabase {
    private static final String TAG = LogsDatabase.class.getSimpleName();

    private static LogsDatabase sInstance;

    private static final Migration MIGRATION_4_5_NEW_FIELDS = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE " + CAP_LOGS_TABLE
                    + " ADD COLUMN mRAWData BLOB DEFAULT NULL");
        }
    };

    public abstract CapLogsDao logsDao();

    public static synchronized LogsDatabase getInstance(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(),
                    LogsDatabase.class, ConstantsDB.DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .addCallback(roomCallback)
                    .addMigrations(MIGRATION_4_5_NEW_FIELDS)
                    .build();
        }
        return sInstance;
    }


    private static final Callback roomCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }

    };
}

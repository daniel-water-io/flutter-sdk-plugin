package io.water.sdk_plugin.background.database;


import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import io.water.sdk_plugin.background.entities.DeviceLogDB;

@Dao
public interface CapLogsDao {

    @Insert
    long[] insert(DeviceLogDB... deviceLogDB);

    @Delete
    void delete(DeviceLogDB deviceLogDB);

    @Query("DELETE FROM cap_logs_table WHERE id =:idLogs")
    void deleteById(long idLogs);

    @Update()
    void updateDeviceLogDB(DeviceLogDB deviceLogDB);

    @Query("DELETE from cap_logs_table")
    void deleteAllLogs();

    @Query("SELECT * FROM cap_logs_table ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllLogsList();

    @Query("SELECT * FROM cap_logs_table WHERE isFlutterReadData = 0 ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllLogsMissedList();

    @Query("SELECT * FROM cap_logs_table WHERE isFlutterReadData = 0 AND opCode =:opCodeEvent ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllTypeLogsMissedList(String opCodeEvent);

    @Query("SELECT * FROM cap_logs_table WHERE mTimeStamp>:startDate AND mTimeStamp<=:endDate ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllLogsList(Long startDate, Long endDate);

    @Query("SELECT * FROM cap_logs_table WHERE isFlutterReadData = 0 AND mTimeStamp>:startDate AND mTimeStamp<=:endDate ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllLogsMissedList(Long startDate, Long endDate);

    @Query("SELECT * FROM cap_logs_table WHERE isFlutterReadData = 0 AND opCode =:opCodeEvent AND id>:lastIdRead ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllTypeLogsMissedList(String opCodeEvent, Long lastIdRead);

    @Query("SELECT MAX(id) FROM cap_logs_table WHERE isFlutterReadData = 0 AND opCode =:opCodeEvent")
    Long getLastIDMissedEvents(String opCodeEvent);

    @Query("SELECT * FROM cap_logs_table WHERE opCode =:opCodeEvent AND mTimeStamp>:startDate AND mTimeStamp<=:endDate ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllTypeLogsList(String opCodeEvent, Long startDate, Long endDate);

    @Query("SELECT * FROM cap_logs_table WHERE opCode =:opCodeEvent AND extraData3 =:extraData3 AND mTimeStamp>:startDate AND mTimeStamp<=:endDate ORDER BY mTimeStamp ASC")
    List<DeviceLogDB> getAllLogsListByExtraData3(String opCodeEvent, Long startDate, Long endDate, String extraData3);

}

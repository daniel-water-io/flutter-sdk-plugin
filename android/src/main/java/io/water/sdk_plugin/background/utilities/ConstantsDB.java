package io.water.sdk_plugin.background.utilities;

public class ConstantsDB {
    public final static String CAP_LOGS_TABLE = "cap_logs_table";
    public final static String DATABASE_NAME = "cap_logs_database";
    public final static int DATABASE_LOG_VERSION = 6;
}

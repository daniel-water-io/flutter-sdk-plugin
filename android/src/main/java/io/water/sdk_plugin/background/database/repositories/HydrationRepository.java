package io.water.sdk_plugin.background.database.repositories;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;

import io.water.sdk_plugin.background.entities.DeviceLogDB;
import io.water.sdk_plugin.background.entities.MeasurementEntity;
import io.water.sdk_plugin.background.utilities.LifeCycleApp;

public class HydrationRepository {

    private static final String TAG = HydrationRepository.class.getSimpleName();

    private static final String MEASUREMENT_LOG_EVENT = "L";
    private final BaseLogsRepository mRepository;
    private static HydrationRepository ourInstance = null;

    public synchronized static HydrationRepository getInstance() {
        if (ourInstance == null)
            ourInstance = new HydrationRepository();
        return ourInstance;
    }

    private HydrationRepository() {
        mRepository = BaseLogsRepository.getInstance(ModuleApp.getContext());
    }

    private ArrayList<DeviceLogDB> getAllMeasurementLogsList(Long startDate, Long endDate) {
        return (ArrayList<DeviceLogDB>) mRepository.getAllTypeLogsList(MEASUREMENT_LOG_EVENT, startDate, endDate);
    }

    public ArrayList<DeviceLogDB> getAllHydrationLogsList() {
        Long lastIDUpdate = mRepository.getLastIdRead();
        if (lastIDUpdate == null)
            lastIDUpdate = -1L;
        // Log.v("MissssSm","lastIDUpdate=> "+lastIDUpdate);


        Long maxID = mRepository.getMaxIDMissedEvents(MEASUREMENT_LOG_EVENT);
        if (maxID != null) {
            if (lastIDUpdate > maxID) {
                lastIDUpdate = maxID;
                mRepository.updateIdFlutterRead(lastIDUpdate);
            }
        }


        ArrayList<DeviceLogDB> logsMissed = (ArrayList<DeviceLogDB>) mRepository.getAllTypeLogsMissedList(MEASUREMENT_LOG_EVENT, lastIDUpdate);
        if (logsMissed != null && logsMissed.size() != 0) {
            long lastIdEventRead = logsMissed.get(logsMissed.size() - 1).getId();
            mRepository.updateIdFlutterRead(lastIdEventRead);
        }
        return logsMissed;
    }

    public long insertDeviceLogDB(DeviceLogDB deviceLogDB) {
        return mRepository.insert(deviceLogDB)[0];
    }


    public void handleReminderReport(DeviceLog log) {
        if (isFlutterReadData())
            return;

        new WIONetwork.Analytics().userLog(
                getBuilder("1",
                        log.getTimeStamp(),
                        "reminder",
                        "cap",
                        "Reminder")
        );
    }

    public void handlePreReminderReport(DeviceLog log) {
        if (isFlutterReadData())
            return;
        new WIONetwork.Analytics().userLog(
                getBuilder("1",
                        log.getTimeStamp(),
                        "pre_reminder",
                        "cap",
                        "Pre reminder")
        );
    }

    public void handleOpenReport(DeviceLog log) {
        mRepository.updateCapStatus(true);
        if (isFlutterReadData())
            return;

        new WIONetwork.Analytics().userLog(
                getBuilder("1",
                        log.getTimeStamp(),
                        "cap",
                        "cap",
                        "Opened Cap")
        );
    }

    public void handleMeasurementReport(DeviceLog log) {
        mRepository.updateCapStatus(false);
        MeasurementEntity measurementEntity = new MeasurementEntity(log);
        updateCurrentMeasurement(measurementEntity);
        mRepository.insert(new DeviceLogDB(MEASUREMENT_LOG_EVENT,log.getTimeStamp(),log.getMeasurement(),log.getExtraData()));
        if (isFlutterReadData())
            return;

        int bottleAmount = measurementEntity.getAmountInBottle();
        long timeStamp = measurementEntity.getTimeStamp();

        new WIONetwork.Analytics().userLog(
                getBuilder(bottleAmount + "",
                        timeStamp,
                        "measurement",
                        "cap",
                        "Measure " + bottleAmount + "ml, std: " + log.getExtraData())
        );

        if (measurementEntity.isRefill()) {
            int filledAmount = bottleAmount - measurementEntity.getLastMeasurement();
            reportUserLog("{" +
                    "\"additional_data\":\"Water\"," +
                    "\"event_data\":" + bottleAmount + "," +
                    "\"event_time\":" + timeStamp + "," +
                    "\"event_type\":\"measurement\"," +
                    "\"event_source\":\"cap\"," +
                    "\"event_text\":\"Filled " + filledAmount + "ml, Water\"" +
                    "}");
        } else {
            int hydratedAmount = measurementEntity.getLastMeasurement() - bottleAmount;
            reportUserLog("{" +
                    "\"additional_data\":\"Water\"," +
                    "\"event_data\":" + bottleAmount + "," +
                    "\"event_time\":" + timeStamp + "," +
                    "\"event_type\":\"measurement\"," +
                    "\"event_source\":\"cap\"," +
                    "\"event_text\":\"Hydrated " + hydratedAmount + "ml, Water\"" +
                    "}");

            AnalyticsEvent.Builder analytics = new AnalyticsEvent.Builder()
                    .setTime(timeStamp)
                    .setAmount(hydratedAmount)
                    .setSource("cap")
                    .setLevelStart(measurementEntity.getLastMeasurement())
                    .setLevelEnd(hydratedAmount)
                    .setTimeSpan(0)
                    .setUsageType("water")
                    .build();

            new WIONetwork.Analytics().userHydration(analytics);
        }
    }

    private void updateCurrentMeasurement(MeasurementEntity measurementEntity) {
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.INTEGER, measurementEntity.getAmountInBottle(), "last_measurement");
    }

    private boolean isFlutterReadData() {
        return true;
//        return LifeCycleApp.getInstance().isFlutterReadData();
    }

    public void handleCloseEvent(final DeviceLog log) {
        mRepository.updateCapStatus(false);
        final boolean isFlutterRead = isFlutterReadData();
        final DeviceLogDB deviceLogDB = new DeviceLogDB(log, isFlutterRead);
        final long[] ids = mRepository.insert(deviceLogDB);
        deviceLogDB.setId(ids[0]);
        if (isFlutterRead) {
            return;
        }
        new WIONetwork.Analytics().userLog(
                getBuilder("0",
                        log.getTimeStamp(),
                        "cap",
                        "cap",
                        "Closed Cap")
        );
    }


    private AnalyticsEvent.Builder getBuilder(String data, long timeStamp, String type, String source, String text) {
        return new AnalyticsEvent.Builder()
                .setData(data)
                .setTime(timeStamp)
                .setType(type)
                .setSource(source)
                .setText(text)
                .build();
    }

    public void handleResetReport(DeviceLog log) {
        if (isFlutterReadData())
            return;

        new WIONetwork.Analytics().userLog(
                getBuilder("0",
                        log.getTimeStamp(),
                        "cap",
                        "cap",
                        "Reset Cap")
        );
    }

    public void deleteAllLogs() {
        mRepository.deleteAllLogs();
    }

    private void reportUserLog(String jsonStr) {
        JsonObject gsonObject = (JsonObject) new JsonParser().parse(jsonStr);
        new WIONetwork.Analytics().userLog(gsonObject);
    }



}

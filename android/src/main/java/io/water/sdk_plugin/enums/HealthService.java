package io.water.sdk_plugin.enums;

public enum HealthService {
    Strava, Garmin, Apple_health, Google, Fitbit, Polar, Suunto
}

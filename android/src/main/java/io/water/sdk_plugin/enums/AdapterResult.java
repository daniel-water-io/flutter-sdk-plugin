package io.water.sdk_plugin.enums;

public enum AdapterResult {
    MISSING_GPS,
    MISSING_BLUETOOTH,
    SUCCESS,
}

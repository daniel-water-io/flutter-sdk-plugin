package io.water.sdk_plugin.interfaces;

public interface ILogout {
    void onLogoutDone();
}

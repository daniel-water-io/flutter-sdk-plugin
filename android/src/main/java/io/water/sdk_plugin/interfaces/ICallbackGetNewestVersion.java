package io.water.sdk_plugin.interfaces;


public interface ICallbackGetNewestVersion {
    void onError(String desc);

    void onSuccess(String newVersion,String priority,String description);
}

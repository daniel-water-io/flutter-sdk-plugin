package io.water.sdk_plugin.models;

public class CapConfigModel {
    String key;
    Object data;
    Class<?> aClass;

    public CapConfigModel(String key, Object data, Class<?> aClass ) {
        this.key = key;
        this.data = data;
        this.aClass = aClass;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Class<?> getaClass() {
        return aClass;
    }

    public void setaClass(Class<?> aClass) {
        this.aClass = aClass;
    }


    @Override
    public String toString() {
        return "CapConfigModel{" +
                "key='" + key + '\'' +
                ", data=" + data +
                '}';
    }
}

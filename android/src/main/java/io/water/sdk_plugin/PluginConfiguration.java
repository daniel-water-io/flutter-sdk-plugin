package io.water.sdk_plugin;

import com.water.water_io_sdk.ble.enums.AppMode;

import java.util.Map;

public class PluginConfiguration {
    public enum AppGenes {Hydration, Vitamins}

    public AppGenes appGenes;
    public boolean includePushNotifications;
    public boolean includeLocalReminders;
    public AppMode appMode;

    public PluginConfiguration(Map<String, Object> map) {
        appGenes = AppGenes.valueOf((String) map.get("appGenes"));
        appMode =  AppMode.valueOf((String)map.get("appMode"));
        includePushNotifications = (boolean) map.get("includePushNotifications");
        includeLocalReminders = (boolean) map.get("includeLocalReminders");
    }

}

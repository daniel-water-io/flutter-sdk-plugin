package io.water.sdk_plugin.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.water.water_io_sdk.ble.enums.AppMode;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.engine.channels.callbacks.PermissionsCallbackChannel;
import io.water.sdk_plugin.utils.NotificationUtils;

public class PluginPermissionsActivity extends FlutterActivity {

    private PermissionsCallbackChannel mPermissionsCallbackChannel;
    public static final int OPEN_SETTINGS_INTENT_CODE = 120;
    private int mDeniedChunckedPermsReqId = -1;
    private MethodChannel.Result resultFromSettings;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        mPermissionsCallbackChannel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean shouldAskForChunkPermissions(int reqId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {

                boolean isNeedRunningBackground = SharedPrefsModuleHelper.getInstance().getAppMode()
                        == AppMode.FOREGROUND_AND_BACKGROUND;

                if (isNeedRunningBackground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    requestPermissions(
                            new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                            },
                            reqId);
                } else {
                    requestPermissions(
                            new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                            },
                            reqId);
                }

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (resultFromSettings != null) {
            mPermissionsCallbackChannel.handleCheckIfHasAllPermissionToScan(resultFromSettings);
            resultFromSettings = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPermissionsCallbackChannel.onActivityResult(requestCode, resultCode, data);
    }

    public void setPermissionsCallbackChannel(PermissionsCallbackChannel permissionsCallbackChannel) {
        mPermissionsCallbackChannel = permissionsCallbackChannel;
        mPermissionsCallbackChannel.setActivity(this);
    }

//    public void askForDeniedChunkPermissions(int randReqId) {
//        if (mDeniedChunckedPermsReqId == -1) {
//            mDeniedChunckedPermsReqId = randReqId;
//
//            openSettingsActivity(null);
//        } else {
//            Log.e("PERM_ACTIVITY", "ALREADY ASKING FOR DENIED PERMS!");
//        }
//    }

    public void openSettingsActivity(MethodChannel.Result result) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        resultFromSettings = result;
        startActivityForResult(intent, OPEN_SETTINGS_INTENT_CODE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDeniedChunckedPermsReqId != -1) {
            mPermissionsCallbackChannel.onDeniedPermissionsResult(mDeniedChunckedPermsReqId, true);
            mDeniedChunckedPermsReqId = -1;
        }
    }

    public boolean checkIsNotificationPermissionMissedAndroid13() {
       return NotificationUtils.checkIsNotificationPermissionMissedAndroid13(this);
    }

    public boolean shouldAskForBluetoothAndroid12Permissions(int randReqId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (checkSelfPermission(Manifest.permission.BLUETOOTH_SCAN)
                    == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT)
                            == PackageManager.PERMISSION_DENIED) {
                requestPermissions(
                        new String[]{
                                Manifest.permission.BLUETOOTH_SCAN,
                                Manifest.permission.BLUETOOTH_CONNECT,
                        },
                        randReqId);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public void requestNotificationPermissionAndroid13(int randReqId) {
        NotificationUtils.requestNotificationPermissionAndroid13(this,randReqId);
    }
}

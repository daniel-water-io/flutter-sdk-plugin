package io.water.sdk_plugin.utils;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import io.flutter.Log;
import io.flutter.embedding.android.FlutterActivity;

public class NotificationUtils {

    private static final String NOTIFICATION_ID_KEY = "NOTIFICATION_ID";
    private int idNotification;
    private final String PUSH_REMOTE_CHANNEL;
    private final String DESCRIPTION_PUSH;

    public NotificationUtils(String PUSH_REMOTE_CHANNEL, String DESCRIPTION_PUSH) {
        Integer idNotification = (Integer) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.INTEGER, NOTIFICATION_ID_KEY);

        if (idNotification == null) {
            idNotification = 0;
        }

        this.idNotification = ++idNotification % 1000;
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.INTEGER, this.idNotification, NOTIFICATION_ID_KEY);

        this.PUSH_REMOTE_CHANNEL = PUSH_REMOTE_CHANNEL;
        this.DESCRIPTION_PUSH = DESCRIPTION_PUSH;
    }

    public void requestNotificationPermissionAndroid12(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = new WIONetwork.Settings().getAppType();
            NotificationChannel channel = new NotificationChannel(channelId, PUSH_REMOTE_CHANNEL, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(DESCRIPTION_PUSH);

            notificationManager.createNotificationChannel(channel);
        }
    }

    public void showNotificationID(String title, String body, Context applicationContext, int id) {
        this.idNotification = id;
        showNotification(title, body, applicationContext);
    }

    public PendingIntent createPendingIntentGetActivity(Context context, int id, Intent intent, int flag) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            return PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_IMMUTABLE | flag);
        } else {
            return PendingIntent.getActivity(context, id, intent, flag);
        }
    }

    public void showNotification(String title, String body, Context applicationContext) {
        Intent intent = new Intent(applicationContext, getFlutterMainActivity(applicationContext));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = createPendingIntentGetActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        final int resourceId = getAppLogoResId(applicationContext, "ic_launcher");
        int resourceTransparentId = getAppLogoResId(applicationContext, "ic_launcher_transparent");
        if (resourceTransparentId == 0) {
            resourceTransparentId = resourceId;
        }

        String channelId = new WIONetwork.Settings().getAppType();
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(applicationContext, channelId)
                        .setSmallIcon(resourceTransparentId)
                        .setLargeIcon(BitmapFactory.decodeResource(applicationContext.getResources(), resourceId))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(body))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, PUSH_REMOTE_CHANNEL, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(DESCRIPTION_PUSH);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(idNotification, notificationBuilder.build());
    }

    public void showReminderNotification(String title, String body, Context applicationContext) {
        Intent intent = new Intent(applicationContext, getFlutterMainActivity(applicationContext));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = createPendingIntentGetActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        final int resourceId = getAppLogoResId(applicationContext, "ic_launcher");
        int resourceTransparentId = getAppLogoResId(applicationContext, "ic_launcher_transparent");
        if (resourceTransparentId == 0) {
            resourceTransparentId = resourceId;
        }

        String channelId = new WIONetwork.Settings().getAppType();
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(applicationContext, channelId)
                        .setSmallIcon(resourceTransparentId)
                        .setLargeIcon(BitmapFactory.decodeResource(applicationContext.getResources(), resourceId))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(body))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, PUSH_REMOTE_CHANNEL, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(DESCRIPTION_PUSH);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(950, notificationBuilder.build());
    }

    private Class<? extends FlutterActivity> getFlutterMainActivity(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PLUGIN_CONFIG_DATA", Context.MODE_PRIVATE);
        String activityClassName = sharedPreferences.getString("MAIN_ACTIVITY_CLASS_NAME", null);

        try {
            Class<? extends FlutterActivity> mainActivityClass = (Class<? extends FlutterActivity>) Class.forName(activityClassName);
            return mainActivityClass;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private int getAppLogoResId(Context context, String imageName) {
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(imageName, "mipmap",
                SharedPrefsModuleHelper.getInstance().getPackageNameClient());
        return resourceId;
    }

    public static void requestNotificationPermissionAndroid13(FlutterActivity activity, int randReqId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.POST_NOTIFICATIONS)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted, so request it from the user.
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.POST_NOTIFICATIONS},
                        randReqId);
            } else {
                // Permission is already granted. You can show notifications here.
            }
        }
    }

    public static boolean checkIsNotificationPermissionMissedAndroid13(FlutterActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int permissionResult = ContextCompat.checkSelfPermission(activity, Manifest.permission.POST_NOTIFICATIONS);
            return permissionResult != PackageManager.PERMISSION_GRANTED;
        } else {
            return false;
        }
    }

}

package io.water.sdk_plugin.utils;

public class Utilities {
    public static boolean isOldVersion(String version) {
        String baseVersions = "01.08.0046V";
        for (int i = 7; i <= 10; i++) {
            String oldVersion=baseVersions;
            if (i< 10)
                oldVersion += "0";
            if ((oldVersion + i).equals(version))
                return true;
        }
        return false;
    }
}

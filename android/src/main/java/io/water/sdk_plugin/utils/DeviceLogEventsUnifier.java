package io.water.sdk_plugin.utils;

import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.ble.entities.WIOSmartDevice;

import java.util.ArrayList;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

public class DeviceLogEventsUnifier {
    private final LifecycleOwner mOwner;
    private final WIOSmartDevice mWIOSmartDevice;

    public MutableLiveData<ArrayList<DeviceLog>> mUnifiedLiveData = new MutableLiveData<>();

    public DeviceLogEventsUnifier(LifecycleOwner owner, WIOSmartDevice WIOSmartDevice) {
        this.mOwner = owner;
        this.mWIOSmartDevice = WIOSmartDevice;

        initObservers();
    }

    private void initObservers() {
        mWIOSmartDevice.getStatusDeviceLiveData().observe(mOwner, new Observer<DeviceLog>() {
            @Override
            public void onChanged(DeviceLog deviceLog) {
                if (deviceLog.getExtraData2() != null) {   // is that an event from the advertisment..?
                    updateChannel(new ArrayList<DeviceLog>() {
                        {
                            add(deviceLog);
                        }
                    });
                }
            }
        });

        //This observer is used just like the Impl class, reading all logs as is from the device..
        mWIOSmartDevice.getLogsDeviceLiveData().observe(mOwner, entry -> {
            ArrayList<DeviceLog> deviceLogs = entry.getValue();
            if (deviceLogs.size() == 0)
                return;
            updateChannel(deviceLogs);
        });
    }

    synchronized public void updateChannel(ArrayList<DeviceLog> deviceLogs) {
        mUnifiedLiveData.setValue(deviceLogs);
    }
}

package io.water.sdk_plugin.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.water.water_io_sdk.network.interfaces.OnResult;
import com.water.water_io_sdk.network.networks.WIONetwork;

import io.water.sdk_plugin.interfaces.ICallbackGetNewestVersion;


public class NetworkAPIsOTA {

    public NetworkAPIsOTA() {

    }

    public void getCapVersionsSupported(ICallbackGetNewestVersion iNetworkCallback) {
        new WIONetwork.Api().getCapVersionsSupportedNewest(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String message, int code) {
                final String NO_AVAILABLE_PROD_VERSION = "No available prod version";
                final String NO_AVAILABLE_VERSION = "No available version";

                final String AVAILABLE_VERSION_KEY = "available_versions";
                final String BODY_KEY = "body";
                final String ID_KEY = "id";
                final String IS_NEWEST_KEY = "is_newest";
                final String IS_PROD_KEY = "is_prod";
                final String VERSION_KEY = "version";
                final String LINK_KEY = "link";
                final String FILE_EXTENSION_KEY = "file_extension";
                final String PRIORITY_KEY = "priority";
                final String DESCRIPTION_KEY = "description_to_app";

                if (code != 200) {
                    iNetworkCallback.onError("code response" + " " + code);
                    return;
                }
                JsonArray jsonArray = jsonObject.get(BODY_KEY).getAsJsonObject().get(AVAILABLE_VERSION_KEY).getAsJsonArray();
                if (jsonArray == null || jsonArray.size() == 0) {
                    iNetworkCallback.onError(NO_AVAILABLE_VERSION);
                    return;
                }

                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject object = jsonArray.get(i).getAsJsonObject();
                    int id = object.get(ID_KEY).getAsInt();
                    boolean isNewest = object.get(IS_NEWEST_KEY).getAsBoolean();
                    boolean isProd = object.get(IS_PROD_KEY).getAsBoolean();
                    String version = object.get(VERSION_KEY).getAsString();
                    String link = object.get(LINK_KEY).getAsString();
                    String fileType = object.get(FILE_EXTENSION_KEY).getAsString();
                    String priority = object.get(PRIORITY_KEY).getAsString();
                    String description = "";
                    if (object.get(DESCRIPTION_KEY) != null) {
                        description = object.get(DESCRIPTION_KEY).getAsString();
                    }
                    if (isProd && isNewest) {
                        iNetworkCallback.onSuccess(version, priority, description);
                        return;
                    }
                }
                iNetworkCallback.onError(NO_AVAILABLE_PROD_VERSION);
            }

            @Override
            public void onError(String errorMessage, int errorCode) {
                final String failRequestDesc = "fail get version request, code response";
                iNetworkCallback.onError(failRequestDesc + " " + errorCode);
            }
        });
    }

}

package io.water.sdk_plugin.utils;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.water.water_io_sdk.push.entities.WIOMessage;

public class PushCallback {

    private static PushCallback instance;
    private final MutableLiveData<WIOMessage> messageMutableLiveData = new MutableLiveData<>();

    public synchronized static PushCallback getInstance() {
        if (instance == null)
            instance = new PushCallback();
        return instance;
    }

    private PushCallback() {

    }

    public void setWIOMessageResult(WIOMessage wioMessage) {
        this.messageMutableLiveData.postValue(wioMessage);
    }

    public LiveData<WIOMessage> getMessageLiveData() {
        return messageMutableLiveData;
    }
}

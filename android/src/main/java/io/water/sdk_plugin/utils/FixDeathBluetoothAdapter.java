//package io.water.sdk_plugin.utils;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.bluetooth.BluetoothAdapter;
//import android.os.Handler;
//import io.water.sdk_plugin.R;
//
//public class FixDeathBluetoothAdapter {
//    private final Activity activity;
//    private ProgressDialog mProgressDialog;
//
//    public FixDeathBluetoothAdapter(Activity activity) {
//        this.activity = activity;
//    }
//
//    private void showCircleProgressbar(String title) {
//        mProgressDialog = new ProgressDialog(activity);
//        mProgressDialog.setTitle(title);
//        mProgressDialog.setMessage(activity.getText(R.string.waiting));
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.show();
//    }
//
//    private void closeDialog() {
//        if (mProgressDialog == null)
//            return;
//        mProgressDialog.dismiss();
//        mProgressDialog = null;
//    }
//
//    public void initBluetoothAdapter(boolean isDelayTimeToCloseDialog) {
//        showCircleProgressbar(activity.getResources().getString(R.string.initialize_bluetooth_adapter));
//        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (mBluetoothAdapter.isEnabled()) {
//            mBluetoothAdapter.disable();
//        }
//
//        new Handler().postDelayed(() -> {
//            mBluetoothAdapter.enable();
//            if (isDelayTimeToCloseDialog)
//                closeDialogWithDelay();
//            else
//                closeDialog();
//        }, 1500);
//    }
//
//    private void closeDialogWithDelay() {
//        new Handler().postDelayed(this::closeDialog, 13000);
//    }
//
//}

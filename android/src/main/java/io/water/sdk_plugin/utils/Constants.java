package io.water.sdk_plugin.utils;

public class Constants {
    public static final String IS_OLD_VERSIONS_CAP = "IS_OLD_VERSIONS_CAP";
    public static final String HOLD_ON_KEY = "HOLD_ON_KEY";
}

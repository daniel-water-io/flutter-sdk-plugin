package io.water.sdk_plugin.utils;

import android.content.Context;
import android.content.Intent;

import com.water.water_io_sdk.application.ModuleApp;

public class NetworkCallbackDeepLink {

    private static NetworkCallbackDeepLink instance;
    private CallbackDeepLink mListener;


    public synchronized static NetworkCallbackDeepLink getInstance() {
        if (instance == null)
            instance = new NetworkCallbackDeepLink();
        return instance;
    }

    private NetworkCallbackDeepLink() {

    }

    public void setObjectResult(Intent intentResult) {
        if (mListener != null)
            mListener.onDeepLinkResult(intentResult);
    }

    public void setListener(CallbackDeepLink listener) {
        this.mListener = listener;

    }
    public void fcmRegister(Context applicationContext){
        ModuleApp.handleFCM(applicationContext);
    }

    public interface CallbackDeepLink {
        void onDeepLinkResult(Intent intentResult);
    }

}

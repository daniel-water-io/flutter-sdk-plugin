package io.water.sdk_plugin.utils;


import com.google.gson.JsonObject;
import com.water.water_io_sdk.network.entities.requests.warer_io.authorized.GetLeaderboard;
import com.water.water_io_sdk.network.interfaces.OnResult;
import com.water.water_io_sdk.network.networks.WIONetwork;

import java.util.Map;

import io.flutter.Log;
import io.flutter.plugin.common.MethodChannel;

public class LeaderboardModelManager {

    public LeaderboardModelManager(Map<String, Object> map, MethodChannel.Result result) {
        String time = map.get("time").toString();
        String gender = map.get("gender").toString();
        String ages = map.get("ages").toString();
        String groupId = map.get("group_id").toString();

        GetLeaderboard.LeaderboardParams params;
        try {
            params = new GetLeaderboard.LeaderboardParams(
                    GetLeaderboard.LeaderboardParams.Time.valueOf(time),
                    GetLeaderboard.LeaderboardParams.Gender.valueOf(gender),
                    GetLeaderboard.LeaderboardParams.Ages.valueOf(ages),
                    groupId
            );
        } catch (Exception exception) {
            exception.getStackTrace();
            params = new GetLeaderboard.LeaderboardParams(
                    GetLeaderboard.LeaderboardParams.Time.TODAY,
                    GetLeaderboard.LeaderboardParams.Gender.ALL,
                    GetLeaderboard.LeaderboardParams.Ages.ALL,
                    null
            );
        }

        new WIONetwork.Api().leaderboard(params, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                Log.v("fasfa", "onSuccess");
                android.util.Log.v("fasfa", "onSuccess");
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                Log.v("fasfa", "onError");
                android.util.Log.v("fasfa", "onError");
                result.error("15", s, i);
            }
        });
    }
}

package io.water.sdk_plugin.engine.channels.observers;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_STATE_BATTREY_STATE_METHOD;;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import java.util.Map;

import io.flutter.Log;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;

public class BatteryStatusChannelObserver extends ChannelObserver<Map.Entry<String, Integer>> {

    public BatteryStatusChannelObserver(MethodChannel methodChannel,  LifecycleOwner owner, LiveData<Map.Entry<String, Integer>> observable) {
        super(methodChannel, CAP_STATE_BATTREY_STATE_METHOD, owner, observable);
    }

    @Override
    public void onChanged(Map.Entry<String, Integer> entry) {
        String macAddress = entry.getKey();
        int batteryPercent = entry.getValue();
//        Log.v("CAP_BATTERY","batteryPercent = "+batteryPercent+"%");
        invokeTarget(batteryPercent);
    }
}


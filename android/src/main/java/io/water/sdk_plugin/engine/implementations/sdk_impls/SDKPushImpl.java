package io.water.sdk_plugin.engine.implementations.sdk_impls;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.push.entities.WIOMessage;
import com.water.water_io_sdk.push.entities.WIOServerPushImpl;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import io.water.sdk_plugin.background.utilities.LifeCycleAppForPush;
import io.water.sdk_plugin.engine.channels.callbacks.NetworkCallbackChannel;
import io.water.sdk_plugin.utils.NotificationUtils;
import io.water.sdk_plugin.utils.PushCallback;

public class SDKPushImpl extends WIOServerPushImpl {

    private static final String PUSH_REMOTE_CHANNEL = "Push remote";
    private static final String DESCRIPTION_PUSH = "Message from the server";

    private static final String FEED_PUSH_REMOTE_CHANNEL = "Feed push update";
    private static final String DESCRIPTION_FEED_PUSH = "Feed update";

    private static final String REMINDER_PUSH_REMOTE_CHANNEL = "Reminder push remote";
    private static final String DESCRIPTION_REMINDER_PUSH = "Reminder to hydrated";

    @Override
    public void onWIOMessageReceived(Context context, WIOMessage wioMessage) {

        boolean isNeedShowNotification = false;

        if (wioMessage != null && wioMessage.getPushType() != null) {
            if (wioMessage.getPushType().equals("FEED_NOTIFICATION_UPDATE")) {
                new NotificationUtils(FEED_PUSH_REMOTE_CHANNEL, DESCRIPTION_FEED_PUSH)
                        .showReminderNotification(wioMessage.getTitle(), wioMessage.getBody(), context);
            } else if (wioMessage.getPushType().equals("APP_REMINDER")) {
//                isNeedShowNotification = false;
                new NotificationUtils(REMINDER_PUSH_REMOTE_CHANNEL, DESCRIPTION_REMINDER_PUSH)
                        .showReminderNotification(wioMessage.getTitle(), wioMessage.getBody(), context);
            } else if (wioMessage.getPushType().equals("BADGES_UPDATE")) {
                // do nothing
//                isNeedShowNotification = false;
            } else {
                if (wioMessage.getPushType().equals("POST_STATUS") || wioMessage.getPushType().equals("COMMENT_STATUS")) {
                    isNeedShowNotification = false;
                } else if (wioMessage.getPushType().equals("UPDATE_DAILY_GOAL")
                        || wioMessage.getPushType().equals("DAILY_WEATHER")) { // SPORT+WEATHER
                    String status = (String) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.STRING,
                            NetworkCallbackChannel.TOGGLE_SPORT_WEATHER);
                    if (status != null && !Boolean.parseBoolean(status)) {
//                        isNeedShowNotification = false;
                    } else {
                        isNeedShowNotification = true;
                    }
                } else {
                    isNeedShowNotification = true;
                }
            }

            if (isNeedShowNotification) {
                new NotificationUtils(wioMessage.getPushType(), DESCRIPTION_PUSH)
                        .showNotification(wioMessage.getTitle(), wioMessage.getBody(), context);
            }
        }

        boolean isBackground = !LifeCycleAppForPush.getInstance().isAppShow();
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.STRING, String.valueOf(isBackground),
                String.valueOf(wioMessage.getTimeStamp()));


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("isBackground", isBackground);
        jsonObject.addProperty("isNeedShowNotification", isNeedShowNotification);

        AnalyticsEvent.Builder builder = new AnalyticsEvent.Builder()
                .setAction("sent")
                .setBody(wioMessage.getBody())
                .setTime(System.currentTimeMillis())
                .setMessageType("notification_push")
                .setTitle(wioMessage.getTitle())
                .setData(jsonObject.toString())
                .build();

        new WIONetwork.Analytics().userNotifications(builder);
        PushCallback.getInstance().setWIOMessageResult(wioMessage);
    }

    @Override
    public void onDeletedMessages() {

    }

}

package io.water.sdk_plugin.engine.channels.callbacks.base;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.base.FlutterChannel;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public abstract class CallbackChannel extends FlutterChannel implements MethodChannel.MethodCallHandler {
    public CallbackChannel(BinaryMessenger binaryMessenger, String channelPath) {
        super(binaryMessenger, channelPath);
        mMethodChannel.setMethodCallHandler(this);
    }

    @Override
    public abstract void onMethodCall(MethodCall methodCall, MethodChannel.Result result);
}

package io.water.sdk_plugin.engine.implementations.procedures;

import com.water.water_io_sdk.ble.connection.command.ClearDataCommand;
import com.water.water_io_sdk.ble.connection.command.HoldOnCommand;
import com.water.water_io_sdk.ble.connection.command.PlaySoundCommand;
import com.water.water_io_sdk.ble.connection.command.SetDailyUsageTimeCommand;
import com.water.water_io_sdk.ble.connection.command.SingleCommand;
import com.water.water_io_sdk.ble.connection.command.StartBlinkCommand;
import com.water.water_io_sdk.ble.connection.command.TrySetTimeBlinkCommand;
import com.water.water_io_sdk.ble.connection.command.advertisement_config.TrySetAdvertisePeriodConfigurationCommand;
import com.water.water_io_sdk.ble.connection.command.set_vitamins_reminder.SetVitaminReminderCommand;
import com.water.water_io_sdk.ble.connection.command.set_vitamins_reminder.SetVitaminsReminderCommand;
import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractFirstConnection;
import com.water.water_io_sdk.ble.enums.AdvertiseInterval;
import com.water.water_io_sdk.reminder_local_push.entities.Reminder;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;

import io.water.sdk_plugin.utils.Constants;

public class SDKMelodyProcedureCmds extends ProcedureAbstractFirstConnection {


    @Override
    public ArrayList<SingleCommand> initCommands() {
        ArrayList<SingleCommand> listCommands = new ArrayList<>();
        listCommands.add(new TrySetTimeBlinkCommand(5));
        listCommands.add(new StartBlinkCommand());
        listCommands.add(new HoldOnCommand(Constants.HOLD_ON_KEY));
        listCommands.add(new ClearDataCommand());
        listCommands.add(new SetDailyUsageTimeCommand(3, 0, 2, 59));
        listCommands.add(new PlaySoundCommand(true));
        handleReminders(listCommands);
        listCommands.add(new TrySetAdvertisePeriodConfigurationCommand(5, AdvertiseInterval._500MS));
        return listCommands;
    }

    private void handleReminders(ArrayList<SingleCommand> listCommands) {
        SharedPrefsModuleHelper helper = SharedPrefsModuleHelper.getInstance();
        boolean isOldVersion = (boolean) helper.getStoreData(StoreType.BOOLEAN, Constants.IS_OLD_VERSIONS_CAP);
        if (isOldVersion) {
            Reminder reminder = helper.getRemindersCmdList()[0];
            listCommands.add(new SetVitaminReminderCommand(reminder.getHour(), reminder.getMinutes()));
        } else {
            listCommands.add(new SetVitaminsReminderCommand());
        }
    }
}

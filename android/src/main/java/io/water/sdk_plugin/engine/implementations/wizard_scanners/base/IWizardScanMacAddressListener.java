package io.water.sdk_plugin.engine.implementations.wizard_scanners.base;

public interface IWizardScanMacAddressListener {
    void onNewDetectedMacAddress(String mac);
}

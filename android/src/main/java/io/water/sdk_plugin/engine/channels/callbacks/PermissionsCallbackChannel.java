package io.water.sdk_plugin.engine.channels.callbacks;

import android.Manifest;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;

import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.enums.AppMode;
import com.water.water_io_sdk.ble.enums.ScanRequest;
import com.water.water_io_sdk.ble.interfaces.CallbackScanRequest;
import com.water.water_io_sdk.ble.interfaces.IWIOApp;
import com.water.water_io_sdk.ble.utilities.GpsUtils;
import com.water.water_io_sdk.ble.utilities.Utils;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.LogsType;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.SDKLogsRepository;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.entities.SDKLog;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import io.flutter.Log;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.activities.PluginPermissionsActivity;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;
import io.water.sdk_plugin.enums.AdapterResult;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.BLUETOOTH_SERVICE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CHECK_IF_ALL_PERMISSION_ALLOWED_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CHECK_IS_ANDROID_NOTIFICATION_PERMISSION_GRANTED;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_STATUS_ADAPTERS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.IS_BLUETOOTH_PERMISSION_GRANTED;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.IS_NOTIFICATION_CAN_SHOW;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.MISSING_PERMISSIONS_CHECK_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.NOTIFICATIONS_OR_BATTERY_OPT_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.OPEN_PERMISSION_APP_SETTINGS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.PERMISSIONS_ADAPTERS_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.PERMISSIONS_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.PERMISSIONS_CHUNK_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REQUEST_ANDROID_NOTIFICATION_PERMISSION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.USER_WAS_ASKED_ABOUT_LOCATION_PERMISSION_ALREADY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CHECK_LOCATION_PERMISSION_STATUS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CHECK_BLUETOOTH_PERMISSION_STATUS;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

public class PermissionsCallbackChannel extends CallbackChannel {
    private PluginPermissionsActivity mActivity;
    private final IWIOApp mWioApp;
    private final HashMap<Integer, MethodChannel.Result> mReqsResHash = new HashMap<>();
    private final boolean NEED_ASK_KEY = true;
    private final boolean NO_NEED_ASK_KEY = false;

    public PermissionsCallbackChannel(BinaryMessenger binaryMessenger, IWIOApp wioApp) {
        super(binaryMessenger, PERMISSIONS_CHANNEL);
        mWioApp = wioApp;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case GET_STATUS_ADAPTERS:
                getStatusOfAdapters(result);
                break;
            case NOTIFICATIONS_OR_BATTERY_OPT_METHOD:
                handleNotificationsPermissionCall(methodCall, result);
                break;
            case PERMISSIONS_CHUNK_METHOD:
                handlePermissionsChunkRequest(methodCall, result);
                break;
            case PERMISSIONS_ADAPTERS_METHOD:
                handleAdaptersPermissionsReuqest(methodCall, result);
                break;
//            case DENIED_PERMISSIONS_CHUNK_METHOD:
//                handleDeniedPermissionsChunkRequest(methodCall, result);
//                break;
            case MISSING_PERMISSIONS_CHECK_METHOD:
                handleMissingPermissionsCheckRequest(result);
                break;
            case USER_WAS_ASKED_ABOUT_LOCATION_PERMISSION_ALREADY:
                handleWasUserAskedAboutLocationPermission(result);
                break;
            case CHECK_IF_ALL_PERMISSION_ALLOWED_METHOD:
                handleCheckIfHasAllPermissionToScan(result);
                break;
            case CHECK_LOCATION_PERMISSION_STATUS:
                handleCheckIfLocationPermissionDenied(result);
                break;
            case CHECK_BLUETOOTH_PERMISSION_STATUS:
                handleCheckIfBluetoothPermissionDenied(result);
                break;
            case IS_BLUETOOTH_PERMISSION_GRANTED:
                handleBluetoothPermissionAndroid12(result);
                break;
            case IS_NOTIFICATION_CAN_SHOW:
                isNotificationCanShow(result);
                break;
            case CHECK_IS_ANDROID_NOTIFICATION_PERMISSION_GRANTED:
                isNotificationPermissionGranted(result);
                break;
            case REQUEST_ANDROID_NOTIFICATION_PERMISSION:
                requestNotificationPermission(result);
                break;
            case OPEN_PERMISSION_APP_SETTINGS:
                mActivity.openSettingsActivity(result);
                break;
        }
    }

    private void isNotificationCanShow(MethodChannel.Result result) {
        boolean isMissedNotificationAndroid13 = mActivity.checkIsNotificationPermissionMissedAndroid13();
        boolean isNotificationEnable = Utils.isNotificationEnable(ModuleApp.getContext());
        SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "isNotificationCanShow", "isMissedNotificationAndroid13 = " + isMissedNotificationAndroid13, "isNotificationEnable = " + isNotificationEnable, "", "");
        new SDKLogsRepository().insert(sdkLog);
        result.success(!isMissedNotificationAndroid13 && isNotificationEnable);
    }

    private void isNotificationPermissionGranted(MethodChannel.Result result) {
        boolean isDenied = mActivity.checkIsNotificationPermissionMissedAndroid13();
        SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "isNotificationPermissionGranted", "isDenied = " + isDenied, "", "", "");
        new SDKLogsRepository().insert(sdkLog);
        result.success(!isDenied);
    }

    private void requestNotificationPermission(MethodChannel.Result result) {
        int randReqId = new Random().nextInt(5000);
        SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "requestNotificationPermission", "", "" + randReqId, "", "");
        new SDKLogsRepository().insert(sdkLog);
        mReqsResHash.put(randReqId, result);
        mActivity.requestNotificationPermissionAndroid13(randReqId);
    }
//
//    //TODO check if missising permission
//    // check if disable permission
//    //Show icon issue
//
//    private void handleNotificationPermission(MethodChannel.Result result) {
//        boolean isMissedNotificationAndroid13 = mActivity.checkIsNotificationPermissionMissedAndroid13();
//        if (isMissedNotificationAndroid13) {
//            int randReqId = new Random().nextInt(5000);
//            SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "handleNotificationPermission", "isMissedNotificationAndroid13 = true", "" + randReqId, "", "");
//            new SDKLogsRepository().insert(sdkLog);
//            mReqsResHash.put(randReqId, result);
//            mActivity.requestNotificationPermissionAndroid13(randReqId);
//        } else {
//            result.success(Utils.isNotificationEnable(ModuleApp.getContext()));
//        }
////        result.success(!isMissedNotificationAndroid13 && Utils.isNotificationEnable(ModuleApp.getContext()));
//    }

    private void handleBluetoothPermissionAndroid12(MethodChannel.Result result) {
        int randReqId = new Random().nextInt(5000);
        boolean askUserPermIsRequired = mActivity.shouldAskForBluetoothAndroid12Permissions(randReqId);

        if (askUserPermIsRequired) {
            SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "handleBluetoothPermissionAndroid12", "", "" + randReqId, "", "");
            new SDKLogsRepository().insert(sdkLog);
            mReqsResHash.put(randReqId, result);
        } else {
            result.success(null);
        }
    }

    public void handleCheckIfLocationPermissionDenied(MethodChannel.Result result) {
        if (isLocationPermissionDenied()) {
            result.success(NEED_ASK_KEY);
        } else {
            result.success(NO_NEED_ASK_KEY);
        }
    }

    private boolean isLocationPermissionDenied() {
        return ContextCompat.checkSelfPermission(ModuleApp.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(ModuleApp.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_DENIED;
    }

    public void handleCheckIfBluetoothPermissionDenied(MethodChannel.Result result) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (isBLEPermissionDeniedOnAndroid12()) {
                result.success(NEED_ASK_KEY);
            } else {
                result.success(NO_NEED_ASK_KEY);
            }
        } else {
            result.success(NO_NEED_ASK_KEY);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    private boolean isBLEPermissionDeniedOnAndroid12() {
        return ContextCompat.checkSelfPermission(ModuleApp.getContext(), Manifest.permission.BLUETOOTH_SCAN)
                == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(ModuleApp.getContext(), Manifest.permission.BLUETOOTH_CONNECT)
                == PackageManager.PERMISSION_DENIED;
    }

    public void handleCheckIfHasAllPermissionToScan(MethodChannel.Result result) {
        mWioApp.getWIOSmartDevice().checkManuallyBeforeScan(scanRequest -> {
            switch (scanRequest) {
                case MISSING_BACKGROUND_LOCATION_PERMISSION:
                case MISSING_LOCATION_PERMISSION:
                case DENIED_BACKGROUND_LOCATION_PERMISSION:
                case DENIED_LOCATION_PERMISSION:
                case DENIED_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                case MISSING_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                    result.success("NEED_ASK");
                    break;
                default:
                    result.success("NO_NEED_ASK");
                    break;
            }
        });
    }

    private void handleWasUserAskedAboutLocationPermission(MethodChannel.Result result) {
        if (SharedPrefsModuleHelper.getInstance().getAppMode() == AppMode.FOREGROUND_AND_BACKGROUND && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            result.success(mActivity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION));
        } else {
            result.success(mActivity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION));
        }
    }

    private void handleMissingPermissionsCheckRequest(MethodChannel.Result result) {
        boolean isAppAbleToStartScanning = mWioApp.getWIOSmartDevice().checkManuallyBeforeScan(new CallbackScanRequest() {
            @Override
            public void onResult(ScanRequest scanRequest) {
                switch (scanRequest) {
                    case MISSING_BACKGROUND_LOCATION_PERMISSION:
                    case MISSING_LOCATION_PERMISSION:
                    case MISSING_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                        result.error("error", "CHUNK_PERMISSIONS_ARE_MISSING", null);
                        break;
                    case DENIED_BACKGROUND_LOCATION_PERMISSION:
                    case DENIED_LOCATION_PERMISSION:
                    case DENIED_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                        result.error("error", "CHUNK_PERMISSIONS_ARE_DENIED", null);
                        break;
                    case MODULE_OFF:
                        result.error("error", "MODULE_IS_OFF", null);
                        break;
                    case ALREADY_SCANNING:
                        result.success(null);
                        break;
                }
            }
        });

        if (isAppAbleToStartScanning) {
            result.success(null);
        }
    }

//    private void handleDeniedPermissionsChunkRequest(MethodCall methodCall, MethodChannel.Result result) {
//        int randReqId = new Random().nextInt(5000);
//        mReqsResHash.put(randReqId, result);
//        mActivity.askForDeniedChunkPermissions(randReqId);
//    }


    public void getStatusOfAdapters(MethodChannel.Result result) {
        boolean shouldRequestBluetooth = shouldTurnOnBluetooth();
        boolean shouldRequestGPS = !GpsUtils.isLocationEnabled(mActivity);
        if (shouldRequestBluetooth) {
            result.success(AdapterResult.MISSING_BLUETOOTH.name());
        } else if (shouldRequestGPS) {
            result.success(AdapterResult.MISSING_GPS.name());
        } else {
            result.success(AdapterResult.SUCCESS.name());
        }
    }

    public boolean shouldTurnOnBluetooth() {
        BluetoothManager bluetoothManager = (BluetoothManager) mActivity.getApplicationContext().getSystemService(BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            return bluetoothManager.getAdapter() == null || !bluetoothManager.getAdapter().isEnabled();
        } else {
            Log.e("PLUGIN_BT", "bluetoothManager IS null!!");
            return true;
        }
    }


    public void turnOnBluetooth(int reqId) {
        BluetoothManager bluetoothManager = (BluetoothManager) mActivity.getApplicationContext().getSystemService(BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            if (bluetoothManager.getAdapter() == null || !bluetoothManager.getAdapter().isEnabled()) {
                Intent enableBtIntent = new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE");
                mActivity.startActivityForResult(enableBtIntent, reqId);
            } else {
                Log.e("PLUGIN_BT", "bluetoothManager IS null!!");
            }
        }
    }

    private void handleAdaptersPermissionsReuqest(MethodCall methodCall, MethodChannel.Result result) {
        String adapters_request = (String) methodCall.arguments;
        AdapterResult adapterResult = AdapterResult.valueOf(adapters_request);
        int gpsID;
        int bluetoothID;
        SDKLog sdkLog = null;

        switch (adapterResult) {
            case SUCCESS:
                result.success(null);
                break;
//            case MISSING_BOTH:
//                gpsID = getRandomID();
//                bluetoothID = getRandomID();
//                mWioApp.handleOptionalToConnectDevice(mActivity, gpsID, bluetoothID);
//                mReqsResHash.put(gpsID, result);
//                mReqsResHash.put(bluetoothID, result);
//                break;
            case MISSING_GPS:
                gpsID = getRandomID();
                GpsUtils.turnOnGps(mActivity, gpsID);
                sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "handleAdaptersPermissionsReuqest", "MISSING_GPS", "" + gpsID, "", "");
                mReqsResHash.put(gpsID, result);
                break;
            case MISSING_BLUETOOTH:
                bluetoothID = getRandomID();
                turnOnBluetooth(bluetoothID);
                sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "handleAdaptersPermissionsReuqest", "MISSING_BLUETOOTH", "" + bluetoothID, "", "");
                mReqsResHash.put(bluetoothID, result);
                break;
        }
        if (sdkLog != null) {
            new SDKLogsRepository().insert(sdkLog);
        }
    }

    private int getRandomID() {
        return new Random().nextInt(5000);
    }

    private void handlePermissionsChunkRequest(MethodCall methodCall, MethodChannel.Result
            result) {
        int randReqId = new Random().nextInt(5000);
        boolean askUserPermIsRequired = mActivity.shouldAskForChunkPermissions(randReqId);

        if (askUserPermIsRequired) {
            SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "handlePermissionsChunkRequest", "", "" + randReqId, "", "");
            new SDKLogsRepository().insert(sdkLog);
            mReqsResHash.put(randReqId, result);
        } else {
            result.success(null);
        }
    }

    private void handleNotificationsPermissionCall(MethodCall methodCall, MethodChannel.Result
            result) {
        if (mActivity == null) {
            result.success(null);
            return;
        }
        boolean isNeedRunningBackground = SharedPrefsModuleHelper.getInstance().getAppMode() == AppMode.FOREGROUND_AND_BACKGROUND;

        if (isNeedRunningBackground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int randReqId = new Random().nextInt(5000);

            PowerManager pm = (PowerManager) mActivity.getSystemService(Context.POWER_SERVICE);
            boolean alreadyIgnored = pm.isIgnoringBatteryOptimizations(mActivity.getPackageName());

            if (!alreadyIgnored) {
                mActivity.startActivityForResult(
                        new Intent(
                                android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                                Uri.parse("package:" + mActivity.getPackageName()
                                )
                        ), randReqId
                );
                SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "handleNotificationsPermissionCall", "", "" + randReqId, "", "");
                new SDKLogsRepository().insert(sdkLog);
                mReqsResHash.put(randReqId, result);
                return;
            }
        }

        result.success(null);
    }

    public void setActivity(PluginPermissionsActivity activity) {
        mActivity = activity;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (!mReqsResHash.containsKey(requestCode)) {
            return;
        }

        if (mReqsResHash.get(requestCode) == null) {
            mReqsResHash.remove(requestCode);
            return;
        }

        ArrayList<String> allowedPermissions = new ArrayList<>();
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (int i = 0; i < grantResults.length; i++) {
            String status;
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                status = "PERMISSION_GRANTED";
                allowedPermissions.add(permissions[i]);
            } else {
                status = "PERMISSION_DENIED";
                deniedPermissions.add(permissions[i]);
            }
            SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "onRequestPermissionsResult", status, "" + requestCode, permissions[i], "");
            new SDKLogsRepository().insert(sdkLog);
        }

        boolean isAllPermissionsAllowed = allowedPermissions.size() == permissions.length;
        if (isAllPermissionsAllowed) {
            mReqsResHash.get(requestCode).success(null);
            mReqsResHash.remove(requestCode);
            return;
        }


        mReqsResHash.get(requestCode).error("DENIED PERMISSIONS DETECTED", deniedPermissions.toString(), deniedPermissions);
        mReqsResHash.remove(requestCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mReqsResHash.containsKey(requestCode)) {
            return;
        }

        if (mReqsResHash.get(requestCode) == null) {
            mReqsResHash.remove(requestCode);
            return;
        }

        String status;
        if (resultCode == RESULT_OK) {
            status = "RESULT_OK";
            mReqsResHash.get(requestCode).success(null);
        } else {
            status = "RESULT_CANCELED";
            mReqsResHash.get(requestCode).error("DENIED ADAPTERS PERMISSIONS DETECTED", null, null);
        }

        SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "onActivityResult", status, "" + requestCode, "", "");
        new SDKLogsRepository().insert(sdkLog);

        mReqsResHash.remove(requestCode);
    }

    public void onDeniedPermissionsResult(int deniedPermsReqId, boolean success) {
        if (!mReqsResHash.containsKey(deniedPermsReqId)) {
            return;
        }

        if (mReqsResHash.get(deniedPermsReqId) == null) {
            mReqsResHash.remove(deniedPermsReqId);
            return;
        }

        MethodChannel.Result result = mReqsResHash.get(deniedPermsReqId);
        assert result != null;

        boolean isAppAbleToStartScanning = mWioApp.getWIOSmartDevice().checkManuallyBeforeScan(new CallbackScanRequest() {
            @Override
            public void onResult(ScanRequest scanRequest) {
                switch (scanRequest) {
                    case MISSING_BACKGROUND_LOCATION_PERMISSION:
                    case MISSING_LOCATION_PERMISSION:
                    case MISSING_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                        result.error("error", "CHUNK_PERMISSIONS_ARE_MISSING", null);
                        break;
                    case DENIED_BACKGROUND_LOCATION_PERMISSION:
                    case DENIED_LOCATION_PERMISSION:
                    case DENIED_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                        result.error("error", "CHUNK_PERMISSIONS_ARE_DENIED", null);
                        break;
                    case MODULE_OFF:
                        result.error("error", "MODULE_IS_OFF", null);
                        break;
                    case ALREADY_SCANNING:
                        result.success(null);
                        break;
                }
            }
        });

        if (isAppAbleToStartScanning) {
            result.success(null);
        }

        SDKLog sdkLog = new SDKLog(LogsType.LIMITATION_PROCESS, "onDeniedPermissionsResult", "isAppAbleToStartScanning: " + isAppAbleToStartScanning, "" + deniedPermsReqId, "", "");
        new SDKLogsRepository().insert(sdkLog);

        mReqsResHash.remove(deniedPermsReqId);
    }
}

package io.water.sdk_plugin.engine.channels.base;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodChannel;


public abstract class FlutterChannel {
    protected String mChannelPath;
    protected MethodChannel mMethodChannel;

    public FlutterChannel(BinaryMessenger binaryMessenger, String channelPath) {
        this.mChannelPath = channelPath;
        mMethodChannel = new MethodChannel(binaryMessenger, mChannelPath);
    }

    public FlutterChannel(MethodChannel methodChannel) {
        this.mMethodChannel = methodChannel;
    }

    public MethodChannel getMethodChannel() {
        return mMethodChannel;
    }


}

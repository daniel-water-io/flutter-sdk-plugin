package io.water.sdk_plugin.engine.implementations.procedures;

import com.water.water_io_sdk.ble.connection.command.SingleCommand;
import com.water.water_io_sdk.ble.connection.command.config.GetMultiConfigCommand;
import com.water.water_io_sdk.ble.connection.command.config.types.AdvertiseIntervalType;
import com.water.water_io_sdk.ble.connection.command.config.types.AdvertisePeriodType;
import com.water.water_io_sdk.ble.connection.command.config.types.CalibrationOffsetType;
import com.water.water_io_sdk.ble.connection.command.config.types.DARType;
import com.water.water_io_sdk.ble.connection.command.config.types.DBRType;
import com.water.water_io_sdk.ble.connection.command.config.types.DailyGoalType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableLedOpenCloseType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableLedStatusIndicatorType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableReminderLedType;
import com.water.water_io_sdk.ble.connection.command.config.types.EndTimeType;
import com.water.water_io_sdk.ble.connection.command.config.types.IGRType;
import com.water.water_io_sdk.ble.connection.command.config.types.IsNegativeFlagType;
import com.water.water_io_sdk.ble.connection.command.config.types.LastFWType;
import com.water.water_io_sdk.ble.connection.command.config.types.LastUserIDType;
import com.water.water_io_sdk.ble.connection.command.config.types.MARType;
import com.water.water_io_sdk.ble.connection.command.config.types.MBRType;
import com.water.water_io_sdk.ble.connection.command.config.types.MaxZType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderCyclesType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderIntervalType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderLedsColorType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderLedsPatternType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderRoundType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderUIElementsType;
import com.water.water_io_sdk.ble.connection.command.config.types.SessionIDType;
import com.water.water_io_sdk.ble.connection.command.config.types.SoundTypeType;
import com.water.water_io_sdk.ble.connection.command.config.types.StartTimeType;
import com.water.water_io_sdk.ble.connection.command.config.types.TimeZoneType;
import com.water.water_io_sdk.ble.connection.command.config.types.TypeConfig;
import com.water.water_io_sdk.ble.connection.command.config.types.VibrationLengthType;
import com.water.water_io_sdk.ble.connection.command.get_version.GetVersionDeviceCommand;
import com.water.water_io_sdk.ble.connection.command.run_single_measurement.RunSingleMeasurementNew;
import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractDefaultConnection;

import java.util.ArrayList;

public class HydrationDefaultCapCmds extends ProcedureAbstractDefaultConnection {

    @Override
    public ArrayList<SingleCommand> initCommands() {
        return new ArrayList<>();
    }
}

package io.water.sdk_plugin.engine.implementations.wizard_scanners.base;

import com.water.water_io_sdk.ble.interfaces.CallbackScanRequest;

public interface IWizardScanner {
    boolean startScan(CallbackScanRequest scanRequest);

    void onYesClicked();

    void onNoClicked();

    String getCapVersion();

    void removeDeviceFromBlackList();
}

package io.water.sdk_plugin.engine.channels.callbacks;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_RESET_BOOT_STATE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.OTA_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.START_OTA_BOOT_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.START_OTA_METHOD;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;

import com.google.gson.JsonObject;
import com.water.water_io_sdk.ble.entities.WIOSmartDevice;
import com.water.water_io_sdk.ble.ota_impl.interfaces.ICallbackOTA;
import com.water.water_io_sdk.ble.utilities.CapConfig;
import com.water.water_io_sdk.ble.utilities.SingletonHandleConnection;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;

import java.util.concurrent.atomic.AtomicReference;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;
import io.water.sdk_plugin.engine.event_channel.StatusOTAEvent;
import io.water.sdk_plugin.viewModels.DeltaEvents;

public class OTACallbackChannel extends CallbackChannel {

    private final WIOSmartDevice mWioSmartDevice;
    private final Activity mActivity;
    private final StatusOTAEvent eventSinkUpdateStatusOTA;
    private final DeltaEvents mLogicBootloaderCap = new DeltaEvents(1000 * 60 * 4);
    private final AtomicReference<String> mFromVersion = new AtomicReference<>("");
    private final AtomicReference<String> mToVersion = new AtomicReference<>("");

    public OTACallbackChannel(BinaryMessenger binaryMessenger, WIOSmartDevice wioSmartDevice, Activity activity,
                              LifecycleOwner lifecycleOwner) {
        super(binaryMessenger, OTA_CHANNEL);
        this.eventSinkUpdateStatusOTA = new StatusOTAEvent(binaryMessenger);
        this.mWioSmartDevice = wioSmartDevice;
        this.mActivity = activity;
        initObserveBootLoaderState(lifecycleOwner);
//        Log.v("OTACallbackChannel", "OTACallbackChannel()");
    }

    private void initObserveBootLoaderState(LifecycleOwner lifecycleOwner) {
        mWioSmartDevice.getBootloaderLiveData().observe(lifecycleOwner, macAddress -> {
            Log.v("OTACallbackChannel", "getBootloaderLiveData");
            if (mLogicBootloaderCap.isReasonableProfits()) {
                mLogicBootloaderCap.setLastTimestampHasError(System.currentTimeMillis());
//                Log.v("OTACallbackChannel", "getBootloaderLiveData 2");
                otaProcess(true);
            } else {
                Log.e("OTACallbackChannel", "getBootloaderLiveData fail");
            }
        });
    }


    private boolean isAlreadyStart = false;

    private void otaProcess(final boolean isShow) {
        if (isAlreadyStart) {
            Log.v("OTACallbackChannel", "otaProcess return");
            return;
        }
        isAlreadyStart = true;


        Log.v("OTACallbackChannel", "otaProcess isShow = " + isShow);

        boolean isFromBootloader = SingletonHandleConnection.getInstance().isInBootLoader() != SingletonHandleConnection.BootStatus.NONE;
        Log.v("OTACallbackChannel", "isFromBootloader = " + SingletonHandleConnection.getInstance().isInBootLoader().name() + " , isFromBootloader=" + isFromBootloader);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFromBootloader && isShow) {
                    eventSinkUpdateStatusOTA.sinkEventDataStuckOTA(SharedPrefsModuleHelper.getInstance().getBootVersionDevice(), null);
                    Log.v("OTACallbackChannel", "sinkEventDataStuckOTA");
                }
                if (!isShow) {
                    Log.v("OTACallbackChannel", "isShow false ");
                }
            }
        }, 4000);
        mWioSmartDevice.startProcessOTA(isFromBootloader, mActivity, true, new ICallbackOTA() {
            @Override
            public void onStatusUpdate(String desc, boolean isFinish) {
                Log.v("OTACallbackChannel", "getBootloaderLiveData onStatusUpdate: " + desc);
                if (isFinish) {
                    eventSinkUpdateStatusOTA.sinkEventData(StatusOTAEvent.OTA_STATUS.END);
                    isAlreadyStart = false;
                } else if (desc.equals("CONNECTED")) {
                    eventSinkUpdateStatusOTA.sinkEventData(StatusOTAEvent.OTA_STATUS.START);
                }
            }

            @Override
            public void versionsInfo(String fromVersion, String toVersion) {
                mFromVersion.set(fromVersion);
                mToVersion.set(toVersion);

                Log.v("OTACallbackChannel", "fromVersion  : " + fromVersion + " , To: " + toVersion);
                eventSinkUpdateStatusOTA.sinkEventData(StatusOTAEvent.OTA_STATUS.START);
            }

            @Override
            public void onProgressUpdate(int percentage) {
                eventSinkUpdateStatusOTA.sinkEventData(percentage);
                Log.v("OTACallbackChannel", "percentage  : " + percentage + "%");
            }

            @Override
            public void onError(String desc) {
                Log.v("OTACallbackChannel", "getBootloaderLiveData onError: " + desc);
                SingletonHandleConnection.getInstance().stopForceConnection();
                mLogicBootloaderCap.setLastTimestampHasError(-1);
                eventSinkUpdateStatusOTA.sinkEventData(StatusOTAEvent.OTA_STATUS.CAP_STUCK_IN_BOOT);
                isAlreadyStart = false;
            }
        });
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        Log.v("OTACallbackChannel", "onMethodCall " + methodCall.method);

        if (START_OTA_METHOD.equals(methodCall.method)) {
            otaProcess(false);
        } else if (START_OTA_BOOT_METHOD.equals(methodCall.method)) {
            otaProcess(true);
        } else if (DEVICE_SET_RESET_BOOT_STATE.equals(methodCall.method)) {
            mWioSmartDevice.stopProcessOTA();
            forceResetAllCache();
            CapConfig.getInstance().setHasPaddingTypeConfig(true);
        }
        result.success(true);
    }

    public void forceResetAllCache() {
        mLogicBootloaderCap.setLastTimestampHasError(-1);
        mFromVersion.set("");
        mToVersion.set("");
        isAlreadyStart = false;
        Log.v("OTACallbackChannel", "forceResetAllCache()");
    }
}

package io.water.sdk_plugin.engine.channels.callbacks;

import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.PLUGIN_MESSENGER_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_STRING_IN_PLUGIN_STORAGE;

import android.util.Log;

public class PluginMessengerCallbackChannel extends CallbackChannel {
    public PluginMessengerCallbackChannel(BinaryMessenger binaryMessenger) {
        super(binaryMessenger, PLUGIN_MESSENGER_CHANNEL);
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case SET_STRING_IN_PLUGIN_STORAGE:
                handleSetStringInPluginStorage((Map<String, Object>) methodCall.arguments, result);
                break;
        }
    }

    private void handleSetStringInPluginStorage(Map<String, Object> arguments, MethodChannel.Result result) {
        String key = (String) arguments.get("key");
        String value = (String) arguments.get("value");

//        Log.v("InsertMessage","Key: "+key+" , Val: "+value);
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.STRING, value, key);
        result.success(null);
    }
}

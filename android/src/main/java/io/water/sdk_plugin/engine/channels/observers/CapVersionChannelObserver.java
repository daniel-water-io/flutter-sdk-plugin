package io.water.sdk_plugin.engine.channels.observers;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_STATE_CHANNEL_PATH;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_STATE_VERSION_METHOD;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;


import com.water.water_io_sdk.ble.entities.CommandResponse;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;

public class CapVersionChannelObserver extends ChannelObserver<CommandResponse> {

    public CapVersionChannelObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData observable) {
        super(binaryMessenger, CAP_STATE_CHANNEL_PATH, CAP_STATE_VERSION_METHOD, owner, observable);
    }

    @Override
    public void onChanged(CommandResponse commandResponse) {
        if (commandResponse.getCommand().equals("GET_VERSION_DEVICE_COMMAND"))
            invokeTarget(commandResponse.getData());
    }

}


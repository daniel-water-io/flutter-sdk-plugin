package io.water.sdk_plugin.engine.channels.observers;

import java.util.HashMap;
import java.util.Map;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_CONNECTION_STATUS_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_CONNECTION_STATUS_METHOD;

public class CapConnectionStatusChannelObserver extends ChannelObserver<Map.Entry<String, Boolean>> {

    public CapConnectionStatusChannelObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData observable) {
        super(binaryMessenger, CAP_CONNECTION_STATUS_CHANNEL, CAP_CONNECTION_STATUS_METHOD, owner, observable);
    }

    @Override
    public void onChanged(Map.Entry<String, Boolean> entry) {
        String macAddress = entry.getKey();
        boolean isConnected = entry.getValue();

        Map<String, Object> map = new HashMap<>();
        map.put("macAddress", macAddress);
        map.put("isConnected", isConnected);
        invokeTarget(map);
    }

}


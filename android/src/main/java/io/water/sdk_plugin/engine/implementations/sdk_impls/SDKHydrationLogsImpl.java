package io.water.sdk_plugin.engine.implementations.sdk_impls;

import android.content.Context;
import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.ble.handler_logs.categoties.CapEvent;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;
import java.util.ArrayList;
import io.water.sdk_plugin.background.utilities.CapLoggerHandler;
import io.water.sdk_plugin.utils.Constants;
import io.water.sdk_plugin.utils.Utilities;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_LAST_SYNC_TIMESTAMP;

public class SDKHydrationLogsImpl extends CapEvent {
    public SDKHydrationLogsImpl(String typeResponse, String data, String macAddress, Context contextApplication) {
        super(typeResponse, data, macAddress, contextApplication);
    }

    public SDKHydrationLogsImpl(ArrayList<DeviceLog> logsDeviceList, String macAddress, Context contextApplication) {
        super(logsDeviceList, macAddress, contextApplication);
    }

    public SDKHydrationLogsImpl(String macAddress, Boolean isNewAttachedDevice, Context contextApplication) {
        super(macAddress, isNewAttachedDevice, contextApplication);
    }

    @Override
    protected void onOpenCapLogEvent(DeviceLog deviceLog) {
//        HydrationRepository.getInstance().handleOpenReport(deviceLog);
    }

    @Override
    protected void onCloseCapLogEvent(DeviceLog deviceLog) {
//        HydrationRepository.getInstance().handleCloseEvent(deviceLog);
    }

    @Override
    protected void onReminderCapLogEvent(DeviceLog deviceLog) {
//        HydrationRepository.getInstance().handleReminderReport(deviceLog);
    }

    @Override
    protected void onMeasurementAccurateCapLogEvent(DeviceLog deviceLog) {
//        HydrationRepository.getInstance().handleMeasurementReport(deviceLog);
    }

    @Override
    protected void onOpenTopCapLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onCloseTopCapLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onConnectLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onDisconnectLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onVoltageLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onResetDeviceLogEvent(DeviceLog deviceLog) {
//        HydrationRepository.getInstance().handleResetReport(deviceLog);
    }

    @Override
    protected void onCommandResponse(String typeResponse, String data, String macAddress) {
        if (typeResponse.equals("GET_VERSION_DEVICE_COMMAND")) {
            SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.BOOLEAN, Utilities.isOldVersion(data), Constants.IS_OLD_VERSIONS_CAP);
        }
    }

    @Override
    protected void onNewAttachedDevice(String macAddress) {

    }

    @Override
    protected void onAdvertisementLogEvents(DeviceLog deviceLog, String macAddress, int rssi) {

    }

    @Override
    public void handleListLogsEvent(ArrayList<DeviceLog> arrayList, String macAddress) {
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.LONG, System.currentTimeMillis(), DEVICE_LAST_SYNC_TIMESTAMP);
        CapLoggerHandler.getInstance(null, null, null, null).onChangeManually(arrayList);
    }


}

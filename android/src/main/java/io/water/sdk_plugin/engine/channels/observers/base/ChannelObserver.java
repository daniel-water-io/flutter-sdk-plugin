package io.water.sdk_plugin.engine.channels.observers.base;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.background.utilities.LifeCycleApp;
import io.water.sdk_plugin.engine.channels.base.FlutterChannel;
import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

public abstract class ChannelObserver<T> extends FlutterChannel implements Observer<T> {

    protected String mTargetMethod;
    private LiveData<T> mObservable;
    private LifecycleOwner mOwner;

    public ChannelObserver(BinaryMessenger binaryMessenger,
                           String channelPath,
                           @NonNull String targetMethod,
                           LifecycleOwner owner,
                           LiveData<T> observable) {
        super(binaryMessenger, channelPath);

        this.mObservable = observable;
        this.mTargetMethod = targetMethod;
        this.mOwner = owner;
        this.mObservable.observe(owner, this);
    }

    public ChannelObserver(MethodChannel methodChannel,
                           @NonNull String targetMethod,
                           LifecycleOwner owner,
                           LiveData<T> observable) {
        super(methodChannel);
        this.mObservable = observable;
        this.mTargetMethod = targetMethod;
        this.mOwner = owner;
        this.mObservable.observe(owner, this);
    }



    @Override
    protected void finalize() throws Throwable {
        new Handler(Looper.getMainLooper()).post(() -> {
            if (mObservable != null)
                mObservable.removeObservers(mOwner);

            mObservable = null;
            mOwner = null;
            mTargetMethod = null;
        });
        
//        super.finalize();
    }


    protected void invokeTarget(Object args) {
        mMethodChannel.invokeMethod(mTargetMethod, args);
    }

    @Override
    public abstract void onChanged(T t);
}

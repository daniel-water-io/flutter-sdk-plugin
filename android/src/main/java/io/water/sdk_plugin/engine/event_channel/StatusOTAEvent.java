package io.water.sdk_plugin.engine.event_channel;
import java.util.HashMap;
import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.event_channel.base.BaseEventChannel;

public class StatusOTAEvent extends BaseEventChannel {
    private final HashMap<String, Object> mapData = new HashMap<>();

    public StatusOTAEvent(BinaryMessenger binaryMessenger) {
        super(binaryMessenger, "OTA_STATE_EVENT");
    }

    public void sinkEventData(OTA_STATUS otaStatus) {
//        mapData.put("state", otaStatus.name());
//        mapData.put("value", 0);
//        super.sinkData(mapData);
        
        HashMap<String,Integer> progressMap=new HashMap<>();
        mapData.put("state", otaStatus.name());
        mapData.put("value", progressMap);
        progressMap.put("value",0);
        progressMap.put("additional_value",null);
    }

    public void sinkEventData(int progress) {
        HashMap<String,Integer> progressMap=new HashMap<>();
        progressMap.put("value",progress);
        progressMap.put("additional_value",progress);
        mapData.put("state", OTA_STATUS.UPLOADING.name());
        mapData.put("value", progressMap);
        super.sinkData(mapData);
    }
    public void sinkEventDataStuckOTA(String boot,String hardware) {
        HashMap<String,String> progressMap=new HashMap<>();
        progressMap.put("value",boot);
        progressMap.put("additional_value",hardware);
        mapData.put("state", OTA_STATUS.CAP_STUCK_IN_BOOT.name());
        mapData.put("value", progressMap);
        super.sinkData(mapData);
    }

    public enum OTA_STATUS {
        START,
        END,
        CAP_STUCK_IN_BOOT,
        UPLOADING
    }
}

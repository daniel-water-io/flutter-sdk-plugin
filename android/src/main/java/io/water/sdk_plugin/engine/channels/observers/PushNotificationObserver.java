package io.water.sdk_plugin.engine.channels.observers;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.NETWORK_USER_CLICK_URL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.PUSH_NOTIFICATIONS_CHANNEL;

import android.util.Log;

import androidx.lifecycle.LifecycleOwner;

import com.google.gson.JsonObject;
import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.utilities.Utils;
import com.water.water_io_sdk.push.entities.WIOMessage;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.background.utilities.LifeCycleApp;
import io.water.sdk_plugin.background.utilities.LifeCycleAppForPush;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;
import io.water.sdk_plugin.background.utilities.LifeCycleAppForPush;
import io.water.sdk_plugin.utils.PushCallback;

public class PushNotificationObserver extends ChannelObserver<WIOMessage> {

    public PushNotificationObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner) {
        super(binaryMessenger, PUSH_NOTIFICATIONS_CHANNEL, NETWORK_USER_CLICK_URL, owner, PushCallback.getInstance().getMessageLiveData());
    }

    @Override
    public void onChanged(WIOMessage wioMessage) {
        Map<String, Object> map = new HashMap<>();
        map.put("title", wioMessage.getTitle());
        map.put("event_time", wioMessage.getTimeStamp());
        map.put("body", wioMessage.getBody());
        JsonObject jsonObject = wioMessage.getExtraJsonData();
        if (jsonObject != null) {
            map.put("data_as_json", jsonObject.toString());
        } else {
            map.put("data_as_json", "{}");
        }

        map.put("push_type", wioMessage.getPushType());

         String isBackground = (String) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.STRING, String.valueOf(wioMessage.getTimeStamp()));
        if (isBackground == null) {
            map.put("is_background", !LifeCycleAppForPush.getInstance().isAppShow());
        } else {
            map.put("is_background", Boolean.valueOf(isBackground));
        }

        invokeTarget(map);
    }

    // TODO - MAKE AN EXTRENAL FILE FOR THE CONSTANTS ABOVE - 'event_type' etc..
}



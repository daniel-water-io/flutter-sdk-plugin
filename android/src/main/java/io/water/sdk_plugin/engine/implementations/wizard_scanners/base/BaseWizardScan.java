package io.water.sdk_plugin.engine.implementations.wizard_scanners.base;

import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.ble.entities.WIOSmartDevice;
import com.water.water_io_sdk.ble.enums.EnumCommandDevice;
import com.water.water_io_sdk.ble.interfaces.CallbackScanRequest;
import com.water.water_io_sdk.ble.interfaces.CallbackScanningWizardMode;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import io.flutter.Log;
import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.WizardScanOnPotentialCapConnectedObserver;
import io.water.sdk_plugin.utils.Constants;
import io.water.sdk_plugin.utils.DeviceLogEventsUnifier;
import io.water.sdk_plugin.utils.Utilities;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_LAST_SYNC_TIMESTAMP;

import android.os.Handler;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

public abstract class BaseWizardScan implements IWizardScanner {

    protected CallbackScanningWizardMode.CallbackScanningWizardModeFeedback mListener;
    protected BinaryMessenger mBinaryMessenger;
    protected WIOSmartDevice mWIOSmartDevice;
    protected WizardScanOnPotentialCapConnectedObserver mPotentialCapConnectedObserver;
    protected LifecycleOwner mLifecycleOwner;
    protected final MutableLiveData<Map.Entry<String, Boolean>> mMacAddressObservable = new MutableLiveData<>();
    protected IWizardScanMacAddressListener mWizardScanMacAddressListener;
    protected Boolean mWasThatTheRelevantCap;        // null means no action from user
    public String mCapVersion;
    protected DeviceLogEventsUnifier mDeviceLogEventsUnifier;

    public abstract boolean startScanning(CallbackScanRequest scanRequest);

    protected abstract void onDisconnect(final String macAddress, final String version);

    protected CallbackScanningWizardMode mWizardModeCallback = new CallbackScanningWizardMode() {
        @Override
        public void onDisconnect(final String macAddress, final String version, boolean isDisconnectedBeforeUserAnswer) {
            BaseWizardScan.this.onDisconnect(macAddress, version);
            if (version.isEmpty())
                return;
            SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.BOOLEAN, Utilities.isOldVersion(version), Constants.IS_OLD_VERSIONS_CAP);
        }

        @Override
        public void onAttachFeedback(CallbackScanningWizardMode.CallbackScanningWizardModeFeedback
                                             callbackScanningWizardModeFeedback) {
            mListener = callbackScanningWizardModeFeedback;
            final EnumCommandDevice COMMAND_LISTENER = EnumCommandDevice.GET_VERSION_DEVICE_COMMAND;
            mListener.setCommandRes(COMMAND_LISTENER, (commandType, data, macAddress) -> {
                mListener.removeCommandRes(COMMAND_LISTENER);
                mCapVersion = data;
            });

        }

        @Override
        public void onConnect(final String macAddress) {
            mMacAddressObservable.setValue(new AbstractMap.SimpleEntry<>(macAddress, false));
            mCapVersion = null;
            mWasThatTheRelevantCap = null;
            SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.LONG, System.currentTimeMillis(), DEVICE_LAST_SYNC_TIMESTAMP);
        }

        @Override
        public void onConnectCapStuckInBoot(String macAddress) {
            android.util.Log.v("Plugin-wizard", "onConnectCapStuckInBoot()" + macAddress);
            mMacAddressObservable.setValue(new AbstractMap.SimpleEntry<>(macAddress, true));
            mCapVersion = null;
            mWasThatTheRelevantCap = null;
            SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.LONG, System.currentTimeMillis(), DEVICE_LAST_SYNC_TIMESTAMP);
        }

        @Override
        public void deviceState(final DeviceLog deviceLog) {
            if (mDeviceLogEventsUnifier != null)
                mDeviceLogEventsUnifier.updateChannel(new ArrayList<DeviceLog>() {
                    {
                        add(deviceLog);
                    }
                });
        }
    };

    public BaseWizardScan(BinaryMessenger mBinaryMessenger, WIOSmartDevice mWIOSmartDevice, LifecycleOwner mLifecycleOwner, IWizardScanMacAddressListener iWizardScanMacAddressListener) {
        this.mBinaryMessenger = mBinaryMessenger;
        this.mWIOSmartDevice = mWIOSmartDevice;
        this.mLifecycleOwner = mLifecycleOwner;
        this.mWizardScanMacAddressListener = iWizardScanMacAddressListener;
    }

    @Override
    public boolean startScan(CallbackScanRequest scanRequest) {
        if (mListener != null)
            return true;        // todo - make mListener null on user skipped with skip and continue withoud cap..

        mPotentialCapConnectedObserver = new WizardScanOnPotentialCapConnectedObserver(mBinaryMessenger,
                mLifecycleOwner,
                mMacAddressObservable
        );

        mMacAddressObservable.observe(mLifecycleOwner, mac -> {
            if (mWizardScanMacAddressListener != null) {
                String macAddress = mac == null ? null : mac.getKey();
                mWizardScanMacAddressListener.onNewDetectedMacAddress(macAddress);
            }
        });

        mCapVersion = null;

        return startScanning(scanRequest);
    }

    @Override
    public void onNoClicked() {
        mWasThatTheRelevantCap = false;
        // isAllCommandSent = false;
        if (mListener != null) {
            mListener.clientResponse(false);
        }
        mCapVersion = null;
    }

    @Override
    public void removeDeviceFromBlackList() {
        final Map.Entry<String, Boolean> entry = mMacAddressObservable.getValue();
        String macAddressConnected = entry == null ? "" : entry.getKey();
        onNoClicked();
        new Handler().postDelayed(() -> {
            if (mListener != null)
                mListener.removeDeviceFromBlackList(macAddressConnected);
        }, 1000);
    }

    @Override
    public void onYesClicked() {
        mWasThatTheRelevantCap = true;
    }

    @Override
    public String getCapVersion() {
        return mCapVersion;
    }
}

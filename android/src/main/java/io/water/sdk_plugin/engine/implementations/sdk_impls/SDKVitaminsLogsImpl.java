package io.water.sdk_plugin.engine.implementations.sdk_impls;

import android.content.Context;
import com.water.water_io_sdk.ble.entities.DeviceLog;
import com.water.water_io_sdk.ble.handler_logs.categoties.VitaminEvent;
import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;

import io.water.sdk_plugin.background.database.repositories.VitaminsRepository;
import io.water.sdk_plugin.utils.Constants;
import io.water.sdk_plugin.utils.Utilities;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_LAST_SYNC_TIMESTAMP;

public class SDKVitaminsLogsImpl extends VitaminEvent {
    public SDKVitaminsLogsImpl(String typeResponse, String data, String macAddress, Context contextApplication) {
        super(typeResponse, data, macAddress, contextApplication);
    }

    public SDKVitaminsLogsImpl(ArrayList<DeviceLog> logsDeviceList, String macAddress, Context contextApplication) {
        super(logsDeviceList, macAddress, contextApplication);
    }

    public SDKVitaminsLogsImpl(String macAddress, Boolean isNewAttachedDevice, Context contextApplication) {
        super(macAddress, isNewAttachedDevice, contextApplication);
    }

    @Override
    protected void onPreReminderCapLogEvent(DeviceLog deviceLog) {
        VitaminsRepository.getInstance().handlePreReminderReport(deviceLog);
    }

    @Override
    protected void onOpenCapLogEvent(DeviceLog deviceLog) {
        VitaminsRepository.getInstance().handleOpenReport(deviceLog);
    }

    @Override
    protected void onCloseCapLogEvent(DeviceLog deviceLog) {
        VitaminsRepository.getInstance().handleCloseAndPillTakenEventClient(deviceLog);
    }

    @Override
    protected void onReminderCapLogEvent(DeviceLog deviceLog) {
        VitaminsRepository.getInstance().handleReminderReport(deviceLog);
    }

    @Override
    protected void onMeasurementAccurateCapLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onOpenTopCapLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onCloseTopCapLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onConnectLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onDisconnectLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onVoltageLogEvent(DeviceLog deviceLog) {

    }

    @Override
    protected void onResetDeviceLogEvent(DeviceLog deviceLog) {
        VitaminsRepository.getInstance().handleResetReport(deviceLog);
    }

    @Override
    protected void onCommandResponse(String typeResponse, String data, String macAddress) {
        switch (typeResponse) {
            case "SET_VITAMIN_REMINDER_COMMAND":
                AnalyticsEvent.Builder builder = new AnalyticsEvent.Builder()
                        .setType("set_reminder_cap_successfully")
                        .setText("Set reminder to cap successfully")
                        .setTime(System.currentTimeMillis());
                new WIONetwork.Analytics().userLog(builder);
                break;
            case "GET_VERSION_DEVICE_COMMAND":
                SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.BOOLEAN, Utilities.isOldVersion(data), Constants.IS_OLD_VERSIONS_CAP);
                break;
        }
    }

    @Override
    protected void onNewAttachedDevice(String macAddress) {

    }

    @Override
    protected void onAdvertisementLogEvents(DeviceLog deviceLog, String macAddress, int rssi) {

    }

    @Override
    public void handleListLogsEvent(ArrayList<DeviceLog> arrayList, String macAddress) {
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.LONG, System.currentTimeMillis(), DEVICE_LAST_SYNC_TIMESTAMP);
    }
}

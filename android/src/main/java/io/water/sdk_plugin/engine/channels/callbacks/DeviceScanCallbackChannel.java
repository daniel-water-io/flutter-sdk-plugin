package io.water.sdk_plugin.engine.channels.callbacks;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.connection.command.DisconnectDeviceCommand;
import com.water.water_io_sdk.ble.connection.command.GetExtraDailyGoalCommand;
import com.water.water_io_sdk.ble.connection.command.GetHydrationsCommand;
import com.water.water_io_sdk.ble.connection.command.GetSyncInfoCommand;
import com.water.water_io_sdk.ble.connection.command.SetExtraDailyGoalCommand;
import com.water.water_io_sdk.ble.connection.command.SetHydrationLevelCommand;
import com.water.water_io_sdk.ble.connection.command.SetManuallyHydrationCommand;
import com.water.water_io_sdk.ble.connection.command.SetMultiPointOffsetCalibrationCommand;
import com.water.water_io_sdk.ble.connection.command.SetSilentModeCommand;
import com.water.water_io_sdk.ble.connection.command.SimulateReminderPatternCommand;
import com.water.water_io_sdk.ble.connection.command.SingleCommand;
import com.water.water_io_sdk.ble.connection.command.StartBlinkCommand;
import com.water.water_io_sdk.ble.connection.command.StartVibrationCommand;
import com.water.water_io_sdk.ble.connection.command.TurnLedsOnCommand;
import com.water.water_io_sdk.ble.connection.command.config.SetMultiConfigCommand;
import com.water.water_io_sdk.ble.connection.command.config.types.CalibrationOffsetType;
import com.water.water_io_sdk.ble.connection.command.config.types.DARType;
import com.water.water_io_sdk.ble.connection.command.config.types.DBRType;
import com.water.water_io_sdk.ble.connection.command.config.types.DailyGoalType;
import com.water.water_io_sdk.ble.connection.command.config.types.DemoModeLedIntervalType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableDemoModeType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableLedOffInChargerType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableLedOpenCloseType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableLedStatusIndicatorType;
import com.water.water_io_sdk.ble.connection.command.config.types.LedOffOutsideActiveTimeType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableReminderLedType;
import com.water.water_io_sdk.ble.connection.command.config.types.EnableShabatModeType;
import com.water.water_io_sdk.ble.connection.command.config.types.EndTimeType;
import com.water.water_io_sdk.ble.connection.command.config.types.MBRType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderCyclesType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderLedsColorType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderLedsPatternType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderLogicType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderRoundType;
import com.water.water_io_sdk.ble.connection.command.config.types.ReminderUIElementsType;
import com.water.water_io_sdk.ble.connection.command.config.types.StartTimeType;
import com.water.water_io_sdk.ble.connection.command.config.types.TypeConfig;
import com.water.water_io_sdk.ble.connection.command.run_single_measurement.RunSingleMeasurementNew;
import com.water.water_io_sdk.ble.entities.WIOSmartDevice;
import com.water.water_io_sdk.ble.interfaces.CallbackDirectCommand;
import com.water.water_io_sdk.ble.interfaces.capCallbacks.ICapCommandsResponse;
import com.water.water_io_sdk.ble.utilities.CapConfig;
import com.water.water_io_sdk.network.entities.requests.firebase.sdk_messaging.ErrorSDK;
import com.water.water_io_sdk.network.utilities.SimpleAsyncTask;
import com.water.water_io_sdk.reminder_local_push.entities.Reminder;
import com.water.water_io_sdk.reminder_local_push.utilities.ReminderManager;
import com.water.water_io_sdk.storage.database.logs.SDKLogsDatabase;
import com.water.water_io_sdk.storage.database.logs.hydration.HydrationDao;
import com.water.water_io_sdk.storage.database.logs.hydration.HydrationModel;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.LogsType;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.SDKLogsRepository;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.entities.SDKLog;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.lifecycle.LifecycleOwner;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.ProceduresEnum;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;
import io.water.sdk_plugin.engine.implementations.wizard_scanners.HoldOnWizardScanner;
import io.water.sdk_plugin.engine.implementations.wizard_scanners.base.IWizardScanMacAddressListener;
import io.water.sdk_plugin.engine.implementations.wizard_scanners.base.IWizardScanner;
import io.water.sdk_plugin.utils.DeviceLogEventsUnifier;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_GET_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_EXTRA_DAILY_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_HYDRATION_LEVEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_MANUALLY_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_COMMIT_BLINK_SEQUENCE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_GET_BASE_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_GET_DEVICE_DATA;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_GET_EXTRA_DAILY_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_LAST_SYNC_TIMESTAMP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_CALLBACK_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_CALLBACK_FORGET_CAP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_CALLBACK_START_SCAN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_CALLBACK_STOP_SCAN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_INVOKE_WIZARD_START_SCAN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_WIZARD_NOT_MY_CAP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_WIZARD_YES_MY_CAP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_DAILY_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_DISCONNECT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_LED_COLOR;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_MULTI_CONFIGURATIONS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_GET_MULTI_CONFIGURATIONS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_REMINDERS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_REMINDER_COLOR;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_SILENT_MODE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SET_VIBRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_UPDATE_DAILY_USAGE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_UPDATE_RSSI_CONFIG;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LAST_DB_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REGISTER_DEVICE_SCAN_WHEN_ENABLE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RUN_SINGLE_MEASUREMENT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_START_CALIBRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SIMULATION_VIBRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.UPDATE_REMINDER_LEDS_PATTERN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SIMULATE_REMINDER_PATTERN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_CHECK_CAP_SYNC;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RESTORE_PAIRED_TO_CAP;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class DeviceScanCallbackChannel extends CallbackChannel implements IWizardScanMacAddressListener {

    private final BinaryMessenger mBinaryMessenger;
    private final WIOSmartDevice mWIOSmartDevice;
    private final LifecycleOwner mLifecycleOwner;
    private final DeviceLogEventsUnifier mDeviceLogEventsUnifier;
    private IWizardScanner wizardScanner;

    private String mLastKnownMacAddress;
    private ICapCommandsResponse mICapCommandsResponse;

    public DeviceScanCallbackChannel(BinaryMessenger binaryMessenger, WIOSmartDevice WIOSmartDevice,
                                     LifecycleOwner lifecycleOwner, DeviceLogEventsUnifier logEventsUnifier) {
        super(binaryMessenger, DEVICE_SCAN_CALLBACK_CHANNEL);
        this.mBinaryMessenger = binaryMessenger;
        this.mWIOSmartDevice = WIOSmartDevice;
        this.mLifecycleOwner = lifecycleOwner;
        this.mDeviceLogEventsUnifier = logEventsUnifier;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case REGISTER_DEVICE_SCAN_WHEN_ENABLE:
                mWIOSmartDevice.registerStartScanWhenEnable();
                result.success(null);
                break;
            case RUN_SINGLE_MEASUREMENT:
                runSingleMeasurementCommand(methodCall, result);
                break;
            case DEVICE_SCAN_CALLBACK_START_SCAN:
                handleStartScanReq(methodCall, result);
                break;
            case DEVICE_SCAN_CALLBACK_FORGET_CAP:
                SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.BOOLEAN, false, "IS_CONNECT_IN_BOOT");
                mWIOSmartDevice.forgetDevice();
                result.success(null);
                break;
            case DEVICE_SCAN_CALLBACK_STOP_SCAN:
                wizardScanner = null;
                mWIOSmartDevice.stopScan();
                result.success(null);
                break;
            case DEVICE_SCAN_INVOKE_WIZARD_START_SCAN:
                SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.BOOLEAN, false, "IS_CONNECT_IN_BOOT");
                startScanningWizardMode(methodCall, result);
                mLastKnownMacAddress = null;
                break;
            case DEVICE_SCAN_WIZARD_YES_MY_CAP:
                if (wizardScanner != null) {
                    wizardScanner.onYesClicked();
                }
                wizardScanner = null;
                updateLastSyncDeviceTimestamp();
                result.success(null);
                break;
            case DEVICE_SCAN_WIZARD_NOT_MY_CAP:
                handleNotMyCap((Boolean) methodCall.arguments, result);
                break;
            case DEVICE_GET_DEVICE_DATA:
                handleGetDeviceData(result);
                break;
            case DEVICE_COMMIT_BLINK_SEQUENCE:
                Integer type = (Integer) methodCall.arguments;
                handleCommitBlinkSequence(type, result);
                break;
            case DEVICE_SET_REMINDERS:
                handleSetDeviceReminders((ArrayList<HashMap<String, Integer>>) methodCall.arguments, result);
                break;
            case DEVICE_SET_LED_COLOR:
                setLedColor((HashMap<String, Object>) methodCall.arguments, result);
                break;
            case DEVICE_UPDATE_DAILY_USAGE:
                handleUpdateDailyUsageTime((HashMap<String, Object>) methodCall.arguments, result);
                break;
            case DEVICE_UPDATE_RSSI_CONFIG:
                handleUpdateRssiConfig((HashMap<String, Object>) methodCall.arguments, result);
                break;
            case DEVICE_LAST_SYNC_TIMESTAMP:
                handleGetLastSyncTimestamp(methodCall, result);
                break;
            case DEVICE_SET_DISCONNECT:
                startDisconnectCap(result);
                break;
            case DEVICE_SET_REMINDER_COLOR:
                setReminderColor((String) methodCall.arguments, result);
                break;
            case DEVICE_SET_DAILY_GOAL:
                setDailyGoal((Integer) methodCall.arguments, result);
                break;
            case DEVICE_SET_SILENT_MODE:
                setSilentMode((Boolean) methodCall.arguments, result);
                break;
            case DEVICE_SET_VIBRATION:
                setVibration((Boolean) methodCall.arguments, result);
                break;
            case DEVICE_SET_MULTI_CONFIGURATIONS:
                HashMap<String, Object> hashMap = (HashMap<String, Object>) methodCall.arguments;
                if (hashMap.containsKey("CALIBRATION_OFFSET")) {
                    setCalibrationToFlash(hashMap, result);
                } else {
                    setMultiConfiguration(hashMap, result);
                }
                break;
            case DEVICE_GET_MULTI_CONFIGURATIONS:
                getMultiConfiguration(result);
                break;
            case DEVICE_START_CALIBRATION:
                startCalibration((Integer) methodCall.arguments, result);
                break;
            case DEVICE_SET_MANUALLY_HYDRATION:
                setManuallyHydration((Integer) methodCall.arguments, result);
                break;
            case DEVICE_SET_EXTRA_DAILY_GOAL:
                setExtraDailyGoal((Integer) methodCall.arguments, result);
                break;
            case DEVICE_SET_HYDRATION_LEVEL:
                setHydrationLevel((Integer) methodCall.arguments, result);
                break;
            case DEVICE_GET_HYDRATION:
                getHydrations(result);
                break;
            case DEVICE_GET_EXTRA_DAILY_GOAL:
                getExtraDailyGoal(result);
                break;
            case DEVICE_GET_BASE_GOAL:
                getBaseGoal(result);
                break;
            case UPDATE_REMINDER_LEDS_PATTERN:
                updateLedPattern(result, (String) methodCall.arguments);
                break;
            case SIMULATE_REMINDER_PATTERN:
                simulateReminderPattern((HashMap<String, Object>) methodCall.arguments, result);
                break;
            case SIMULATION_VIBRATION:
                simulationVibration((Integer) methodCall.arguments, result);
                break;
            case DEVICE_CHECK_CAP_SYNC:
                checkCapSync((HashMap<String, Integer>) methodCall.arguments, result);
                break;
            case RESTORE_PAIRED_TO_CAP:
                restorePairedToCap(result, (String) methodCall.arguments);
                break;
            case GET_LAST_DB_HYDRATION:
                getLastDbHydrations(((Number) methodCall.arguments).longValue(), result);
                break;
            default:
                Log.e("DeviceScanCallback", "Error, not found case " + methodCall.method);
                break;
        }
    }

    private void getLastDbHydrations(Long lastIndexTimeStampFrom, MethodChannel.Result result) {
        HydrationDao hydrationDao = SDKLogsDatabase.getInstance(ModuleApp.getContext()).hydrationDao();
        List<HydrationModel> hydrationModels = hydrationDao.getAllHydrationFromIndex(lastIndexTimeStampFrom);
        JsonArray jsonArray = new JsonArray();

        if (hydrationModels == null) {
            SDKLog sdkLog = new SDKLog(LogsType.app, "getLastDbHydrations", lastIndexTimeStampFrom.toString(), "hydrationModels = null", "totalAmount = 0");
            new SDKLogsRepository().insert(sdkLog);
            result.success(jsonArray.toString());
            return;
        }

        int totalAmount = 0;
        for (int i = 0; i < hydrationModels.size(); i++) {
            HydrationModel model = hydrationModels.get(i);
            JsonObject json = new JsonObject();
            json.addProperty("id", model.getTimestampInsert());
            json.addProperty("eventTime", model.getEvent_time());
            json.addProperty("value", model.getAmount());
            totalAmount += model.getAmount();
            if (model.getLogic().contains("hydration")) {
                json.addProperty("type", "cap");
            } else {
                json.addProperty("type", model.getLogic().toLowerCase());
            }
            jsonArray.add(json);
        }

        SDKLog sdkLog = new SDKLog(LogsType.app, "getLastDbHydrations", lastIndexTimeStampFrom.toString(), "hydrationModels " + hydrationModels.size(), "totalAmount = " + totalAmount, jsonArray.toString());
        new SDKLogsRepository().insert(sdkLog);
        result.success(jsonArray.toString());
    }


    private void restorePairedToCap(MethodChannel.Result result, String macAddress) {
        boolean isValidAddress = macAddress != null && BluetoothAdapter.checkBluetoothAddress(macAddress);
        if (isValidAddress) {
            SharedPrefsModuleHelper.getInstance().setMacAddress(macAddress);
            SharedPrefsModuleHelper.getInstance().setIsPairedFromBootAndMissedFirstConfig(true);
        }
        result.success(isValidAddress);
    }

    private void checkCapSync(HashMap<String, Integer> args, MethodChannel.Result result) {
        try {
            new SimpleAsyncTask(() -> {
                int base_goal = args.get("base_goal");
                int extra_goal = args.get("extra_goal");
                int man_goal = args.get("man_goal");
                int hydration_measurement = args.get("hydration_measurement");

                mICapCommandsResponse = commandResponse -> {
                    if (commandResponse.getCommand().equals("GET_SYNC_INFO_COMMAND")) {
                        mWIOSmartDevice.removeListenerCapCallbacks(mICapCommandsResponse);
                        if (commandResponse.isSuccess()) {
                            JsonObject capJsonObject = new Gson().fromJson(commandResponse.getData(), JsonObject.class);

                            int cap_base_goal = capJsonObject.get("base_goal").getAsInt();
                            int cap_extra_daily_goal = capJsonObject.get("extra_daily_goal").getAsInt();
                            int cap_manually_hydration = capJsonObject.get("manually_hydration").getAsInt();
                            int cap_hydration_measurement = capJsonObject.get("hydration_measurement").getAsInt();

                            JsonObject appJson = new JsonObject();
                            appJson.addProperty("base_goal", base_goal);
                            appJson.addProperty("extra_daily_goal", extra_goal);
                            appJson.addProperty("manually_hydration", man_goal);
                            appJson.addProperty("hydration_measurement", hydration_measurement);

                            JsonObject objectReport = new JsonObject();
                            objectReport.add("app", appJson);
                            objectReport.add("cap", capJsonObject);

                            if (base_goal != cap_base_goal ||
                                    extra_goal != cap_extra_daily_goal ||
                                    man_goal != cap_manually_hydration ||
                                    hydration_measurement != cap_hydration_measurement) {
                                SDKLog sdkLog = new SDKLog(LogsType.APP_CAP_FAILURE_SYNC, objectReport.toString(), "", "Cap sync error", "", SharedPrefsModuleHelper.getInstance().getVersionDevice());
                                new SDKLogsRepository().insert(sdkLog);
                                sdkLog.setEventType(LogsType.app.name());
                                new ErrorSDK(sdkLog.toDashboardJson(), System.currentTimeMillis(), LogsType.app.name().toLowerCase()).sendRequest();
                            }
                        }
                    }
                };
                mWIOSmartDevice.registerListenerCapCallbacks(mICapCommandsResponse);
            }).execute();

            ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
                {
                    add(new GetSyncInfoCommand());
                }
            };
            setDirect(singleCommands, b -> result.success(b));

        } catch (Exception exception) {
            exception.getStackTrace();
            result.success(false);
        }

    }

    private void setCalibrationToFlash(HashMap<String, Object> offset, MethodChannel.Result result) {
        ArrayList<TypeConfig> typeConfigs = getTypeConfigFromFlutter(offset);
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetMultiConfigCommand(typeConfigs));
            }
        };
        setDirect(singleCommands, b -> result.success(b));
    }

    private void runSingleMeasurementCommand(MethodCall methodCall, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new RunSingleMeasurementNew());
            }
        };
        setDirect(singleCommands, b -> result.success(b));
    }

    private void simulateReminderPattern(HashMap<String, Object> args, MethodChannel.Result result) {
        if (args != null) {
            String colorAsString = args.get("color").toString().replaceAll("#", "");
            String[] colors = colorAsString.split(",");
            int pattern = (int) args.get("pattern");
            // TODO check the value is current
            ReminderLedsPatternType.ReminderPattern reminderPattern = ReminderLedsPatternType.ReminderPattern
                    .values()[pattern - 1];

            ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
                {
                    add(new SimulateReminderPatternCommand(colors, reminderPattern));
                }
            };
            setDirect(singleCommands, b -> result.success(null));
        } else {
            result.success(null);
        }
    }

    private void keepInternalLogWhenFail(boolean isSentCap, int value, String funcName) {
        if (isSentCap)
            return;

        SDKLog sdkLog = new SDKLog(LogsType.APP_CAP_FAILURE_SYNC, "fail send cmd cap sync", funcName, "value = " + value, "", "");
        new SDKLogsRepository().insert(sdkLog);
    }

    private void setManuallyHydration(int totalManuallyHydration, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetManuallyHydrationCommand(totalManuallyHydration));
                // add(new GetHydrationsCommand());
                // add(new GetMultiConfigCommand(
                // new ArrayList<TypeConfig>() {{
                // add(new DailyGoalType());
                // }}
                // ));
            }
        };

        setDirect(singleCommands, isSentCap -> {
            keepInternalLogWhenFail(isSentCap, totalManuallyHydration, "setManuallyHydration");
            result.success(isSentCap);
        });
    }


    private void setExtraDailyGoal(int extraGoal, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetExtraDailyGoalCommand(extraGoal));
                // add(new GetHydrationsCommand());
                // add(new GetMultiConfigCommand(
                // new ArrayList<TypeConfig>() {{
                // add(new DailyGoalType());
                // }}
                // ));
            }
        };

        setDirect(singleCommands, isSentCap -> {
            keepInternalLogWhenFail(isSentCap, extraGoal, "setExtraDailyGoal");
            result.success(isSentCap);
        });
    }

    private void setHydrationLevel(int hydrationLevel, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetHydrationLevelCommand(hydrationLevel));
                // add(new GetHydrationsCommand());
                // add(new GetMultiConfigCommand(
                // new ArrayList<TypeConfig>() {{
                // add(new DailyGoalType());
                // }}
                // ));
            }
        };

        setDirect(singleCommands, isSentCap -> {
            keepInternalLogWhenFail(isSentCap, hydrationLevel, "setHydrationLevel");
            result.success(isSentCap);
        });
    }

    private void simulationVibration(int duration, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new StartVibrationCommand(duration));
            }
        };
        setDirect(singleCommands, isSent -> result.success(isSent));
    }


    private void getHydrations(MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new GetHydrationsCommand());
            }
        };
        setDirect(singleCommands, isSent -> {
            result.success(0);// todo sent original value
        });
    }

    private void getExtraDailyGoal(MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new GetExtraDailyGoalCommand());
            }
        };
        setDirect(singleCommands, isSent -> {
            result.success(0);// todo sent original value
        });
    }

    private void getBaseGoal(MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetMultiConfigCommand(
                        new ArrayList<TypeConfig>() {
                            {
                                add(new DailyGoalType());
                            }
                        }));
            }
        };
        setDirect(singleCommands, isSent -> {
            result.success(0);// todo sent original value
        });
    }

    private void updateLedPattern(MethodChannel.Result result, String patternEnum) {
        ReminderLedsPatternType.ReminderPattern reminderPattern = ReminderLedsPatternType.ReminderPattern
                .valueOf(patternEnum);
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetMultiConfigCommand(
                        new ArrayList<TypeConfig>() {
                            {
                                add(new ReminderLedsPatternType(reminderPattern));
                            }
                        }));
            }
        };
        setDirect(singleCommands, isSent -> {
            result.success(0);// todo sent original value
        });
    }

    private void setSilentMode(Boolean isSilentMode, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetSilentModeCommand(isSilentMode));
            }
        };
        setDirect(singleCommands, b -> result.success(null));
    }

    private void setMultiConfiguration(HashMap<String, Object> arguments, MethodChannel.Result result) {
        boolean isImmediate = wizardScanner == null && isConnectingCap();
        ArrayList<TypeConfig> typeConfigs = getTypeConfigFromFlutter(arguments);

        // TODO load all types
        if (isImmediate) {
            ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
                {
                    add(new SetMultiConfigCommand(typeConfigs));
                }
            };
            setDirect(singleCommands, isSent -> {
                if (!isSent) {
                    CapConfig.getInstance().setPaddingTypeConfigCoMDS(typeConfigs);
                    handleFailDailyGoalFlash(arguments, isImmediate);
                }
                result.success(null);
            });
        } else {
            CapConfig.getInstance().setPaddingTypeConfigCoMDS(typeConfigs);
            handleFailDailyGoalFlash(arguments, isImmediate);
        }
    }

    private void handleFailDailyGoalFlash(HashMap<String, Object> arguments, boolean isImmediate) {
        if (arguments.containsKey("DAILY_GOAL")) {
            TypeConfig config = (TypeConfig) arguments.get("DAILY_GOAL");
            if (config == null)
                return;
            String goalAsString = config.getDataSend();
            if (goalAsString == null)
                return;

            int goal = Integer.parseInt(goalAsString);
            keepInternalLogWhenFail(false, goal, "setMultiConfiguration " + "isImmediate = " + isImmediate);
        }
    }

    private boolean isConnectingCap() {
        Map.Entry<String, Boolean> entry = mWIOSmartDevice.getConnectionDeviceLiveData().getValue();
        return entry != null && entry.getValue();
    }

    private void getMultiConfiguration(MethodChannel.Result result) {
        result.success(null);// todo sent original value
    }

    private void startCalibration(int offset, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetMultiPointOffsetCalibrationCommand(offset));
            }
        };
        setDirect(singleCommands, isSuccess -> result.success(isSuccess));
    }

    private ArrayList<TypeConfig> getTypeConfigFromFlutter(HashMap<String, Object> arguments) {
        ArrayList<TypeConfig> typeConfigs = new ArrayList<>();

        final Iterator<Map.Entry<String, Object>> it = arguments.entrySet().iterator();
        while (it.hasNext()) {
            TypeConfig typeConfig = null;
            Map.Entry<String, Object> pair = (Map.Entry<String, Object>) it.next();
            Log.v("config-flutter", "Item: " + pair.getKey());
            switch (pair.getKey()) {
                case "IS_REGULAR_REMINDER":
                case "REMINDER_LOGIC":
                    boolean isRegular = (boolean) pair.getValue();
                    typeConfig = new ReminderLogicType(isRegular);
                    break;
                case "MBR":
                    int mbr = (int) pair.getValue();
                    typeConfig = new MBRType(mbr);
                    break;
                case "IS_LIGHTS_OFF_WHILE_CHARGING":
                    boolean isLightsWhileCharging = (boolean) pair.getValue();
                    typeConfig = new EnableLedOffInChargerType(isLightsWhileCharging);
                    break;
                case "ENABLE_SHABBAT_MODE":
                    boolean isShabbatModeOn = (boolean) pair.getValue();
                    typeConfig = new EnableShabatModeType(isShabbatModeOn);
                    break;
                case "IS_LED_OFF_OUTSIDE_ACTIVE_TIME":
                    boolean isOff = (boolean) pair.getValue();
                    typeConfig = new LedOffOutsideActiveTimeType(isOff);
                    break;
                case "ENABLE_DEMO_MODE":
                    boolean isDemoOn = (boolean) pair.getValue();
                    typeConfig = new EnableDemoModeType(isDemoOn);
                    break;
                case "DEMO_MODE_LED_TIMEOUT":
                    int durationSec = (int) pair.getValue();
                    typeConfig = new DemoModeLedIntervalType(durationSec);
                    break;
                case "REMINDER_UI_ELEMENT":
                    ArrayList<Boolean> flags = (ArrayList<Boolean>) pair.getValue();
                    typeConfig = new ReminderUIElementsType(flags.get(0), flags.get(1), flags.get(2));
                    break;
                case "START_TIME":
                    int start = (int) pair.getValue();
                    typeConfig = new StartTimeType(start);
                    break;
                case "END_TIME":
                    int end = (int) pair.getValue();
                    typeConfig = new EndTimeType(end);
                    break;
                case "DAILY_GOAL":
                    int goal = (int) pair.getValue();
                    typeConfig = new DailyGoalType(goal);
                    break;
                case "DAR":
                    boolean DAR = (boolean) pair.getValue();
                    typeConfig = new DARType(DAR);
                    break;
                case "DBR":
                    boolean DBR = (boolean) pair.getValue();
                    typeConfig = new DBRType(DBR);
                    break;
                case "REMINDER_COLOR":
                    String colorAsString = pair.getValue().toString().replaceAll("#", "");
                    String[] colors = colorAsString.split(",");
                    typeConfig = new ReminderLedsColorType(colors);
                    break;
                case "ENABLE_LEDS_OPEN_CLOSE":
                    boolean isEnable = (boolean) pair.getValue();
                    typeConfig = new EnableLedOpenCloseType(isEnable);
                    break;
                case "REMINDER_PATTERN":
                    String pattern = (String) pair.getValue();
                    ReminderLedsPatternType.ReminderPattern reminderPattern = ReminderLedsPatternType.ReminderPattern
                            .valueOf(pattern);
                    typeConfig = new ReminderLedsPatternType(reminderPattern);
                    break;
                case "ENABLE_HYDRATION_STATUS_INDICATOR":
                    boolean isStatusLedOn = (boolean) pair.getValue();
                    typeConfig = new EnableLedStatusIndicatorType(isStatusLedOn);
                    break;
                case "ENABLE_REMINDER_LED_INDICATOR":
                    boolean isReminderLedOn = (boolean) pair.getValue();
                    typeConfig = new EnableReminderLedType(isReminderLedOn);
                    break;
                case "REMINDER_ROUNDS":
                    int rounds = (int) pair.getValue();
                    typeConfig = new ReminderRoundType(rounds);
                    break;
                case "REMINDER_CYCLE":
                    int cycle = (int) pair.getValue();
                    typeConfig = new ReminderCyclesType(cycle);
                    break;
                case "CALIBRATION_OFFSET":
                    int offset = (int) pair.getValue();
                    typeConfig = new CalibrationOffsetType(offset);
                    break;
            }
            if (typeConfig != null)
                typeConfigs.add(typeConfig);
            System.out.println(pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
        return typeConfigs;
    }

    private void handleNotMyCap(Boolean isAddToBlackList, MethodChannel.Result result) {
        if (wizardScanner != null) {
            if (!isAddToBlackList) {
                wizardScanner.removeDeviceFromBlackList();
            } else {
                wizardScanner.onNoClicked();
            }
        }
        mLastKnownMacAddress = null;
        result.success(null);
    }

    private void setVibration(Boolean isVibrate, MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetMultiConfigCommand(
                        new ArrayList<TypeConfig>() {
                            {
                                add(new ReminderUIElementsType(true, true, isVibrate));
                            }
                        }));
            }
        };
        setDirect(singleCommands, b -> result.success(null));

    }

    private void setDailyGoal(Integer args, MethodChannel.Result result) {
        if (args != null) {
            int dailyGoal = args;
            ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
                {
                    add(new SetMultiConfigCommand(
                            new ArrayList<TypeConfig>() {
                                {
                                    add(new DailyGoalType(dailyGoal));
                                }
                            }));
                }
            };
            setDirect(singleCommands, b -> result.success(null));
        } else {
            result.success(null);
        }
    }

    private void setReminderColor(String reminderColor, MethodChannel.Result result) {
        String color = reminderColor.substring(1);
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new SetMultiConfigCommand(
                        new ArrayList<TypeConfig>() {
                            {
                                add(new ReminderLedsColorType(color));
                            }
                        }));
            }
        };
        setDirect(singleCommands, b -> result.success(null));
    }

    private void startDisconnectCap(MethodChannel.Result result) {
        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(new DisconnectDeviceCommand());
            }
        };
        setDirect(singleCommands, b -> result.success(null));
    }

    private void setLedColor(HashMap<String, Object> args, MethodChannel.Result result) {
        if (args != null) {
            String colorAsString = args.get("color").toString().replaceAll("#", "");
            String[] colors = colorAsString.split(",");
            int duration = (int) args.get("duration");

            Log.d("TestDu", "Color: " + colorAsString + " , Duration: " + duration);

            ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
                {
                    add(new TurnLedsOnCommand(colors, duration));
                }
            };
            setDirect(singleCommands, b -> result.success(null));
        } else {
            result.success(null);
        }
    }

    private void setDirect(ArrayList<SingleCommand> listCommands, CallbackDirectCommand callbackResult) {
        mWIOSmartDevice.setDirectCommands(listCommands, isSent -> {
            String res = "";
            if (isSent)
                res = "The command was sent successfully";
            else
                res = "Fail";
            Log.d("TestDu", "Is connecting? " + res);
            if (callbackResult != null) {
                callbackResult.onCapSent(isSent);
            }
        });
    }

    private void handleGetLastSyncTimestamp(MethodCall methodCall, MethodChannel.Result result) {
        long timestamp = (long) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.LONG,
                DEVICE_LAST_SYNC_TIMESTAMP);
        result.success(timestamp);
    }

    private void handleStartScanReq(MethodCall methodCall, MethodChannel.Result result) {
        boolean scanIsStarted = mWIOSmartDevice.startScan(scanRequest -> {
            switch (scanRequest) {
                case MISSING_BACKGROUND_LOCATION_PERMISSION:
                case MISSING_LOCATION_PERMISSION:
                case MISSING_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                    result.error("error", "CHUNK_PERMISSIONS_ARE_MISSING", null);
                    break;
                case DENIED_BACKGROUND_LOCATION_PERMISSION:
                case DENIED_LOCATION_PERMISSION:
                case DENIED_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                    result.error("error", "CHUNK_PERMISSIONS_ARE_DENIED", null);
                    break;
                case MODULE_OFF:
                    result.error("error", "MODULE_IS_OFF", null);
                    break;
                case ALREADY_SCANNING:
                    result.success(null);
                    Log.v("BLESCAN", "already startScan");
                    break;
            }
        });

        if (scanIsStarted) {
            result.success(null);
            Log.v("BLESCAN", "startScan");
        }
    }

    private void handleUpdateRssiConfig(HashMap<String, Object> args, MethodChannel.Result result) {
//        mWIOSmartDevice.updateRssiConfig((boolean) args.get("activate"),
//                (int) args.get("rssi"), -100);
        result.success(null);
    }

    private void handleSetDeviceReminders(ArrayList<HashMap<String, Integer>> args, MethodChannel.Result result) {
        Reminder[] reminders = null;

        if (args != null) {
            reminders = new Reminder[args.size()];

            for (int i = 0; i < args.size(); i++) {
                HashMap<String, Integer> reminderHM = args.get(i);
                reminders[i] = new Reminder(reminderHM.get("hours"), reminderHM.get("minutes"));
            }
        }

        ReminderManager.getInstance().cap().setReminders(reminders);

        result.success(null);
    }

    private void handleUpdateDailyUsageTime(HashMap<String, Object> args, MethodChannel.Result result) {
        if (args != null) {
            int fromHour = (int) args.get("FROM_HOUR");
            int toHour = (int) args.get("TO_HOUR");
            int fromMinutes = (int) args.get("FROM_MINUTES");
            int toMinutes = (int) args.get("TO_MINUTES");
            boolean isConfig = (boolean) args.get("IS_CONFIGURATION");
            if (isConfig) {
                ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
                    {
                        add(new SetMultiConfigCommand(
                                new ArrayList<TypeConfig>() {
                                    {
                                        add(new StartTimeType(fromHour));
                                        add(new EndTimeType(toHour));
                                    }
                                }));
                    }
                };
                setDirect(singleCommands, b -> result.success(null));
            } else {
                CapConfig.getInstance().updateDailyUsage(fromHour, fromMinutes, toHour, toMinutes);
                result.success(null);
            }
        }
    }

    private void handleCommitBlinkSequence(Integer type, MethodChannel.Result result) {
        if (mWIOSmartDevice.getMacAddress().isEmpty()) {
            return;
        }

        StartBlinkCommand startBlinkCommand;

        if (type != null) {
            StartBlinkCommand.BlinkingType blinkingType = type == 1 ? StartBlinkCommand.BlinkingType.GOOD_STATUS
                    : StartBlinkCommand.BlinkingType.BED_STATUS;

            startBlinkCommand = new StartBlinkCommand(blinkingType);
        } else {
            startBlinkCommand = new StartBlinkCommand();
        }

        ArrayList<SingleCommand> singleCommands = new ArrayList<SingleCommand>() {
            {
                add(startBlinkCommand);
            }
        };
        setDirect(singleCommands, isSuccess -> {
            Log.v("BLINK", isSuccess + "");
            result.success(null);
        });
    }

    private void startScanningWizardMode(MethodCall methodCall, MethodChannel.Result result) {
        ProceduresEnum proceduresEnum = ProceduresEnum.valueOf((String) methodCall.arguments);

        wizardScanner = new HoldOnWizardScanner(mBinaryMessenger, mWIOSmartDevice,
                mLifecycleOwner, this,
                proceduresEnum.getProcedureInstance(), mDeviceLogEventsUnifier);

        boolean scanIsStarted = wizardScanner.startScan(scanRequest -> {
            switch (scanRequest) {
                case MISSING_BACKGROUND_LOCATION_PERMISSION:
                case MISSING_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                case MISSING_LOCATION_PERMISSION:
                    result.error("error", "CHUNK_PERMISSIONS_ARE_MISSING", null);
                    break;
                case DENIED_BACKGROUND_LOCATION_PERMISSION:
                case DENIED_BLUETOOTH_SCAN_AND_CONNECT_PERMISSION:
                case DENIED_LOCATION_PERMISSION:
                    result.error("error", "CHUNK_PERMISSIONS_ARE_DENIED", null);
                    break;
                case MODULE_OFF:
                    result.error("error", "MODULE_IS_OFF", null);
                    break;
            }
        });

        if (scanIsStarted) {
            result.success(null);
        }
    }

    private void handleGetDeviceData(MethodChannel.Result result) {
        String deviceMacAddress = mWIOSmartDevice.getMacAddress();

        if (deviceMacAddress == null || deviceMacAddress.isEmpty()) {
            deviceMacAddress = mLastKnownMacAddress;
        }

        String capVersion;
        if (wizardScanner != null && wizardScanner.getCapVersion() != null) {
            capVersion = wizardScanner.getCapVersion();
            Log.d("handleGetDeviceData", capVersion + " , if");
        } else {
            capVersion = mWIOSmartDevice.getDeviceVersion();
            Log.d("handleGetDeviceData", capVersion + " , else");
        }

        HashMap<String, String> map = new HashMap<>();

        map.put("mac_address", deviceMacAddress);
        map.put("cap_version", capVersion);

        result.success(map);
    }

    @Override
    public void onNewDetectedMacAddress(String mac) {
        mLastKnownMacAddress = mac;
    }

    private void updateLastSyncDeviceTimestamp() {
        SharedPrefsModuleHelper.getInstance().setStoreData(StoreType.LONG, System.currentTimeMillis(),
                DEVICE_LAST_SYNC_TIMESTAMP);
    }

    public void forceResetAllCache() {
        Log.v("Plugin-wizard", "forceResetAllCache()");
        wizardScanner = null;
        mWIOSmartDevice.stopScan();
        mWIOSmartDevice.forgetDevice();
        mLastKnownMacAddress = null;
    }
}

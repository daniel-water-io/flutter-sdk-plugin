package io.water.sdk_plugin.engine.channels.callbacks;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.water.water_io_sdk.network.interfaces.OnResult;
import com.water.water_io_sdk.network.networks.WIONetwork;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;
import io.water.sdk_plugin.interfaces.ILogout;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_LOGIN_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_PROFILE_UPDATE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_REGISTER_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_LOG_OUT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_RESET_PASSWORD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_SOCIAL_LOGIN;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class AuthCallbackChannel extends CallbackChannel {

    private final ILogout mILogout;

    public AuthCallbackChannel(BinaryMessenger binaryMessenger, ILogout callback) {
        super(binaryMessenger, AUTH_CHANNEL);
        this.mILogout = callback;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case AUTH_REGISTER_METHOD:
                handleRegisterMethodCall(methodCall, result);
                break;
            case AUTH_LOGIN_METHOD:
                handleLoginMethodCall(methodCall, result);
                break;
            case AUTH_PROFILE_UPDATE:
                handleUserProfileUpdate(methodCall, result);
                break;
            case AUTH_LOG_OUT:
                logOut(result);
                break;
            case AUTH_SOCIAL_LOGIN:
                socialLogin((Map<String, Object>) methodCall.arguments, result);
                break;
            case AUTH_RESET_PASSWORD:
                getAuthResetPassword((String) methodCall.arguments, result);
                break;
        }
    }

    private void getAuthResetPassword(String pass, MethodChannel.Result result) {
        new WIONetwork.Api().appForgotPassword(pass, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(true);
            }

            @Override
            public void onError(String msg, int code) {
                result.error("15",  msg, msg);
            }
        });
    }

    private void socialLogin(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);

        new WIONetwork.Api().appExternalLogin(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String msg, int code) {
                result.error("15",  msg, msg);
            }
        });
    }

    private void logOut(MethodChannel.Result result) {
        new WIONetwork.Api().appLogout(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                if (mILogout != null) {
                    mILogout.onLogoutDone();
                }
                new Handler(Looper.getMainLooper()).postDelayed(() -> result.success(true),1000);
            }

            @Override
            public void onError(String s, int errorCode) {
                if (errorCode == 10) {
                    result.error("15", s, errorCode);
                    //no internet connection, cache still exist!
                    // fail logout
                } else {
                    //some other error from server like 401 code,cache removed!
                    result.error("16", s, errorCode);
                    if (mILogout != null) {
                        mILogout.onLogoutDone();
                    }
                }
            }
        });
    }

    private void handleRegisterMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        Map<String, Object> map = (Map<String, Object>) methodCall.arguments;
        String email = (String) map.get("email");
        String password = (String) map.get("password");

        new WIONetwork.Api().appRegister(email, password, true, false, new OnResult<JsonObject>() {

            @Override
            public void onSuccess(JsonObject body, String message, int code) {
                result.success(message);
            }

            @Override
            public void onError(String msg, int code) {
                result.error("15",  msg, msg);
            }
        });
    }

    private void handleLoginMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        Map<String, Object> map = (Map<String, Object>) methodCall.arguments;
        String email = (String) map.get("email");
        String password = (String) map.get("password");


        //TODO
        new WIONetwork.Api().appLogin(email, password, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject body, String message, int code) {
                result.success(message);
            }

            @Override
            public void onError(String msg, int code) {
//                Log.v("NET",msg+",  "+code);
                result.error("15",  msg, msg);
            }
        });
    }

    private void handleUserProfileUpdate(MethodCall methodCall, MethodChannel.Result result) {

        new WIONetwork.Api().updateUserProfile(getJsonObjFromArgs((Map<String, Object>) methodCall.arguments), new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String msg, int code) {
                result.error("15",  msg, msg);
            }
        });
    }

    private JsonObject getJsonObjFromArgs(Map<String, Object> args) {
        return (new Gson()).fromJson((new Gson()).toJson(args), JsonObject.class);
    }
}
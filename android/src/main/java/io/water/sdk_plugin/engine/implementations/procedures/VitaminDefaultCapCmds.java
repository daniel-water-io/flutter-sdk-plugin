package io.water.sdk_plugin.engine.implementations.procedures;
import com.water.water_io_sdk.ble.connection.command.SetRealTimeCommand;
import com.water.water_io_sdk.ble.connection.command.SingleCommand;
import com.water.water_io_sdk.ble.connection.command.set_vitamins_reminder.SetVitaminReminderCommand;
import com.water.water_io_sdk.ble.connection.command.set_vitamins_reminder.SetVitaminsReminderCommand;
import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractDefaultConnection;
import com.water.water_io_sdk.ble.utilities.CapConfig;
import com.water.water_io_sdk.reminder_local_push.entities.Reminder;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;

import io.water.sdk_plugin.utils.Constants;

public class VitaminDefaultCapCmds extends ProcedureAbstractDefaultConnection {

    @Override
    public ArrayList<SingleCommand> initCommands() {
        ArrayList<SingleCommand> listCommands = new ArrayList<>();
        listCommands.add(new SetRealTimeCommand());

        SharedPrefsModuleHelper helper = SharedPrefsModuleHelper.getInstance();
        if (CapConfig.getInstance().isRemindersChange()) {
            boolean isOldVersion = (boolean) helper.getStoreData(StoreType.BOOLEAN, Constants.IS_OLD_VERSIONS_CAP);
            if (isOldVersion) {
                Reminder reminder = helper.getRemindersCmdList()[0];
                listCommands.add(new SetVitaminReminderCommand(reminder.getHour(), reminder.getMinutes()));
            } else {
                listCommands.add(new SetVitaminsReminderCommand());
            }
        }
        
//        if (!(boolean) helper.getStoreData(StoreType.BOOLEAN, "DAILY_USAGE_ONE_TIME_FIX")) {
//            listCommands.add(new SetDailyUsageTimeCommand(3, 0, 2, 59));
//        }

        return listCommands;
    }
}

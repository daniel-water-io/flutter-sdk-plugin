package io.water.sdk_plugin.engine.channels.observers;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION_CHANNEL;

import com.google.gson.JsonObject;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class WizardScanOnPotentialCapConnectedObserver extends ChannelObserver<Map.Entry<String, Boolean>> {

    public WizardScanOnPotentialCapConnectedObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData observable) {
        super(binaryMessenger, DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION_CHANNEL, DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION, owner, observable);
    }

    @Override
    public void onChanged(Map.Entry<String, Boolean> entry) {
        if (entry != null) {
            String macAddress = entry.getKey();
            boolean isInBoot = entry.getValue();
            Map<String, Object> map = new HashMap<>();
            map.put("macAddress", macAddress);
            map.put("isBootMode", isInBoot);
//            android.util.Log.v("DANIUELSFAAS", "onChanged()" + macAddress + " , isBootMode: " + isInBoot);
            invokeTarget(map);
        } else {
            android.util.Log.e("WizardScan", "onChanged() NULL" );
//            invokeTarget(null);
        }
    }
}

package io.water.sdk_plugin.engine.event_channel.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.util.HashMap;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;

public abstract class BaseEventChannel implements EventChannel.StreamHandler {

    private EventChannel.EventSink mEventSink;
    private final static String KEY_MESSAGE = "KEY_MESSAGE";


    public BaseEventChannel(BinaryMessenger binaryMessenger, String name) {
        EventChannel eventChannel = new EventChannel(binaryMessenger, name);
        eventChannel.setStreamHandler(this);
    }

    @Override
    public void onListen(Object arguments, EventChannel.EventSink events) {
        this.mEventSink = events;
    }

    @Override
    public void onCancel(Object arguments) {
        this.mEventSink = null;
    }

    public void sinkData(HashMap<String, Object> mapData) {
        Message message = mHandlerSink.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_MESSAGE, mapData);
        message.setData(bundle);
        mHandlerSink.sendMessage(message);
    }

    private final Handler mHandlerSink = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (mEventSink == null)
                return;
            Bundle bundle = msg.getData();
            HashMap<String, Object> mapData = (HashMap<String, Object>) bundle.getSerializable(KEY_MESSAGE);
            mEventSink.success(mapData);
        }
    };

}

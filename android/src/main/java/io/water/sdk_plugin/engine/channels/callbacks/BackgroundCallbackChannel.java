package io.water.sdk_plugin.engine.channels.callbacks;

import android.app.ActivityManager;

import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.entities.DeviceLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.PluginConfiguration;
import io.water.sdk_plugin.background.database.repositories.BaseLogsRepository;
import io.water.sdk_plugin.background.database.repositories.HydrationRepository;
import io.water.sdk_plugin.background.database.repositories.VitaminsRepository;
import io.water.sdk_plugin.background.entities.DeviceLogDB;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;

import static android.content.Context.ACTIVITY_SERVICE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.BACKGROUND_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.BACKGROUND_SYNC_ON_START_APP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CLEAR_ALL_BACKGROUND_DB;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CLEAR_APP_DATA;

public class BackgroundCallbackChannel extends CallbackChannel {

    private final PluginConfiguration mPluginConfiguration;

    public BackgroundCallbackChannel(BinaryMessenger binaryMessenger, PluginConfiguration pluginConfiguration) {
        super(binaryMessenger, BACKGROUND_CHANNEL);
        this.mPluginConfiguration = pluginConfiguration;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case BACKGROUND_SYNC_ON_START_APP:
                handleSyncOnStartReq(methodCall, result);
                break;
            case CLEAR_ALL_BACKGROUND_DB:
                handleClearAllDbLogs(result);
                break;
            case CLEAR_APP_DATA:
                handleClearAppData(result);
                break;
        }
    }

    private void handleClearAppData(MethodChannel.Result result) {
        ((ActivityManager) ModuleApp.getContext().getSystemService(ACTIVITY_SERVICE))
                .clearApplicationUserData();
        result.success(null);
    }

    private void handleClearAllDbLogs(MethodChannel.Result result) {
        switch (mPluginConfiguration.appGenes) {
            default:
                 break;
            case Hydration:
                HydrationRepository.getInstance().deleteAllLogs();
                break;
            case Vitamins:
                VitaminsRepository.getInstance().deleteAllLogs();
                break;
        }


        VitaminsRepository.getInstance().deleteAllLogs();
        result.success(null);
    }

    //TODO handle hydration instance dinaminc
    private void handleSyncOnStartReq(MethodCall methodCall, MethodChannel.Result result) {

        ArrayList<DeviceLogDB> missedEvents;

        switch (mPluginConfiguration.appGenes) {
            default:
                missedEvents = new ArrayList<>();
                break;
            case Hydration:
                missedEvents = HydrationRepository.getInstance().getAllHydrationLogsList();
                break;
            case Vitamins:
                missedEvents = VitaminsRepository.getInstance().getAllMissedCloseLogsList();
                break;
        }


        if (BaseLogsRepository.getInstance(ModuleApp.getContext()).isCapOpen()) {
            DeviceLogDB deviceLogOpen = new DeviceLogDB("O", System.currentTimeMillis());
            missedEvents.add(deviceLogOpen);
        }

        ArrayList<Map<String, Object>> res = new ArrayList<>();

        for (int i = 0; i < missedEvents.size(); i++) {
            DeviceLog deviceLog = missedEvents.get(i);

            Map<String, Object> map = new HashMap<>();

            map.put("event_type", deviceLog.getEventType());
            map.put("timestamp", deviceLog.getTimeStamp());
            map.put("measurement", deviceLog.getMeasurement());
            map.put("extra_data", deviceLog.getExtraData());
            map.put("extra_data2", deviceLog.getExtraData2());
            map.put("cap_mac_address", deviceLog.getMacAddress());
            map.put("fromBackground", true);

            res.add(map);
        }

        result.success(res);
    }
}

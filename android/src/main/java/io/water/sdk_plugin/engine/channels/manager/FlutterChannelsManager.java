package io.water.sdk_plugin.engine.channels.manager;

import android.app.Activity;

import com.water.water_io_sdk.ble.entities.WIOSmartDevice;
import com.water.water_io_sdk.ble.interfaces.IWIOApp;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.PluginConfiguration;
import io.water.sdk_plugin.background.utilities.CapLoggerHandler;
import io.water.sdk_plugin.engine.channels.callbacks.AuthCallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.BackgroundCallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.DeviceScanCallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.LocalPushNotificationsCallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.NetworkCallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.OTACallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.PermissionsCallbackChannel;
import io.water.sdk_plugin.engine.channels.callbacks.PluginMessengerCallbackChannel;
import io.water.sdk_plugin.engine.channels.observers.CapConfigChannelObserver;
import io.water.sdk_plugin.engine.channels.observers.CapConnectionStatusChannelObserver;
import io.water.sdk_plugin.engine.channels.observers.CapVersionChannelObserver;
import io.water.sdk_plugin.engine.channels.observers.NetworkObserver;
import io.water.sdk_plugin.engine.channels.observers.PushNotificationObserver;
import io.water.sdk_plugin.engine.channels.observers.UnauthenticatedObserver;
import io.water.sdk_plugin.interfaces.ILogout;
import io.water.sdk_plugin.utils.DeviceLogEventsUnifier;

public class FlutterChannelsManager implements NetworkCallbackChannel.CallbackGetActivity {

    private final IWIOApp mWioApp;
    private final BinaryMessenger mBinaryMessenger;
    private final LifecycleOwner mLifecycleOwner;
    private PermissionsCallbackChannel mPermissionsCallbackChannel;
    private final PluginConfiguration mPluginConfiguration;
    private final Activity mActivity;

    public FlutterChannelsManager(IWIOApp wioApp, BinaryMessenger binaryMessenger, LifecycleOwner lifecycleOwner, PluginConfiguration pluginConfiguration, Activity activity) {
        this.mWioApp = wioApp;
        this.mBinaryMessenger = binaryMessenger;
        this.mLifecycleOwner = lifecycleOwner;
        this.mPluginConfiguration = pluginConfiguration;
        this.mActivity = activity;
        initChannels();
    }

    private void initChannels() {
        WIOSmartDevice wioSmartDevice = mWioApp.getWIOSmartDevice();
        OTACallbackChannel otaCallbackChannel = new OTACallbackChannel(mBinaryMessenger, wioSmartDevice, mActivity, mLifecycleOwner);
        DeviceLogEventsUnifier logEventsUnifier = new DeviceLogEventsUnifier(mLifecycleOwner, wioSmartDevice);

        // CALL BACKS
        DeviceScanCallbackChannel deviceScanCallbackChannel = new DeviceScanCallbackChannel(mBinaryMessenger, wioSmartDevice, mLifecycleOwner, logEventsUnifier);
        new AuthCallbackChannel(mBinaryMessenger, new ILogout() {
            @Override
            public void onLogoutDone() {
                deviceScanCallbackChannel.forceResetAllCache();
                otaCallbackChannel.forceResetAllCache();
            }
        });
        new NetworkCallbackChannel(mBinaryMessenger,this);
        mPermissionsCallbackChannel = new PermissionsCallbackChannel(mBinaryMessenger, mWioApp);
        new LocalPushNotificationsCallbackChannel(mBinaryMessenger);
        new BackgroundCallbackChannel(mBinaryMessenger, mPluginConfiguration);
        new PluginMessengerCallbackChannel(mBinaryMessenger);
        // OBSERVERS
        CapLoggerHandler.getInstance(mBinaryMessenger, mLifecycleOwner, logEventsUnifier.mUnifiedLiveData, wioSmartDevice.getLiveDataBatteryPercent());
        new CapConnectionStatusChannelObserver(mBinaryMessenger, mLifecycleOwner, wioSmartDevice.getConnectionDeviceLiveData());
        new CapVersionChannelObserver(mBinaryMessenger, mLifecycleOwner, wioSmartDevice.getCommandResponseLiveData());
        new CapConfigChannelObserver(mBinaryMessenger, mLifecycleOwner, wioSmartDevice.getCommandResponseLiveData());
        new PushNotificationObserver(mBinaryMessenger, mLifecycleOwner);
        new NetworkObserver(mBinaryMessenger, mLifecycleOwner);
        new UnauthenticatedObserver(mBinaryMessenger, mLifecycleOwner);
    }

    public PermissionsCallbackChannel getPermissionsCallbackChannel() {
        return mPermissionsCallbackChannel;
    }

    public void destroyAll() {
        // TODO!
    }

    @Override
    public Activity getParentActivity() {
        return mActivity;
    }
}

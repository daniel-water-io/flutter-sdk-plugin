package io.water.sdk_plugin.engine.implementations.procedures;

import com.water.water_io_sdk.ble.connection.command.ClearDataCommand;
import com.water.water_io_sdk.ble.connection.command.HoldOnCommand;
import com.water.water_io_sdk.ble.connection.command.SetDailyUsageTimeCommand;
import com.water.water_io_sdk.ble.connection.command.SingleCommand;
import com.water.water_io_sdk.ble.connection.command.StartBlinkCommand;
import com.water.water_io_sdk.ble.connection.command.TrySetTimeBlinkCommand;
import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractFirstConnection;

import java.util.ArrayList;

import io.water.sdk_plugin.utils.Constants;


public class SDKBritaProcedureCmds extends ProcedureAbstractFirstConnection {

    @Override
    public ArrayList<SingleCommand> initCommands() {
        ArrayList<SingleCommand> listCommands = new ArrayList<>();
        listCommands.add(new TrySetTimeBlinkCommand(5));
        listCommands.add(new StartBlinkCommand());
        listCommands.add(new HoldOnCommand(Constants.HOLD_ON_KEY));
        listCommands.add(new ClearDataCommand());
        listCommands.add(new SetDailyUsageTimeCommand(7, 23));
        return listCommands;
    }

}

package io.water.sdk_plugin.engine.channels;

public class ChannelsConsts {

    public static final String OTA_CHANNEL = "OTA_CHANNEL";
    public static final String START_OTA_METHOD = "START_OTA_METHOD";

    public static final String PLUGIN_CONFIG_CHANNEL = "PLUGIN_CONFIG_CHANNEL";
    public static final String CONFIG_PARAMS_METHOD = "CONFIG_PARAMS_METHOD";

    public static final String PUSH_NOTIFICATIONS_CHANNEL = "PUSH_NOTIFICATIONS_CHANNEL";
    public static final String CAP_STATE_CHANNEL_PATH = "CAP_STATE_CHANNEL_PATH";
    public static final String CAP_STATE_CHANNEL_TARGET_METHOD = "CAP_STATE_CHANNEL_TARGET_METHOD";

    public static final String CAP_CONNECTION_STATUS_CHANNEL = "CAP_CONNECTION_STATUS_CHANNEL";
    public static final String CAP_CONNECTION_STATUS_METHOD = "CAP_CONNECTION_STATUS_METHOD";
    public static final String CAP_STATE_VERSION_METHOD = "CAP_STATE_VERSION_METHOD";

    public static final String CAP_STATE_BATTREY_STATE_METHOD = "CAP_STATE_BATTREY_STATE_METHOD";
    public static final String DEVICE_CHECK_CAP_SYNC = "DEVICE_CHECK_CAP_SYNC";
    public static final String RESTORE_PAIRED_TO_CAP = "RESTORE_PAIRED_TO_CAP";
    public static final String GET_LAST_DB_HYDRATION = "GET_LAST_DB_HYDRATION";
    public static final String CAP_CONFIGURATIONS_CHANNEL = "CAP_CONFIGURATIONS_CHANNEL";
    public static final String CAP_CONFIGURATIONS_CHANGED_METHOD = "CAP_CONFIGURATIONS_CHANGED_METHOD";

    public static final String DEVICE_SCAN_CALLBACK_CHANNEL = "DEVICE_SCAN_CALLBACK_CHANNEL";
    public static final String DEVICE_SCAN_CALLBACK_START_SCAN = "DEVICE_SCAN_CALLBACK_START_SCAN";
    public static final String REGISTER_DEVICE_SCAN_WHEN_ENABLE = "REGISTER_DEVICE_SCAN_WHEN_ENABLE";
    public static final String DEVICE_SCAN_CALLBACK_STOP_SCAN = "DEVICE_SCAN_CALLBACK_STOP_SCAN";
    public static final String DEVICE_SCAN_CALLBACK_FORGET_CAP = "DEVICE_SCAN_CALLBACK_FORGET_CAP";
    public static final String DEVICE_SCAN_INVOKE_WIZARD_START_SCAN = "DEVICE_SCAN_INVOKE_WIZARD_START_SCAN";
    public static final String DEVICE_SCAN_WIZARD_YES_MY_CAP = "DEVICE_SCAN_WIZARD_YES_MY_CAP";
    public static final String DEVICE_SCAN_WIZARD_NOT_MY_CAP = "DEVICE_SCAN_WIZARD_NOT_MY_CAP";
    public static final String DEVICE_GET_DEVICE_DATA = "DEVICE_GET_DEVICE_DATA";
    public static final String DEVICE_COMMIT_BLINK_SEQUENCE = "DEVICE_COMMIT_BLINK_SEQUENCE";
    public static final String DEVICE_SET_REMINDERS = "DEVICE_SET_REMINDERS";
    public static final String DEVICE_SET_LED_COLOR = "DEVICE_SET_LED_COLOR";
    public static final String DEVICE_UPDATE_DAILY_USAGE = "DEVICE_UPDATE_DAILY_USAGE";
    public static final String DEVICE_UPDATE_RSSI_CONFIG = "DEVICE_UPDATE_RSSI_CONFIG";
    public static final String DEVICE_LAST_SYNC_TIMESTAMP = "DEVICE_LAST_SYNC_TIMESTAMP";

    public static final String DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION_CHANNEL = "DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION_CHANNEL";
    public static final String DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION = "DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION";

    public static final String AUTH_CHANNEL = "AUTH_CHANNEL";
    public static final String AUTH_REGISTER_METHOD = "AUTH_REGISTER_METHOD";
    public static final String AUTH_LOGIN_METHOD = "AUTH_LOGIN_METHOD";
    public static final String AUTH_PROFILE_UPDATE = "AUTH_PROFILE_UPDATE";
    public static final String BUILD_HYDRATION_GOAL = "BUILD_HYDRATION_GOAL";

    public static final String GET_URL_LOGIN_GARMIN = "GET_URL_LOGIN_GARMIN";
    public static final String GET_URL_LOGIN_STRAVA = "GET_URL_LOGIN_STRAVA";
    public static final String GET_URL_LOGIN_TERRA = "GET_URL_LOGIN_TERRA";
    public static final String GET_URL_LOGIN_WHOOP = "GET_URL_LOGIN_WHOOP";
    public static final String NETWORK_USER_CLICK_URL = "NETWORK_USER_CLICK_URL";
    public static final String AUTH_USER_UNAUTHENTICATED_CHANGED = "AUTH_USER_UNAUTHENTICATED_CHANGED";

    public static final String DISCONNECT_WEATHER_CITY = "DISCONNECT_WEATHER_CITY";
    public static final String DISCONNECT_EXTERNAL_SERVICE = "DISCONNECT_EXTERNAL_SERVICE";
    public static final String SELECT_WEATHER_CITY = "SELECT_WEATHER_CITY";
    public static final String STATUS_EXTERNAL_SERVICE = "STATUS_EXTERNAL_SERVICE";
    public static final String GET_HYDRATION_DAILY_GOAL = "GET_HYDRATION_DAILY_GOAL";
    public static final String GET_HYDRATION_DAILY_GOAL_DETAILS = "GET_HYDRATION_DAILY_GOAL_DETAILS";
    public static final String GET_TOTAL_GOAL_DAYS = "GET_TOTAL_GOAL_DAYS";

    public static final String SIMULATE_REMINDER_PATTERN = "SIMULATE_REMINDER_PATTERN";
    public static final String GET_FEED = "GET_FEED";
    public static final String RUN_SINGLE_MEASUREMENT = "RUN_SINGLE_MEASUREMENT";
    public static final String GET_LATEST_APP_VERSION = "GET_LATEST_APP_VERSION";
    public static final String GET_CALIBRATION_FROM_FT2_RESULT = "GET_CALIBRATION_FROM_FT2_RESULT";

    public static final String GET_TUTORIAL_LINKS = "GET_TUTORIAL_LINKS";
    public static final String VERIFY_CITY = "VERIFY_CITY";
    public static final String GET_AUTO_COMPLETE_PLACE = "GET_AUTO_COMPLETE_PLACE";
    public static final String NETWORK_CHANNEL = "NETWORK_CHANNEL";
    public static final String REPORT_USER_LOG = "REPORT_USER_LOG";
    public static final String REPORT_USER_CRASH = "REPORT_USER_CRASH";
    public static final String REPORT_ERROR = "REPORT_ERROR";
    public static final String ADD_SPORT_ACTIVITY = "ADD_SPORT_ACTIVITY";
    public static final String UPDATE_SPORT_ACTIVITY = "UPDATE_SPORT_ACTIVITY";
    public static final String DELETE_SPORT_ACTIVITY = "DELETE_SPORT_ACTIVITY";
    public static final String DELETE_ALL_TODAY_SPORT_ACTIVITY = "DELETE_ALL_TODAY_SPORT_ACTIVITY";
    public static final String REPORT_SCREEN_CHANGE = "REPORT_SCREEN_CHANGE";
    public static final String GET_LOCATION = "GET_LOCATION";
    public static final String GET_APP_TYPE = "GET_APP_TYPE";
    public static final String GET_USER_ID = "GET_USER_ID";
    public static final String GET_USER_SCORE = "GET_USER_SCORE";
    public static final String REPORT_USER_HYDRATION = "REPORT_USER_HYDRATION";
    public static final String RETRIEVE_APP_PRIVACY_POLICY = "RETRIEVE_APP_PRIVACY_POLICY";
    public static final String GET_LAST_OTA_VERSIONS = "GET_LAST_OTA_VERSIONS";
    public static final String GET_LAST_OTA_VERSIONS_IN_BOOT = "GET_LAST_OTA_VERSIONS_IN_BOOT";
    public static final String RETRIEVE_APP_TREMS_AND_CONDITIONS = "RETRIEVE_APP_TREMS_AND_CONDITIONS";
    public static final String RETRIEVE_APP_CONFIGURATIONS = "RETRIEVE_APP_CONFIGURATIONS";
    public static final String SEND_SDK_LOGS = "SEND_SDK_LOGS";
    public static final String RESEND_FAILED_LOGS = "RESEND_FAILED_LOGS";
    public static final String RETRIEVE_APP_TREMS_AND_CONDITIONS_VERSION = "RETRIEVE_APP_TREMS_AND_CONDITIONS_VERSION";
    public static final String GET_DRS_HOST_CODE = "GET_DRS_HOST_CODE";
    public static final String DOES_DRS_USER_SKILL_ENABLED = "DOES_DRS_USER_SKILL_ENABLED";
    public static final String SET_DRS_PROCESS_DATA = "SET_DRS_PROCESS_DATA";
    public static final String FETCH_REMOTE_CALIBRATION_TABLE = "FETCH_REMOTE_CALIBRATION_TABLE";
    public static final String FETCH_BOTTLE_LIST = "FETCH_BOTTLE_LIST";
    public static final String REPORT_ATTRIBUTES = "REPORT_ATTRIBUTES";

    public static final String GET_STATUS_ADAPTERS = "GET_STATUS_ADAPTERS";
    public static final String PERMISSIONS_CHANNEL = "PERMISSIONS_CHANNEL";
    public static final String NOTIFICATIONS_OR_BATTERY_OPT_METHOD = "NOTIFICATIONS_OR_BATTERY_OPT_METHOD";
    public static final String PERMISSIONS_CHUNK_METHOD = "PERMISSIONS_CHUNK_METHOD";
    public static final String PERMISSIONS_ADAPTERS_METHOD = "PERMISSIONS_ADAPTERS_METHOD";
    public static final String DENIED_PERMISSIONS_CHUNK_METHOD = "DENIED_PERMISSIONS_CHUNK_METHOD";
    public static final String MISSING_PERMISSIONS_CHECK_METHOD = "MISSING_PERMISSIONS_CHECK_METHOD";
    public static final String CHECK_IF_ALL_PERMISSION_ALLOWED_METHOD = "CHECK_IF_ALL_PERMISSION_ALLOWED_METHOD";
    public static final String USER_WAS_ASKED_ABOUT_LOCATION_PERMISSION_ALREADY = "USER_WAS_ASKED_ABOUT_LOCATION_PERMISSION_ALREADY";
    public static final String IS_BLUETOOTH_PERMISSION_GRANTED = "IS_BLUETOOTH_PERMISSION_GRANTED";
    public static final String CHECK_IS_ANDROID_NOTIFICATION_PERMISSION_GRANTED = "CHECK_IS_ANDROID_NOTIFICATION_PERMISSION_GRANTED";
    public static final String REQUEST_ANDROID_NOTIFICATION_PERMISSION = "REQUEST_ANDROID_NOTIFICATION_PERMISSION";
    public static final String IS_NOTIFICATION_CAN_SHOW = "IS_NOTIFICATION_CAN_SHOW";
    public static final String OPEN_PERMISSION_APP_SETTINGS = "OPEN_PERMISSION_APP_SETTINGS";
    public static final String CHECK_LOCATION_PERMISSION_STATUS = "CHECK_LOCATION_PERMISSION_STATUS";
    public static final String CHECK_BLUETOOTH_PERMISSION_STATUS = "CHECK_BLUETOOTH_PERMISSION_STATUS";

    public static final String LOCAL_PUSH_NOTIFICATIONS_CHANNEL = "LOCAL_PUSH_NOTIFICATIONS_CHANNEL";
    public static final String SET_LOCAL_PUSHS_REMINDERS = "SET_LOCAL_PUSHS_REMINDERS";
    public static final String SET_LOCAL_PUSHS_RAND_MSGS = "SET_LOCAL_PUSHS_RAND_MSGS";
    public static final String CANCEL_ONCE_LOCAL_PUSH_REMINDERS = "CANCEL_ONCE_LOCAL_PUSH_REMINDERS";

    public static final String BACKGROUND_CHANNEL = "BACKGROUND_CHANNEL";
    public static final String BACKGROUND_SYNC_ON_START_APP = "BACKGROUND_SYNC_ON_START_APP";
    public static final String CLEAR_ALL_BACKGROUND_DB = "CLEAR_ALL_BACKGROUND_DB";
    public static final String CLEAR_APP_DATA = "CLEAR_APP_DATA";

    public static final String PLUGIN_MESSENGER_CHANNEL = "PLUGIN_MESSENGER_CHANNEL";
    public static final String SET_STRING_IN_PLUGIN_STORAGE = "SET_STRING_IN_PLUGIN_STORAGE";


    public static final String GET_ENCOURAGEMENT_SENTENCE = "GET_ENCOURAGEMENT_SENTENCE";
    public static final String GET_CALENDAR_DAILY_GOAL = "GET_CALENDAR_DAILY_GOAL";
    public static final String GET_DAILY_HYDRATION = "GET_DAILY_HYDRATION";
    public static final String SET_NOTIFICATION_REMINDER = "SET_NOTIFICATION_REMINDER";
    public static final String SET_TIME_RANGE_HYDRATION_DAY = "SET_TIME_RANGE_HYDRATION_DAY";
    public static final String SET_MANUAL_DAILY_GOAL = "SET_MANUAL_DAILY_GOAL";


    public static final String DEVICE_SET_DISCONNECT = "DEVICE_SET_DISCONNECT";
    public static final String DEVICE_SET_REMINDER_COLOR = "DEVICE_SET_REMINDER_COLOR";
    public static final String DEVICE_SET_DAILY_GOAL = "DEVICE_SET_DAILY_GOAL";
    public static final String DEVICE_SET_SILENT_MODE = "DEVICE_SET_SILENT_MODE";
    public static final String DEVICE_SET_VIBRATION = "DEVICE_SET_VIBRATION";
    public static final String DEVICE_SET_MULTI_CONFIGURATIONS = "DEVICE_SET_MULTI_CONFIGURATIONS";
    public static final String DEVICE_GET_MULTI_CONFIGURATIONS = "DEVICE_GET_MULTI_CONFIGURATIONS";
    public static final String DEVICE_START_CALIBRATION = "DEVICE_SET_CALIBRATION";


    public static final String GET_NOTIFICATIONS_LIST = "GET_NOTIFICATIONS_LIST";
    public static final String GET_NOTIFICATION = "GET_NOTIFICATION";
    public static final String AUTH_LOG_OUT = "AUTH_LOG_OUT";
    public static final String AUTH_SOCIAL_LOGIN = "AUTH_SOCIAL_LOGIN";
    public static final String AUTH_RESET_PASSWORD = "AUTH_RESET_PASSWORD";
    public static final String SET_STORE_USER_DATA = "SET_STORE_USER_DATA";
    public static final String RESTORE_USER_DATA = "RESTORE_USER_DATA";
    public static final String RESTORE_USER_DATA_MIGRATION = "RESTORE_USER_DATA_MIGRATION";
    public static final String GET_SPORT_ACTIVITIES = "GET_SPORT_ACTIVITIES";
    public static final String DELETE_ACCOUNT = "DELETE_ACCOUNT";
    public static final String GET_BADGES = "GET_BADGES";
    public static final String SEND_SUPPORT_MGS = "SEND_SUPPORT_MGS";

    public static final String DEVICE_SET_MANUALLY_HYDRATION = "DEVICE_SET_MANUALLY_HYDRATION";
    public static final String DEVICE_SET_EXTRA_DAILY_GOAL = "DEVICE_SET_EXTRA_DAILY_GOAL";
    public static final String DEVICE_SET_HYDRATION_LEVEL = "DEVICE_SET_HYDRATION_LEVEL";
    public static final String DEVICE_GET_HYDRATION = "DEVICE_GET_HYDRATION";
    public static final String DEVICE_GET_EXTRA_DAILY_GOAL = "DEVICE_GET_EXTRA_DAILY_GOAL";
    public static final String DEVICE_GET_BASE_GOAL = "DEVICE_GET_BASE_GOAL";
    public static final String UPDATE_REMINDER_LEDS_PATTERN = "UPDATE_REMINDER_LEDS_PATTERN";
    public static final String SIMULATION_VIBRATION = "SIMULATION_VIBRATION";

    public static final String GET_CURRENT_ENVIRONMENT = "GET_CURRENT_ENVIRONMENT";
    public static final String START_OTA_BOOT_METHOD = "START_OTA_BOOT_METHOD";
    public static final String SHARE_IN_FACEBOOK = "SHARE_IN_FACEBOOK";
    public static final String SHARE_IN_WHATSAPP = "SHARE_IN_WHATSAPP";
    public static final String APP_TO_APP_AUTH = "APP_TO_APP_AUTH";

    public static final String GET_HOLIDAYS = "GET_HOLIDAYS";
    public static final String GET_LEADERBOARD = "GET_LEADERBOARD";
    public static final String REPORT_FEED_ANALYTIC = "REPORT_FEED_ANALYTIC";
    public static final String ADD_EXTRA_MULTI_HYDRATION = "ADD_EXTRA_MULTI_HYDRATION";
    public static final String DELETE_EXTRA_HYDRATION = "DELETE_EXTRA_HYDRATION";

    public static final String GET_USER_GROUPS = "GET_USER_GROUPS";
    public static final String JOIN_GROUP = "JOIN_GROUP";
    public static final String REMOVE_USER_FROM_GROUP = "REMOVE_USER_FROM_GROUP";
    public static final String GET_GROUP_INFO = "GET_GROUP_INFO";
    public static final String CREATE_NEW_GROUP = "CREATE_NEW_GROUP";
    public static final String EDIT_GROUP = "EDIT_GROUP";
    public static final String REPORT_USER_LOCATION = "REPORT_USER_LOCATION";
    public static final String GET_LAST_USER_LOCATION = "GET_LAST_USER_LOCATION";
    public static final String GET_SHORTCUTS = "GET_SHORTCUTS";
    public static final String GET_LOGIC_HYDRATION = "GET_LOGIC_HYDRATION";
    public static final String GET_REFER_FRIEND_COUPON = "GET_REFER_FRIEND_COUPON";
    public static final String CREDIT_COUPON = "CREDIT_COUPON";
    public static final String GET_TOTAL_USAGES = "GET_TOTAL_USAGES";
    public static final String GET_SOCIAL_TOKEN = "GET_SOCIAL_TOKEN";
    public static final String GET_A2A_DETAILS = "GET_A2A_DETAILS";
    public static final String GET_APP_VERSIONS = "GET_APP_VERSIONS";
    public static final String SUBSCRIBE_TO_REMINDER_TOPIC = "SUBSCRIBE_TO_REMINDER_TOPIC";
    public static final String SUBSCRIPTION_TOPIC = "SUBSCRIPTION_TOPIC";
    public static final String ADD_POST = "ADD_POST";
    public static final String ADD_COMMENT = "ADD_COMMENT";
    public static final String GET_WEEKLY_REPORT = "GET_WEEKLY_REPORT";
    public static final String REGISTRATION_WARRANTY = "REGISTRATION_WARRANTY";
    public static final String FILLING_ANSWER = "FILLING_ANSWER";
    public static final String DEVICE_SET_RESET_BOOT_STATE = "DEVICE_SET_RESET_BOOT_STATE";
    public static final String GET_URL_LOGIN_OURA = "GET_URL_LOGIN_OURA";
}

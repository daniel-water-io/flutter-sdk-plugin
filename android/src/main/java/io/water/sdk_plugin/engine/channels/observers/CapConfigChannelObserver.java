package io.water.sdk_plugin.engine.channels.observers;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_CONFIGURATIONS_CHANGED_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_CONFIGURATIONS_CHANNEL;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.water.water_io_sdk.ble.entities.CommandResponse;

import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;
import io.water.sdk_plugin.models.CapConfigModel;

public class CapConfigChannelObserver extends ChannelObserver<CommandResponse> {

    public CapConfigChannelObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData observable) {
        super(binaryMessenger, CAP_CONFIGURATIONS_CHANNEL, CAP_CONFIGURATIONS_CHANGED_METHOD, owner, observable);
    }

    @Override
    public void onChanged(CommandResponse commandResponse) {
        if (commandResponse.getCommand().equals("GET_MULTI_PARAM_CONFIG_COMMAND")) {

            new Thread(() -> {
                JsonObject data = new Gson().fromJson(commandResponse.getData(), JsonObject.class);
                HashMap<String, CapConfigModel> mapCapConfigModels = parseCapConfigs(data.get("result").getAsJsonArray());
                JsonObject json = new JsonObject();
                for (Map.Entry<String, CapConfigModel> entry : mapCapConfigModels.entrySet()) {
                    String key = entry.getKey();
                    if (entry.getValue().getaClass() == String.class) {
                        json.addProperty(key, entry.getValue().getData().toString());
                    } else if (entry.getValue().getaClass() == Integer.class) {
                        json.addProperty(key, Integer.valueOf(entry.getValue().getData().toString()));
                    } else if (entry.getValue().getaClass() == JsonObject.class) {
                        json.addProperty(key, entry.getValue().getData().toString());
                    }
                }
                new Handler(Looper.getMainLooper()).post(() -> {
                    invokeTarget(json.toString());
                });
            }).start();
        }
    }

    private String getStringWithoutEdges(String data) {
        byte[] bytes = data.getBytes();
        byte[] filteredByteArray = Arrays.copyOfRange(bytes, 1, bytes.length - 1);
        return new String(filteredByteArray, StandardCharsets.UTF_8);
    }

    public HashMap<String, CapConfigModel> parseCapConfigs(JsonArray array) {
        HashMap<String, CapConfigModel> mapCapConfigModels = new HashMap<>();

        for (int i = 0; i < array.size(); i++) {
            try {
                JsonObject val = array.get(i).getAsJsonObject();
                String key = val.get("name").getAsString();
                String data = val.get("data").getAsString();

                if (data == null || key == null ||
                        data.toLowerCase().equals("null") ||
                        key.toLowerCase().equals("null")) {
                    continue;
                }

                Map.Entry<Object, Class<?>> mapEntry = getData(key, data);
                CapConfigModel capConfigModel = new CapConfigModel(key, mapEntry.getKey(), mapEntry.getValue());
                mapCapConfigModels.put(key, capConfigModel);
            } catch (Exception exception) {
                exception.getStackTrace();
            }
        }

        return mapCapConfigModels;
    }

    public Map.Entry<Object, Class<?>> getData(String key, String val) {
        switch (key) {
            case "CRC":
            case "TIME_ZONE":
            case "DAILY_GOAL":
            case "START_TIME":
            case "END_TIME":
            case "REMINDER_LEDS_ENABLE":
            case "REMINDER_LEDS_PATTERN":
            case "REMINDER_ROUNDS":
            case "REMINDER_CYCLES":
            case "REMINDER_INTERVAL":
            case "ENABLE_LEDS_OPEN_CLOSE":
            case "STATUS_LEDS_ENABLE":
            case "LED_OFF_OUTSIDE_ACTIVE_TIME":
            case "ENABLE_SHABAT_MODE":
            case "DEMO_MODE_ENABLE":
            case "DEMO_MODE_LED_INTERVAL":
            case "DAR":
            case "DBR":
            case "MAR":
            case "MBR":
            case "IGR":
            case "CALIBRATION_OFFSET":
            case "MAX_Z":
            case "LTV_ADVERTISE_PERIOD":
            case "ADVERTISE_INTERVAL":
            case "SESSION_ID":
            case "REMINDER_LOGIC":
            case "LAST_USER_ID":
                return new AbstractMap.SimpleEntry<>(Integer.valueOf(val), Integer.class);
            case "BATCH_NUMBER":
            case "HW_VERSION":
            case "LAST_FW_VERSION":
            case "REMINDER_LEDS_COLOR":
                return new AbstractMap.SimpleEntry<>(val, String.class);
            case "REMINDER_UI_ELEMENTS":
                return new AbstractMap.SimpleEntry<>(new Gson().fromJson(val, JsonObject.class), JsonObject.class);
            default:
                return new AbstractMap.SimpleEntry<>("unknown", String.class);
        }

    }

}



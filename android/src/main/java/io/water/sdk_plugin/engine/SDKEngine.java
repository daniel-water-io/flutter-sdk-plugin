package io.water.sdk_plugin.engine;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;

import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractDefaultConnection;
import com.water.water_io_sdk.ble.entities.WIOSmartDevice;
import com.water.water_io_sdk.ble.enums.AppMode;
import com.water.water_io_sdk.ble.enums.DeviceStatus;
import com.water.water_io_sdk.ble.handler_logs.categoties.HandlerEvent;
import com.water.water_io_sdk.ble.sdkEngine.WIOAppManually;
import com.water.water_io_sdk.ble.sdkEngine.configurations.WIOConfig;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.PluginConfiguration;
import io.water.sdk_plugin.activities.PluginPermissionsActivity;
import io.water.sdk_plugin.engine.channels.manager.FlutterChannelsManager;
import io.water.sdk_plugin.engine.implementations.procedures.HydrationDefaultCapCmds;
import io.water.sdk_plugin.engine.implementations.procedures.SDKFirstConfigDeviceCmds;
import io.water.sdk_plugin.engine.implementations.procedures.VitaminDefaultCapCmds;
import io.water.sdk_plugin.engine.implementations.sdk_impls.SDKHydrationLogsImpl;
import io.water.sdk_plugin.engine.implementations.sdk_impls.SDKPushImpl;
import io.water.sdk_plugin.engine.implementations.sdk_impls.SDKReminderImpl;
import io.water.sdk_plugin.engine.implementations.sdk_impls.SDKVitaminsLogsImpl;


public class SDKEngine {
    private WIOAppManually mWIOApp;
    private final FlutterChannelsManager mChannelsManager;

    public WIOSmartDevice getWIOSmartDevice() {
        return mWIOApp.getWIOSmartDevice();
    }

    public SDKEngine(Activity activity, LifecycleOwner lifecycleOwner, BinaryMessenger binaryMessenger, PluginConfiguration pluginConfiguration) {
        // USING THE MANUAL VERSION..!
        WIOConfig wioConfig = new WIOConfig()
                .setIsGetCurrentLocationAllowed(updateLocationReportByApp(pluginConfiguration.appGenes))
                .setFilterLastEventObserve(getLogsStatusListener(pluginConfiguration.appGenes))
                .setWIODeviceEventsImp(getEventsImplByAppGenes(pluginConfiguration.appGenes))
                .setAppMode(
                        pluginConfiguration.appMode)
                .setWIOServerPushImp(
                        pluginConfiguration.includePushNotifications ?
                                SDKPushImpl.class :
                                null
                )
                .setWIOReminderImp(
                        pluginConfiguration.includeLocalReminders ?
                                SDKReminderImpl.class :
                                null
                )
                .setDefaultProcedureDevice(getDefaultProcedureByGenes(pluginConfiguration.appGenes))
                .setFirstProcedureDevice(SDKFirstConfigDeviceCmds.class)
                .setParallelConnectionSupport(false)
                .build();
        mWIOApp = new WIOAppManually(activity, wioConfig);

        mWIOApp.eventOnCreate();
        mChannelsManager = new FlutterChannelsManager(mWIOApp, binaryMessenger, lifecycleOwner, pluginConfiguration, activity);

//        handleFixDeathBluetoothAdapter(activity, lifecycleOwner);

        if (activity instanceof PluginPermissionsActivity) {
            ((PluginPermissionsActivity) activity).setPermissionsCallbackChannel(mChannelsManager.getPermissionsCallbackChannel());
        } else {
            mWIOApp.handleOptionalToConnectDevice(activity);
            askForPermissions(activity);
        }
    }

//    private void handleFixDeathBluetoothAdapter(Activity activity, LifecycleOwner lifecycleOwner) {
//        mWIOApp.getWIOSmartDevice().getLiveDataOnScanFail().observe(lifecycleOwner, errorCode -> {
//            if (activity != null && !activity.isFinishing() && !activity.isDestroyed()) {
//                if (mLogicDeathBluetoothAdapter.isReasonableProfits()) {
//                    mLogicDeathBluetoothAdapter.setLastTimestampHasError(System.currentTimeMillis());
//                    new FixDeathBluetoothAdapter(activity).initBluetoothAdapter(false);
//                } else if (mLogicDeathBluetoothAdapter.isReasonableProfitsHalfTime() && mLogicDeathBluetoothAdapter.isAlreadyReported()) {
//                    mLogicDeathBluetoothAdapter.setAlreadyReported(true);
//                    SDKLog sdkLog = new SDKLog(LogsType.ERROR_BLE, "Bluetooth adapter maybe death", errorCode + "", errorCode + "", "");
//                    new SDKLogsRepository().insert(sdkLog);
//                    new ErrorSDK(sdkLog.toDashboardJson(), System.currentTimeMillis(), LogsType.ERROR_BLE.name().toLowerCase()).sendRequest();
//
//                    //TODO add this to the next version
////                    if (mWIOApp.getWIOSmartDevice().isIsScanningAllow()) {
////                        mWIOApp.getWIOSmartDevice().stopScan();
////                        new Handler(Looper.getMainLooper()).postDelayed(() -> {
////                            mWIOApp.getWIOSmartDevice().startScan();
////                            SDKLog sdkLog1 = new SDKLog(LogsType.SDK_FLOW, "Trying fix bluetooth adapter maybe death", "", "", "");
////                            new SDKLogsRepository().insert(sdkLog1);
////                            new ErrorSDK(sdkLog1.toDashboardJson(), System.currentTimeMillis(), LogsType.SDK_FLOW.name().toLowerCase()).sendRequest();
////                        }, 5000);
////                    }
//
////                  Toast.makeText(activity, activity.getString(R.string.reboot_your_device), Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//    }

    private boolean updateLocationReportByApp(PluginConfiguration.AppGenes appGenes) {
//        switch (appGenes) {
//            case Hydration:
//                return false;
//            case Vitamins:
//                return true;
//            default:
//                return true;
//        }
        return true;
    }

    private List<DeviceStatus> getLogsStatusListener(PluginConfiguration.AppGenes appGenes) {
        switch (appGenes) {
            default:
            case Hydration:
                return new ArrayList<DeviceStatus>() {{
                    add(DeviceStatus.CLOSED_CAP);
                    add(DeviceStatus.OPENED_CAP);
                    add(DeviceStatus.MEASUREMENT_ACCURATE);
//                    add(DeviceStatus.MEASURE_NO_VALID);
                }};
            case Vitamins:
                return new ArrayList<DeviceStatus>() {{
                    add(DeviceStatus.CLOSED_CAP);
                    add(DeviceStatus.OPENED_CAP);
                }};
        }
    }

    private Class<? extends ProcedureAbstractDefaultConnection> getDefaultProcedureByGenes(PluginConfiguration.AppGenes appGenes) {
        switch (appGenes) {
            default:
            case Hydration:
                return HydrationDefaultCapCmds.class;
            case Vitamins:
                return VitaminDefaultCapCmds.class;
        }
    }

    private Class<? extends HandlerEvent> getEventsImplByAppGenes(PluginConfiguration.AppGenes appGenes) {
        switch (appGenes) {
            case Hydration:
                return SDKHydrationLogsImpl.class;
            case Vitamins:
                return SDKVitaminsLogsImpl.class;
            default:
                return null;
        }
    }

    private void askForPermissions(Activity activity) {
        boolean isNeedRunningBackground = SharedPrefsModuleHelper.getInstance().getAppMode() == AppMode.FOREGROUND_AND_BACKGROUND;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED ||
                    activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {

                if (isNeedRunningBackground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    activity.requestPermissions(
                            new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                            },
                            0);
                } else {
                    activity.requestPermissions(
                            new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                            },
                            0);
                }
            }
        }

        if (isNeedRunningBackground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity.startActivityForResult(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:" + activity.getPackageName())), 0);
        }
    }

    public void eventOnResume() {
        if (mWIOApp != null) {
            mWIOApp.eventOnResume();
        }
    }

    public void onSdkEngineDestroy() {
        // TODO!
        if (mWIOApp == null)
            return;

        mChannelsManager.destroyAll();
        mWIOApp.eventOnDestroy();
        mWIOApp = null;
    }
}

package io.water.sdk_plugin.engine.channels.callbacks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.water.water_io_sdk.application.ModuleApp;
import com.water.water_io_sdk.ble.entities.LocationEvent;
import com.water.water_io_sdk.ble.enums.TriggerReport;
import com.water.water_io_sdk.ble.utilities.LocationUser;
import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.entities.requests.firebase.sdk_messaging.ErrorSDK;
import com.water.water_io_sdk.network.entities.requests.warer_io.authorized.GetFactoryTestResults;
import com.water.water_io_sdk.network.entities.requests.warer_io.authorized.GetNotificationData;
import com.water.water_io_sdk.network.entities.requests.warer_io.authorized.UploadLogFile;
import com.water.water_io_sdk.network.enums.FirebaseTitle;
import com.water.water_io_sdk.network.interfaces.OnResult;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.network.utilities.AnalyticsEventHelper;
import com.water.water_io_sdk.network.utilities.FileUpload;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.LogsType;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.SDKLogsRepository;
import com.water.water_io_sdk.storage.database.logs.sdk_messaging.entities.SDKLog;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;
import io.water.sdk_plugin.enums.HealthService;
import io.water.sdk_plugin.interfaces.ICallbackGetNewestVersion;
import io.water.sdk_plugin.utils.LeaderboardModelManager;
import io.water.sdk_plugin.utils.NetworkAPIsOTA;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.ADD_COMMENT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.ADD_EXTRA_MULTI_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.ADD_POST;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.APP_TO_APP_AUTH;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.BUILD_HYDRATION_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CREATE_NEW_GROUP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CREDIT_COUPON;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DELETE_ACCOUNT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DELETE_EXTRA_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DISCONNECT_EXTERNAL_SERVICE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DISCONNECT_WEATHER_CITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DOES_DRS_USER_SKILL_ENABLED;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.EDIT_GROUP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.FETCH_BOTTLE_LIST;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.FETCH_REMOTE_CALIBRATION_TABLE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.FILLING_ANSWER;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_A2A_DETAILS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_APP_VERSIONS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_GROUP_INFO;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_HOLIDAYS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LAST_DB_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LAST_USER_LOCATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LATEST_APP_VERSION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_CALIBRATION_FROM_FT2_RESULT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_APP_TYPE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_SOCIAL_TOKEN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_AUTO_COMPLETE_PLACE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_BADGES;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_CALENDAR_DAILY_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_DAILY_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_DRS_HOST_CODE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_ENCOURAGEMENT_SENTENCE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_HYDRATION_DAILY_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_HYDRATION_DAILY_GOAL_DETAILS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LAST_OTA_VERSIONS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LAST_OTA_VERSIONS_IN_BOOT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LEADERBOARD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LOCATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_NOTIFICATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_NOTIFICATIONS_LIST;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_REFER_FRIEND_COUPON;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_TOTAL_USAGES;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_URL_LOGIN_GARMIN;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_URL_LOGIN_STRAVA;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_URL_LOGIN_TERRA;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_URL_LOGIN_WHOOP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_USER_GROUPS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_USER_ID;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_USER_SCORE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_WEEKLY_REPORT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.JOIN_GROUP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.NETWORK_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.ADD_SPORT_ACTIVITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REGISTRATION_WARRANTY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REMOVE_USER_FROM_GROUP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_ATTRIBUTES;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DELETE_SPORT_ACTIVITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.DELETE_ALL_TODAY_SPORT_ACTIVITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_SPORT_ACTIVITIES;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_ERROR;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_FEED_ANALYTIC;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_USER_CRASH;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_CURRENT_ENVIRONMENT;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_USER_LOCATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RESEND_FAILED_LOGS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RESTORE_USER_DATA_MIGRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SEND_SUPPORT_MGS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SHARE_IN_FACEBOOK;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SHARE_IN_WHATSAPP;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SUBSCRIBE_TO_REMINDER_TOPIC;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SUBSCRIPTION_TOPIC;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.UPDATE_SPORT_ACTIVITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_SCREEN_CHANGE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_USER_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.REPORT_USER_LOG;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RESTORE_USER_DATA;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RETRIEVE_APP_CONFIGURATIONS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RETRIEVE_APP_PRIVACY_POLICY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RETRIEVE_APP_TREMS_AND_CONDITIONS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.RETRIEVE_APP_TREMS_AND_CONDITIONS_VERSION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SELECT_WEATHER_CITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SEND_SDK_LOGS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_DRS_PROCESS_DATA;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_MANUAL_DAILY_GOAL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_NOTIFICATION_REMINDER;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_STORE_USER_DATA;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_TIME_RANGE_HYDRATION_DAY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.STATUS_EXTERNAL_SERVICE;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.VERIFY_CITY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_FEED;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_TUTORIAL_LINKS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_TOTAL_GOAL_DAYS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_SHORTCUTS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_LOGIC_HYDRATION;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.GET_URL_LOGIN_OURA;

import androidx.core.content.FileProvider;

import org.json.JSONException;
import org.json.JSONObject;

public class NetworkCallbackChannel extends CallbackChannel {

    private final int DEFAULT_OFFSET = 92;
    private CallbackGetActivity listener;

    public NetworkCallbackChannel(BinaryMessenger binaryMessenger, CallbackGetActivity listener) {
        super(binaryMessenger, NETWORK_CHANNEL);
        this.listener = listener;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        Log.d("Network_Test", methodCall.method);
        printServerMode();

        switch (methodCall.method) {
            case REPORT_USER_LOG:
                handleReportUserLogMethodCall(methodCall, result);
                break;
            case SELECT_WEATHER_CITY:
                getCityWeatherByGps((Map<String, Object>) methodCall.arguments, result);
                break;
            case VERIFY_CITY:
                getCityWeatherManually((String) methodCall.arguments, result);
                break;
            case GET_AUTO_COMPLETE_PLACE:
                getCityAutoComplete((Map<String, Object>) methodCall.arguments, result);
                break;
            case ADD_SPORT_ACTIVITY:
                handleAddReportSportActivityMethodCall(methodCall, result);
                break;
            case UPDATE_SPORT_ACTIVITY:
                handleReportUpdateSportActivityMethodCall((Map<String, Object>) methodCall.arguments, result);
                break;
            case DELETE_SPORT_ACTIVITY:
                // handleReportDeleteAllSportActivitiesMethodCall(result);
                handleReportDeleteSportActivityMethodCall((String) methodCall.arguments, result);
                break;
            case DELETE_ALL_TODAY_SPORT_ACTIVITY:
                handleReportDeleteAllSportActivitiesMethodCall(result);
                break;
            case REPORT_SCREEN_CHANGE:
                handleReportScreenMethodCall(methodCall, result);
                break;
            case GET_APP_TYPE:
                handleAppTypeRequestMethodCall(methodCall, result);
                break;
            case GET_LOCATION:
                handleGetUserLocation(methodCall, result);
                break;
            case GET_USER_ID:
                handleUserIdRequestMethodCall(methodCall, result);
                break;
            case GET_USER_SCORE:
                handleUserScoreRequestMethodCall(methodCall, result);
                break;
            case REPORT_USER_HYDRATION:
                handleUserHydrationReportMethodCall(methodCall, result);
                break;
            case RETRIEVE_APP_PRIVACY_POLICY:
                handleAppPrivacyPolicyLinkReq((Boolean) methodCall.arguments, result);
                break;
            case RETRIEVE_APP_TREMS_AND_CONDITIONS:
                handleAppTermsAndCondsLinkReq((Boolean) methodCall.arguments, result);
                break;
            case RETRIEVE_APP_CONFIGURATIONS:
                handleAppConfiguration(methodCall, result);
                break;
            case SEND_SDK_LOGS:
                sendSdkLogs(methodCall, result);
                break;
            case RETRIEVE_APP_TREMS_AND_CONDITIONS_VERSION:
                handleAppTermsAndCondsVersionReq(result);
                break;
            case GET_DRS_HOST_CODE:
                handleGetDrsHostCode((Map<String, Object>) methodCall.arguments, result);
                break;
            case DOES_DRS_USER_SKILL_ENABLED:
                handleDoesDrsUserSkillEnabled(result);
                break;
            case SET_DRS_PROCESS_DATA:
                handleSetDrsProcessData((Map<String, Object>) methodCall.arguments, result);
                break;
            case FETCH_REMOTE_CALIBRATION_TABLE:
                handleFetchRemoteCalibrationTable((String) methodCall.arguments, result);
                break;
            case FETCH_BOTTLE_LIST:
                handleFetchBottleList(result);
                break;
            case REPORT_ATTRIBUTES:
                handleReportAttributes(methodCall, result);
                break;
            case BUILD_HYDRATION_GOAL:
                handleDailyGoal(result);
                break;
            case GET_ENCOURAGEMENT_SENTENCE:
                getEncouragementSentence(result);
                break;
            case GET_CALENDAR_DAILY_GOAL:
                getCalendarDailyGoal((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_DAILY_HYDRATION:
                getDailyHydration((Map<String, Object>) methodCall.arguments, result);
                break;
            case SET_NOTIFICATION_REMINDER:
                notificationReminder((Map<String, Object>) methodCall.arguments, result);
                break;
            case SET_TIME_RANGE_HYDRATION_DAY:
                timeRangeHydrationDay((Map<String, Object>) methodCall.arguments, result);
                break;
            case SET_MANUAL_DAILY_GOAL:
                manualDailyGoal((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_LAST_OTA_VERSIONS:
                String capVersion = (String) methodCall.arguments;
                getCapVersionsSupported(capVersion, result);
                break;
            case GET_LAST_OTA_VERSIONS_IN_BOOT:
                result.success("test");
                break;
            case GET_URL_LOGIN_STRAVA:
                loadStravaURL(result);
                break;
            case GET_URL_LOGIN_GARMIN:
                loadGarminURL(result);
                break;
            case GET_URL_LOGIN_WHOOP:
                loadWhoopURL(result);
                break;
            case GET_URL_LOGIN_TERRA:
                loadTerraURL((String) methodCall.arguments, result);
                break;
            case DISCONNECT_EXTERNAL_SERVICE:
                disconnectFromHealthService((String) methodCall.arguments, result);
                break;
            case DISCONNECT_WEATHER_CITY:
                disconnectWeatherCity(result);
                break;
            case STATUS_EXTERNAL_SERVICE:
                getExternalService(result);
                break;
            case GET_HYDRATION_DAILY_GOAL:
                getHydrationDailyGoal(result);
                break;
            case GET_HYDRATION_DAILY_GOAL_DETAILS:
                getHydrationDailyGoalDetails((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_NOTIFICATIONS_LIST:
                getNotificationList((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_NOTIFICATION:
                getNotification((String) methodCall.arguments, result);
                break;
            case SET_STORE_USER_DATA:
                storeUserData((Map<String, Object>) methodCall.arguments, result);
                break;
            case RESTORE_USER_DATA:
                restoreUserData(result);
                break;
            case RESTORE_USER_DATA_MIGRATION:
                restoreEmailData(result);
                break;
            case SEND_SUPPORT_MGS:
                sendSupportMsg(methodCall.arguments.toString(), result);
                break;
            case GET_SPORT_ACTIVITIES:
                restoreSportActivitiesData((Map<String, Object>) methodCall.arguments, result);
                break;
            case DELETE_ACCOUNT:
                deleteAccount(result);
                break;
            case GET_BADGES:
                getBadges(result);
                break;
            case GET_FEED:
                getFeed((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_LATEST_APP_VERSION:
                getLatestAppVersion(result);
                break;
            case GET_CALIBRATION_FROM_FT2_RESULT:
                getFT2Res(methodCall.arguments.toString(), result);
                break;
            case REPORT_USER_CRASH:
                handleReportUserCrashMethodCall(methodCall, result);
                break;
            case GET_CURRENT_ENVIRONMENT:
                getCurrentEnvironmentMethodCall(methodCall, result);
                break;
            case SHARE_IN_FACEBOOK:
                shareInFacebookMethodCall((String) methodCall.arguments, result);
                break;
            case GET_TUTORIAL_LINKS:
                getTutorialLinks(result);
                break;
            case APP_TO_APP_AUTH:
                Map<String, Object> map = (Map<String, Object>) methodCall.arguments;
                appToAppMethod(map, result);
                break;
            case GET_HOLIDAYS:
                getHolidaysMethodCall((Integer) methodCall.arguments, result);
                break;
            case GET_LEADERBOARD:
                Map<String, Object> leaderboardFilters = (Map<String, Object>) methodCall.arguments;
                getLeaderboardMethodCall(leaderboardFilters, result);
                break;
            case REPORT_FEED_ANALYTIC:
                reportFeedAnalyticMethodCall(methodCall, result);
                break;
            case ADD_EXTRA_MULTI_HYDRATION:
                addMultiHydrationMethodCall(methodCall, result);
                break;
            case DELETE_EXTRA_HYDRATION:
                deleteExtraHydrationMethodCall((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_USER_GROUPS:
                getUserGroups(result); // Kfir take the userId from headers
                break;
            case JOIN_GROUP:
                joinGroup((String) methodCall.arguments, result);
                break;
            case REMOVE_USER_FROM_GROUP:
                removeUserFromGroup((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_GROUP_INFO:
                getGroupInfo((String) methodCall.arguments, result);
                break;
            case CREATE_NEW_GROUP:
                createNewGroup((Map<String, Object>) methodCall.arguments, result);
                break;
            case EDIT_GROUP:
                editGroup((Map<String, Object>) methodCall.arguments, result);
                break;
            case REPORT_USER_LOCATION:
                reportUserLocation(result);
                break;
            case GET_LAST_USER_LOCATION:
                getLastCapLocation(result);
                break;
            case GET_TOTAL_GOAL_DAYS:
                getTotalGoalDays(result);
                break;
            case RESEND_FAILED_LOGS:
                resendFailedLogs(result);
                break;
            case REPORT_ERROR:
                handleReportErrorMethodCall(methodCall, result);
                break;
            case GET_SHORTCUTS:
                getShortcutsMethodCall(result);
                break;
            case GET_LOGIC_HYDRATION:
                getLogicHydrationMethodCall(result);
                break;
            case GET_REFER_FRIEND_COUPON:
                getReferFriendCoupon(result);
                break;
            case CREDIT_COUPON:
                getCreditCoupon((String) methodCall.arguments, result);
                break;
            case GET_TOTAL_USAGES:
                getTotalUsage(result);
                break;
            case GET_SOCIAL_TOKEN:
                getStreamToken(result);
                break;
            case GET_A2A_DETAILS:
                getAppToAppDetails((String) methodCall.arguments, result);
                break;
            case GET_APP_VERSIONS:
                getAppVersions(result);
                break;
            case SUBSCRIBE_TO_REMINDER_TOPIC:
                setReminderTopicSubscribe((Map<String, Object>) methodCall.arguments, result);
                break;
            case SUBSCRIPTION_TOPIC:
                subscribeTopics((Map<String, Object>) methodCall.arguments, result);
                break;
            case SHARE_IN_WHATSAPP:
                shareInWhatsAppMethodCall((Map<String, Object>) methodCall.arguments, result);
                break;
            case ADD_POST:
                addPost((Map<String, Object>) methodCall.arguments, result);
                break;
            case ADD_COMMENT:
                addComment((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_WEEKLY_REPORT:
                getWeeklyReport((Map<String, Object>) methodCall.arguments, result);
                break;
            case REGISTRATION_WARRANTY:
                registrationWarranty((Map<String, Object>) methodCall.arguments, result);
                break;
            case FILLING_ANSWER:
                setFillingAnswer((Map<String, Object>) methodCall.arguments, result);
                break;
            case GET_URL_LOGIN_OURA:
                loadOuraURL(result);
                break;
        }
    }

    private void loadOuraURL(MethodChannel.Result result) {
        loadURL("/oura/login-url", result);
    }

    private void setFillingAnswer(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);
        new WIONetwork.Api().questionnaire(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(true);
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void registrationWarranty(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);
        new WIONetwork.Api().warrantyInfo(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getWeeklyReport(Map<String, Object> arguments, MethodChannel.Result result) {
        int from = (Integer) arguments.get("from_date");
        int to = (Integer) arguments.get("to_date");

        new WIONetwork.Api().getWeeklyReport(Long.valueOf(from), Long.valueOf(to), new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void addComment(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);

        new WIONetwork.Api().createComment(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void addPost(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);

        new WIONetwork.Api().createPost(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }


    private void shareInWhatsAppMethodCall
            (Map<String, Object> arguments, MethodChannel.Result result) {
        try {
            String msg = (String) arguments.get("msg");
            String imagePath = (String) arguments.get("imagePath");
            boolean isWhatsAppBusiness = (boolean) arguments.get("isWhatsAppBusiness");

            Activity activity = null;
            if (listener != null) {
                activity = listener.getParentActivity();
            }
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setPackage(isWhatsAppBusiness ? "com.whatsapp.w4b" : "com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg);
            whatsappIntent.setType("image/png");
            System.out.print(imagePath + "url is not empty");
            File file = new File(imagePath);
            Uri fileUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file);
            whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            whatsappIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
            whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(whatsappIntent);
            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");
        }
    }

    private void subscribeTopics(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);

        new WIONetwork.Api().FCMTopic(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(i == 200);
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void setReminderTopicSubscribe
            (Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);
        new WIONetwork.Api().reminderTopic(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(i == 200);
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getAppVersions(MethodChannel.Result result) {
        new WIONetwork.Api().getVersionInfo(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getAppToAppDetails(String clientID, MethodChannel.Result result) {
        new WIONetwork.Api().getApp2AppDetails(clientID, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getStreamToken(MethodChannel.Result result) {
        new WIONetwork.Api().getStreamGenerateToken(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getTotalUsage(MethodChannel.Result result) {
        new WIONetwork.Api().unusedReferrals(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getCreditCoupon(String mail, MethodChannel.Result result) {
        JsonObject object = new JsonObject();
        object.addProperty("email", mail);
        new WIONetwork.Api().creditCoupon(object, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getReferFriendCoupon(MethodChannel.Result result) {
        new WIONetwork.Api().referAFriendCoupon(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getLogicHydrationMethodCall(MethodChannel.Result result) {
        new WIONetwork.Api().getLogicHydration(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }


    private void getShortcutsMethodCall(MethodChannel.Result result) {
        new WIONetwork.Api().getAppShortcutsConfig(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void handleReportErrorMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        JsonObject jsonObject = new Gson().fromJson((String) methodCall.arguments, JsonObject.class);
        try {
            String type = jsonObject.get("event_type").toString();
            LogsType logsType = LogsType.valueOf(type);
            new ErrorSDK(jsonObject, System.currentTimeMillis(), logsType.name().toLowerCase()).sendRequest();
        } catch (Exception exception) {
            exception.getStackTrace();
            new ErrorSDK(jsonObject, System.currentTimeMillis(), LogsType.app.name().toLowerCase()).sendRequest();
        }
        result.success(200);
    }

    private void resendFailedLogs(MethodChannel.Result result) {
        new WIONetwork.Settings().resendFailedRequest();
        result.success(true);
    }

    private void getTotalGoalDays(MethodChannel.Result result) {
        new WIONetwork.Api().getUserTotalGoalDays(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                // result.success(jsonObject.get("body").toString());
                result.success(jsonObject.get("body").getAsJsonObject().get("total_goal_days").getAsInt());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getGroupInfo(String groupId, MethodChannel.Result result) {
        JsonObject object = new JsonObject();
        object.addProperty("group_id", groupId);
        new WIONetwork.Api().getLeaderboardGroupInfo(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        }, groupId);
    }

    private void getLastCapLocation(MethodChannel.Result result) {
        new WIONetwork.Api().getCapLastLocation(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void reportUserLocation(MethodChannel.Result result) {
        new LocationUser((timeStamp, latitude, longitude) -> {
            AnalyticsEvent.Builder builder = AnalyticsEventHelper.reportLocation(timeStamp, latitude, longitude);
            new WIONetwork.Analytics().userLocation(builder.setType(FirebaseTitle.LOCATION.getTitle()));
            result.success(true);
        }).execute();
    }

    private void editGroup(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);

        new WIONetwork.Api().editingLeaderboardGroup(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void createNewGroup(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(arguments), JsonObject.class);

        new WIONetwork.Api().creatingNewLeaderboardGroup(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    // private File imgFromBase64(final Context context, final String imageData) {
    // final byte[] imgBytesData = android.util.Base64.decode(imageData,
    // android.util.Base64.DEFAULT);
    //
    // final File file;
    // try {
    // file = File.createTempFile("image", null, context.getCacheDir());
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // final FileOutputStream fileOutputStream;
    // try {
    // fileOutputStream = new FileOutputStream(file);
    // } catch (FileNotFoundException e) {
    // e.printStackTrace();
    // return null;
    // }
    //
    // final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
    // fileOutputStream);
    // try {
    // bufferedOutputStream.write(imgBytesData);
    // } catch (IOException e) {
    // e.printStackTrace();
    // return null;
    // } finally {
    // try {
    // bufferedOutputStream.close();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }
    // return file;
    // }

    // private void getGroupInfo(String groupId, MethodChannel.Result result) {
    // new WIONetwork.Api().geLeaderboardGroupInfo(new OnResult<JsonObject>() {
    // @Override
    // public void onSuccess(JsonObject jsonObject, String s, int i) {
    // result.success(jsonObject.get("body").toString());
    // }
    //
    // @Override
    // public void onError(String s, int i) {
    // result.error("15", s, i);
    // }
    // }, groupId);
    // }

    private void removeUserFromGroup(Map<String, Object> arguments, MethodChannel.Result
            result) {
        int userIdToRemove = (int) arguments.get("user_id_to_remove");
        String groupId = (String) arguments.get("group_id");
        new WIONetwork.Api().removeUserFromLeaderboardGroup(userIdToRemove, groupId, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void joinGroup(String groupId, MethodChannel.Result result) {
        JsonObject object = new JsonObject();
        object.addProperty("group_id", groupId);
        new WIONetwork.Api().joinToLeaderboardGroup(object, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getUserGroups(MethodChannel.Result result) {
        new WIONetwork.Api().getLeaderboardGroupsID(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void deleteExtraHydrationMethodCall
            (Map<String, Object> arguments, MethodChannel.Result result) {
        long event_time = (long) arguments.get("eventTime");
        String hydration_type = (String) arguments.get("drinkType");

        new WIONetwork.Api().deleteExtraHydration(hydration_type, event_time, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void addMultiHydrationMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(methodCall.arguments), JsonObject.class);
        new WIONetwork.Api().addMultiExtraHydration(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void reportFeedAnalyticMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        JsonObject json = new JsonParser().parse((String) methodCall.arguments).getAsJsonObject();
        new WIONetwork.Analytics().feedAnalytics(json);
        result.success(200);
    }

    private void getLeaderboardMethodCall(Map<String, Object> map, MethodChannel.Result
            result) {
        new LeaderboardModelManager(map, result);
    }

    private void getHolidaysMethodCall(Integer year, MethodChannel.Result result) {
        new WIONetwork.Api().getHoliday(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        }, year.toString());
    }

    private void appToAppMethod(Map<String, Object> map, MethodChannel.Result result) {
        String client_id = map.get("client_id").toString();
        String client_secret = map.get("client_secret").toString();
        String redirect_url = map.get("redirect_url").toString();
        String scopes = map.get("scope").toString();
        // String state = map.get("state").toString();

        new WIONetwork.Api().appToAppLogin(client_id, client_secret, redirect_url, scopes, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getTutorialLinks(MethodChannel.Result result) {
        new WIONetwork.Api().getTutorialLinks(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void shareInFacebookMethodCall(String imagePath, MethodChannel.Result result) {
        Activity activity = null;
        if (listener != null) {
            activity = listener.getParentActivity();
        }

        ShareDialog shareDialog = new ShareDialog(activity);
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        SharePhoto photo = new SharePhoto.Builder().setBitmap(bitmap).build();
        if (ShareDialog.canShow(SharePhotoContent.class)) {
            SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
            shareDialog.show(content);
            result.success("success");
            return;
        }
        result.success("facebook application not found");
    }

    // private void shareLinkInFacebookMethodCall(Map<String, Object> arguments,
    // MethodChannel.Result result) {
    // String text = (String) arguments.get("text");
    // String url = (String) arguments.get("url");
    // Activity activity = null;
    // if (listener != null) {
    // activity = listener.getParentActivity();
    // }
    // if (ShareDialog.canShow(ShareLinkContent.class)) {
    // ShareDialog shareDialog = new ShareDialog(activity);
    // ShareLinkContent content = new ShareLinkContent.Builder()
    // .setQuote(text)
    // .setContentUrl(Uri.parse(url))
    // .build();
    // shareDialog.show(content, ShareDialog.Mode.NATIVE);
    // result.success("success");
    // return;
    // }
    // result.success("facebook application not found");
    // }

    private void getCurrentEnvironmentMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        String serverMode = "production";
        String serverURL = SharedPrefsModuleHelper.getInstance().getBaseConfiguration().getUrl_server().toLowerCase();

        if (serverURL.contains("qa")) {
            serverMode = "qa";
        } else if (serverURL.contains("dev")) {
            serverMode = "dev";
        }
        result.success(serverMode);
    }

    private void handleReportUserCrashMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        JsonObject json = new Gson().fromJson((String) methodCall.arguments, JsonObject.class);
        String msgCrash = json.get("error").toString();
        String jsonAsStringStackTrace = json.get("stackTrace").toString();
        String screenjson = json.get("screen").toString();
        SDKLog sdkLog = new SDKLog(LogsType.app_critical, "Flutter crash", "msgCrash = " + msgCrash,
                "screen name = " + screenjson, "msgCrash = " + msgCrash, jsonAsStringStackTrace);
        new SDKLogsRepository().insert(sdkLog);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("event_time", sdkLog.getTimeStamp());
        jsonObject.addProperty("event_type", sdkLog.getEventType());
        jsonObject.addProperty("event_data", sdkLog.getEventData());
        jsonObject.addProperty("event_src", sdkLog.getExtraData1());
        jsonObject.addProperty("event_desc", sdkLog.getExtraData2());
        jsonObject.addProperty("payload_response", "Flutter crash");
        jsonObject.addProperty("payload_sent", "Flutter crash all info in storage log");
        new ErrorSDK(jsonObject, System.currentTimeMillis(), LogsType.app_critical.name().toLowerCase()).sendRequest();

        new FileUpload(true, () -> {
        }).execute();
        result.success(200);
    }

    private void getLatestAppVersion(MethodChannel.Result result) {
        new WIONetwork.Api().getVersionInfo(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                int buildNumber = -1;
                try {
                    buildNumber = jsonObject.get("body").getAsJsonObject().get("latest_build_number").getAsInt();
                } catch (Exception e) {
                    e.getStackTrace();
                }
                result.success(buildNumber);
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    public interface CallbackGetActivity {
        Activity getParentActivity();

    }

    private void getFT2Res(String macAddress, MethodChannel.Result result) {
        GetFactoryTestResults getFactoryTestResults = new GetFactoryTestResults(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                double offset = DEFAULT_OFFSET;
                try {
                    offset = Double.parseDouble(jsonObject.get("body").getAsJsonObject().get("factory_last_result")
                            .getAsJsonObject().get("cap_offset").toString());
                } catch (Exception e) {
                    e.getStackTrace();
                    try {
                        offset = Integer.parseInt(jsonObject.get("body").getAsJsonObject().get("factory_last_result")
                                .getAsJsonObject().get("cap_offset").toString());
                    } catch (Exception exception) {
                        exception.getStackTrace();
                    }
                }
                result.success((int) offset);
            }

            @Override
            public void onError(String s, int code) {
                if (code == 404) {
                    result.success(DEFAULT_OFFSET);
                } else {
                    result.error("15", s, code);
                }
            }
        }, macAddress);
        getFactoryTestResults.start();
    }

    private void printServerMode() {
        String serverMode = "Prod";
        String serverURL = SharedPrefsModuleHelper.getInstance().getBaseConfiguration().getUrl_server().toLowerCase();

        if (serverURL.contains("qa")) {
            serverMode = "QA";
        } else if (serverURL.contains("dev")) {
            serverMode = "Dev";
        }

        Log.d("SERVER_MODE", "Server mode = " + serverMode);
    }

    private void getFeed(Map<String, Object> arguments, MethodChannel.Result result) {
        int pageSize = (int) arguments.get("page_size");
        String before = (String) arguments.get("before");
        String after = (String) arguments.get("after");

        new WIONetwork.Api().getFeed(pageSize, before, after, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getBadges(MethodChannel.Result result) {
        new WIONetwork.Api().getBadges(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void deleteAccount(MethodChannel.Result result) {
        new WIONetwork.Api().deleteAccount(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void restoreSportActivitiesData
            (Map<String, Object> arguments, MethodChannel.Result result) {
        int pageIndex = (int) arguments.get("page_index");
        int pageSize = (int) arguments.get("page_size");
        int startTime = (int) arguments.get("start_time");

        new WIONetwork.Api().restoreSportUserData(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        }, pageSize, pageIndex, startTime);
    }

    private void sendSupportMsg(String jsonObjectAsString, MethodChannel.Result result) {
        JsonObject jsonObject = new Gson().fromJson(jsonObjectAsString, JsonObject.class);
        String appVersion = getAppVersion();
        jsonObject.addProperty("app_version", "Android-" + appVersion);
        new WIONetwork.Analytics().support(jsonObject);
        result.success(true);
    }

    private String getAppVersion() {
        try {
            Context context = ModuleApp.getContext();
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private void restoreUserData(MethodChannel.Result result) {
        new WIONetwork.Api().restoreUserData(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void restoreEmailData(MethodChannel.Result result) {
        new WIONetwork.Api().restoreEmailData(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    public final static String TOGGLE_SPORT_WEATHER = "TOGGLE_SPORT_WEATHER";

    private void storeUserData(Map<String, Object> arguments, MethodChannel.Result result) {
        JsonObject jsonObject = (new Gson()).fromJson((new Gson()).toJson(arguments), JsonObject.class);
        boolean isExist = jsonObject.has("active_notifications_channels");
        if (isExist) {
            boolean isSportWeatherStatusOn = jsonObject.get("active_notifications_channels").toString()
                    .contains("updates");
            SharedPrefsModuleHelper.getInstance().setStoreData(
                    StoreType.STRING, String.valueOf(isSportWeatherStatusOn), TOGGLE_SPORT_WEATHER);
        }
        new WIONetwork.Api().userConfig(jsonObject, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getNotification(String arg, MethodChannel.Result result) {
        // TODO daniel impl
        String id = arg;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("mok", "Amit getNotification");
        result.success(jsonObject.toString());
    }

    private void getNotificationList(Map<String, Object> arguments, MethodChannel.Result
            result) {
        int pageIndex = (int) arguments.get("index");
        int pageSize = (int) arguments.get("amount");
        String message_code = (String) arguments.getOrDefault("message_code", null);
        Integer min_timestamp = (Integer) arguments.getOrDefault("minimum_date_time", null);
        Boolean include_silent = (Boolean) arguments.getOrDefault("include_silent", null);

        // Long min_timestamp = (Long) arguments.getOrDefault("minimum_date_time",
        // null);
        // Long min_timestamp =
        // Long.valueOf(String.valueOf(arguments.getOrDefault("minimum_date_time",
        // null)));

        GetNotificationData.MessageType messageType = null;

        OnResult<JsonObject> onResultCallback = new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        };

        if (message_code != null) {
            try {
                messageType = GetNotificationData.MessageType.valueOf(message_code);
            } catch (Exception exception) {
                exception.getStackTrace();
            }
        }

        if (min_timestamp != null && messageType != null) {
            if (include_silent != null) {
                new WIONetwork.Api().getNotificationDataWithFilterTimeAndTypeWithSilent(pageSize, pageIndex,
                        messageType,
                        (long) min_timestamp, include_silent, onResultCallback);
            } else {
                new WIONetwork.Api().getNotificationDataWithFilterTimeAndType(pageSize, pageIndex, messageType,
                        (long) min_timestamp, onResultCallback);
            }
        } else if (messageType != null) {
            if (include_silent != null) {
                new WIONetwork.Api().getNotificationDataWithFilterByTypeWithSilent(pageSize, pageIndex, messageType,
                        include_silent, onResultCallback);
            } else {
                new WIONetwork.Api().getNotificationDataWithFilterByType(pageSize, pageIndex, messageType,
                        onResultCallback);
            }
        } else if (min_timestamp != null) {
            if (include_silent != null) {
                new WIONetwork.Api().getNotificationDataWithFilterTimeWithSilent(pageSize, pageIndex,
                        (long) min_timestamp, include_silent,
                        onResultCallback);
            } else {
                new WIONetwork.Api().getNotificationDataWithFilterTime(pageSize, pageIndex, (long) min_timestamp,
                        onResultCallback);
            }
        } else {
            if (include_silent != null) {
                new WIONetwork.Api().getNotificationDataWithSilent(pageSize, pageIndex, include_silent,
                        onResultCallback);
            } else {
                new WIONetwork.Api().getNotificationData(pageSize, pageIndex, onResultCallback);

            }
        }

    }

    private void disconnectWeatherCity(MethodChannel.Result result) {
        new WIONetwork.Api().disconnectWeatherCity(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int code) {
                result.success(code == 200);
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getCityWeatherByGps(Map<String, Object> argsMap, MethodChannel.Result
            result) {

        // String place_id = (String) argsMap.get("place_id");
        Double lon = (Double) argsMap.get("lon");
        Double lat = (Double) argsMap.get("lat");

        new WIONetwork.Api().getWeatherCity(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        }, String.valueOf(lat), String.valueOf(lon));
        // }
    }

    private void getCityWeatherManually(String place_id, MethodChannel.Result result) {

        new WIONetwork.Api().getWeatherCity(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        }, place_id);
    }

    private void getCityAutoComplete(Map<String, Object> argsMap, MethodChannel.Result
            result) {

        String city = (String) argsMap.get("place");
        String session_token = (String) argsMap.get("token");

        new WIONetwork.Api().getCityAutoComplete(city, session_token, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void handleGetUserLocation(MethodCall methodCall, MethodChannel.Result result) {
        JsonObject jsonObject = new JsonObject();
        LocationEvent locationEvent = SharedPrefsModuleHelper.getInstance().getLastLocation();
        if (locationEvent != null) {
            jsonObject.addProperty("latitude", locationEvent.getLatitude());
            jsonObject.addProperty("longitude", locationEvent.getLongitude());
            jsonObject.addProperty("timestamp", locationEvent.getTimeStamp());
            result.success(jsonObject.toString());
        } else {
            new LocationUser(3, (timeStamp, latitude, longitude) -> {
                jsonObject.addProperty("latitude", latitude);
                jsonObject.addProperty("longitude", longitude);
                jsonObject.addProperty("timestamp", timeStamp);
                result.success(jsonObject.toString());
            }).execute();
        }
    }

    private void handleAddReportSportActivityMethodCall(MethodCall
                                                                methodCall, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(methodCall.arguments), JsonObject.class);
        new WIONetwork.Api().addSportActivity(json, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void handleReportUpdateSportActivityMethodCall
            (Map<String, Object> argsMap, MethodChannel.Result result) {
        JsonObject json = new Gson().fromJson(new Gson().toJson(argsMap.get("data")), JsonObject.class);
        String sport_id = (String) argsMap.get("sport_id");

        new WIONetwork.Api().editSportActivity(json, sport_id, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void handleReportDeleteSportActivityMethodCall(String sportID, MethodChannel.Result
            result) {
        new WIONetwork.Api().deleteSportActivity(sportID, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void handleReportDeleteAllSportActivitiesMethodCall(MethodChannel.Result result) {
        new WIONetwork.Api().deleteAllSportActivities(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getExternalService(MethodChannel.Result result) {
        new WIONetwork.Api().getServicesHealthStatus(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });
    }

    private void getHydrationDailyGoal(MethodChannel.Result result) {
        new WIONetwork.Api().getHydrationDailyGoals(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });

    }

    private void getHydrationDailyGoalDetails
            (Map<String, Object> argsMap, MethodChannel.Result result) {
        String type = (String) argsMap.get("type");
        String timeOffset = (String) argsMap.get("timeOffset");

        new WIONetwork.Api().getHydrationDailyGoalDetails(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        }, type, timeOffset);

    }

    private void disconnectFromHealthService(String arguments, MethodChannel.Result result) {
        try {
            HealthService healthService = HealthService.valueOf(arguments);
            new WIONetwork.Api().disconnectHealthService(new OnResult<JsonObject>() {
                @Override
                public void onSuccess(JsonObject jsonObject, String s, int code) {
                    result.success(code == 200);
                }

                @Override
                public void onError(String s, int i) {
                    result.error("15", s, i);
                }
            }, healthService.name());

        } catch (Exception e) {
            e.getMessage();
            result.error("15", "parsing error", null);
        }
    }

    private void loadStravaURL(MethodChannel.Result result) {
        loadURL("strava/login-url", result);
    }

    private void loadGarminURL(MethodChannel.Result result) {
        loadURL("garmin-login-url", result);
    }

    private void loadWhoopURL(MethodChannel.Result result) {
        loadURL("whoop-login-url", result);
    }

    private void loadTerraURL(String resource, MethodChannel.Result result) {
        loadURL("terra-login-url?resource=" + resource, result);
    }

    private void loadURL(String deepLink, MethodChannel.Result result) {
        new WIONetwork.Api().getRedirectLinkURL(deepLink, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                String redirect_url = jsonObject.get("body").getAsJsonObject().get("redirect_url").getAsString();
                result.success(redirect_url);
            }

            @Override
            public void onError(String s, int i) {
                result.error("15", s, i);
            }
        });

    }

    private void getCapVersionsSupported(String capVersion, MethodChannel.Result result) {
        new NetworkAPIsOTA().getCapVersionsSupported(new ICallbackGetNewestVersion() {
            @Override
            public void onError(String desc) {
                result.error("15", desc, desc);
            }

            @Override
            public void onSuccess(String newVersion, String priority, String description) {
                result.success(getVersionResponse(newVersion, priority, description));
            }
        });
    }

    private String getVersionResponse(String version, String priority, String description) {
        JsonObject objectRes = new JsonObject();
        objectRes.addProperty("version", version);
        objectRes.addProperty("priority", priority);
        objectRes.addProperty("description_to_app", description);
        return objectRes.toString();
    }

    private void handleReportAttributes(MethodCall methodCall, MethodChannel.Result result) {
        switch ((String) methodCall.arguments) {
            case "START_APP":
                new WIONetwork.Settings().reportAttributes(TriggerReport.START_APP);
                break;
            case "CLOSE_APP_IN_WIZARD":
                new WIONetwork.Settings().reportAttributes(TriggerReport.CLOSE_APP_IN_WIZARD);
                break;
            case "FINISH_WIZARD":
                new WIONetwork.Settings().reportAttributes(TriggerReport.FINISH_WIZARD);
                break;
        }

        result.success(null);
    }

    private void handleFetchBottleList(MethodChannel.Result result) {
        new WIONetwork.Api().getCalibrationBottlesList(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void handleDailyGoal(MethodChannel.Result result) {
        new WIONetwork.Api().buildUserHydrationProfile(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").getAsJsonObject().get("hydration_profile").getAsInt());
            }

            @Override
            public void onError(String s, int i) {
                // result.success(2000);
                result.error(s, s, i);
            }
        });
    }

    private void handleFetchRemoteCalibrationTable(String bottleName, MethodChannel.Result
            result) {
        new WIONetwork.Api().getCalibrationList(bottleName, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void getEncouragementSentence(MethodChannel.Result result) {
        new WIONetwork.Api().getEncouragementSentence(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").getAsString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void getCalendarDailyGoal(Map<String, Object> argsMap, MethodChannel.Result
            result) {
        String yearMonth = (String) argsMap.get("date");
        String timeOffset = (String) argsMap.get("timeOffset");

        new WIONetwork.Api().getCalendarDailyGoal(yearMonth, timeOffset, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void getDailyHydration(Map<String, Object> argsMap, MethodChannel.Result result) {
        String date = (String) argsMap.get("date");
        String timeOffset = (String) argsMap.get("timeOffset");

        new WIONetwork.Api().getDailyHydration(date, timeOffset, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void notificationReminder(Map<String, Object> argsMap, MethodChannel.Result
            result) {
        String reminder_type = (String) argsMap.get("type");
        String interval = (String) argsMap.get("interval");

        new WIONetwork.Api().notificationReminder(reminder_type, interval, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void timeRangeHydrationDay(Map<String, Object> argsMap, MethodChannel.Result
            result) {
        String start_day = (String) argsMap.get("startDay");
        String end_day = (String) argsMap.get("endDay");
        new WIONetwork.Api().timeRangeHydrationDay(start_day, end_day, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void manualDailyGoal(Map<String, Object> argsMap, MethodChannel.Result result) {
        Boolean is_manual = (Boolean) argsMap.get("isManual");
        int goal = (int) argsMap.get("goal");

        new WIONetwork.Api().manualDailyGoalPlaster(is_manual, goal, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, s, i);
            }
        });
    }

    private void handleSetDrsProcessData(Map<String, Object> argsMap, MethodChannel.Result
            result) {
        JsonObject jsonObject = (new Gson()).fromJson((new Gson()).toJson(argsMap), JsonObject.class);

        new WIONetwork.Api().alexaSettings(jsonObject, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String msg, int code) {
                // Log.v("TAAAAA", jsonObject + "\n" + msg + "\n" + code);
            }

            @Override
            public void onError(String msg, int code) {
                // Log.v("TAAAAA", msg + "\n" + code);
            }
        });
        result.success(null);
    }

    private void handleDoesDrsUserSkillEnabled(MethodChannel.Result result) {
        new WIONetwork.Api().alexaSettings(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String msg, int code) {
                boolean isEnabled = jsonObject.get("body").getAsJsonObject().get("is_skill_enabled").getAsBoolean();
                result.success(isEnabled);
            }

            @Override
            public void onError(String msg, int code) {
                result.error("Err", "unable to reach is enabeld req", null);
            }
        });
    }

    private void handleGetDrsHostCode(Map<String, Object> argsMap, MethodChannel.Result
            result) {
        String clientId = (String) argsMap.get("clientId"); // man or cap
        String redirectUrl = (String) argsMap.get("redirectUrl");

        new WIONetwork.Api().amazonDRS(clientId, redirectUrl, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                String hostCode = jsonObject.get("body").getAsString();
                result.success(hostCode);
            }

            @Override
            public void onError(String s, int i) {
                result.error("Err", "unable to reach get drs host code req", null);
            }
        });
    }

    private void handleAppTermsAndCondsVersionReq(MethodChannel.Result result) {
        new WIONetwork.Api().getTermsAndConditions(true, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").getAsJsonObject().get("version").getAsString());
            }

            @Override
            public void onError(String s, int i) {
                result.success("1");
            }
        });
    }

    private void sendSdkLogs(MethodCall methodCall, MethodChannel.Result result) {
        new FileUpload(true, () -> {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    result.success(null);
                }
            });
        }).execute();
    }

    private void handleAppConfiguration(MethodCall methodCall, MethodChannel.Result result) {
        new WIONetwork.Api().getSystemConfigurations(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").toString());
            }

            @Override
            public void onError(String s, int i) {
                result.error(s, "error - " + i, null);
            }
        });
    }

    private void handleAppTermsAndCondsLinkReq(boolean isHeb, MethodChannel.Result
            result) {
        new WIONetwork.Api().getTermsAndConditions(isHeb, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").getAsJsonObject().get("terms").getAsString());
            }

            @Override
            public void onError(String s, int i) {
                result.success("https://www.water-io.com/default-terms-and-conditions");
            }
        });
    }

    private void handleAppPrivacyPolicyLinkReq(boolean isHeb, MethodChannel.Result result) {
        new WIONetwork.Api().getTermsAndConditions(isHeb, new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                result.success(jsonObject.get("body").getAsJsonObject().get("privacy").getAsString());
            }

            @Override
            public void onError(String s, int i) {
                result.success("https://www.water-io.com/default-terms-and-conditions");
            }
        });
    }

    private void handleAppTypeRequestMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        result.success(new WIONetwork.Settings().getAppType());
    }

    private void handleUserIdRequestMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        Integer userId = new WIONetwork.Settings().getUserId();
        userId = userId == -1 ? null : userId;
        result.success(userId);
    }

    private void handleUserScoreRequestMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        new WIONetwork.Api().getUserScore(new OnResult<JsonObject>() {
            @Override
            public void onSuccess(JsonObject jsonObject, String s, int i) {
                // int score =
                // jsonObject.get("body").getAsJsonObject().get("score").getAsInt();
                try {
                    String response = jsonObject.get("body").toString();
                    // Log.d("NETWORK", "getUserScore(), " + response);
                    result.success(response);
                } catch (Exception e) {
                    e.getMessage();
                    e.getStackTrace();
                }

            }

            @Override
            public void onError(String s, int i) {
                result.success(0);
            }
        });
    }

    private void handleReportUserLogMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        JsonObject json = new JsonParser().parse((String) methodCall.arguments).getAsJsonObject();
        new WIONetwork.Analytics().userLog(json);
        result.success(200);
    }

    private void handleReportScreenMethodCall(MethodCall methodCall, MethodChannel.Result
            result) {
        String screenName = (String) methodCall.arguments;
        new WIONetwork.Analytics().appScreens(screenName, null);
        result.success(200);
    }

    private void handleUserHydrationReportMethodCall(MethodCall
                                                             methodCall, MethodChannel.Result result) {
        Map<String, Object> map = (Map<String, Object>) methodCall.arguments;
        int amount = (int) map.get("amount");
        long timestamp = ((Number) map.get("timestamp")).longValue();
        int waterLevelStart = (int) map.get("water_level_start");
        int waterLevelEnd = (int) map.get("water_level_end");
        String drinkType = (String) map.get("drink_type");
        String eventSource = (String) map.get("event_source"); // man or cap

        AnalyticsEvent.Builder analytics = new AnalyticsEvent.Builder()
                .setTime(timestamp)
                .setAmount(amount)
                .setSource(eventSource)
                .setLevelStart(waterLevelStart)
                .setLevelEnd(waterLevelEnd)
                .setTimeSpan(0)
                .setUsageType(drinkType)
                .build();

        new WIONetwork.Analytics().userHydration(analytics);
        result.success(200);
    }
}

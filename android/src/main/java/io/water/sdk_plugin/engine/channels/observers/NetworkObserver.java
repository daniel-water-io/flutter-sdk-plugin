package io.water.sdk_plugin.engine.channels.observers;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.NETWORK_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.NETWORK_USER_CLICK_URL;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonObject;
import com.water.water_io_sdk.network.interfaces.OnResult;
import com.water.water_io_sdk.network.networks.WIONetwork;

import io.flutter.Log;
import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;
import io.water.sdk_plugin.utils.NetworkCallbackDeepLink;

public class NetworkObserver extends ChannelObserver<String> {

    public NetworkObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner) {
        super(binaryMessenger, NETWORK_CHANNEL, NETWORK_USER_CLICK_URL, owner, new MutableLiveData<>());
        init();
    }

    private void init() {
        NetworkCallbackDeepLink.getInstance().setListener(intentResult -> {
            String url = intentResult.getData().toString();
            onChanged(url);
         });
    }

    @Override
    public void onChanged(String URL) {
        invokeTarget(URL);
    }

}


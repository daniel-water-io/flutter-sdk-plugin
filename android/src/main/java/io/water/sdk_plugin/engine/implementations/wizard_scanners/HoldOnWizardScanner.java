package io.water.sdk_plugin.engine.implementations.wizard_scanners;

import android.os.Handler;

import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractFirstConnection;
import com.water.water_io_sdk.ble.entities.WIOSmartDevice;
import com.water.water_io_sdk.ble.interfaces.CallbackScanRequest;

import androidx.lifecycle.LifecycleOwner;

import io.flutter.Log;
import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.implementations.wizard_scanners.base.BaseWizardScan;
import io.water.sdk_plugin.engine.implementations.wizard_scanners.base.IWizardScanMacAddressListener;
import io.water.sdk_plugin.utils.Constants;
import io.water.sdk_plugin.utils.DeviceLogEventsUnifier;

public class HoldOnWizardScanner extends BaseWizardScan {

    private final ProcedureAbstractFirstConnection mProcedureAbstractFirstConnection;

    public HoldOnWizardScanner(BinaryMessenger mBinaryMessenger, WIOSmartDevice mWIOSmartDevice,
                               LifecycleOwner mLifecycleOwner, IWizardScanMacAddressListener iWizardScanMacAddressListener,
                               ProcedureAbstractFirstConnection procedureAbstractFirstConnection,
                               DeviceLogEventsUnifier deviceLogEventsUnifier
    ) {
        super(mBinaryMessenger, mWIOSmartDevice, mLifecycleOwner, iWizardScanMacAddressListener);
        this.mProcedureAbstractFirstConnection = procedureAbstractFirstConnection;
        this.mDeviceLogEventsUnifier = deviceLogEventsUnifier;
    }

    @Override
    public boolean startScanning(CallbackScanRequest scanRequest) {
        return mWIOSmartDevice.startCustomScanningWizardMode(scanRequest, mProcedureAbstractFirstConnection, mWizardModeCallback);
    }

    @Override
    public void onYesClicked() {
        super.onYesClicked();
        if (mListener != null) {
            mListener.clientResponse(true, () -> {
                if (mListener != null) {
                    mListener.continueTONextCMD(Constants.HOLD_ON_KEY);
                }
                mPotentialCapConnectedObserver = null;
                mListener = null;
            });
        }
    }

    @Override
    protected void onDisconnect(String macAddress, String version) {
        if (mWasThatTheRelevantCap == null) {
            mWasThatTheRelevantCap = false;
            mMacAddressObservable.setValue(null);
            new Handler().postDelayed(() -> {
                if (mListener != null) {
                    mListener.removeDeviceFromBlackList(macAddress);
                }
            }, 1000);
        }
    }
}

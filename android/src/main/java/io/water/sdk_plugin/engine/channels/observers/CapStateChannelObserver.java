package io.water.sdk_plugin.engine.channels.observers;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.water.water_io_sdk.ble.entities.DeviceLog;
import java.util.ArrayList;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_STATE_CHANNEL_PATH;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CAP_STATE_CHANNEL_TARGET_METHOD;

public class CapStateChannelObserver extends ChannelObserver<ArrayList<DeviceLog>> {

    public CapStateChannelObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner, LiveData observable, LiveData liveDataBatteryPercent) {
        super(binaryMessenger, CAP_STATE_CHANNEL_PATH, CAP_STATE_CHANNEL_TARGET_METHOD, owner, observable);
        new BatteryStatusChannelObserver(getMethodChannel(), owner, liveDataBatteryPercent);
    }

    @Override
    public void onChanged(ArrayList<DeviceLog> deviceLogs) {
        JsonArray myCustomArray =new JsonArray();
        for (int i = 0; i < deviceLogs.size(); i++) {
            DeviceLog deviceLog = deviceLogs.get(i);
            JsonObject element=new JsonObject();
            element.addProperty("event_type", deviceLog.getEventType());
            element.addProperty("timestamp", deviceLog.getTimeStamp());
            element.addProperty("measurement", deviceLog.getMeasurement());
            element.addProperty("extra_data", deviceLog.getExtraData());
            element.addProperty("extra_data2", deviceLog.getExtraData2());
            element.addProperty("cap_mac_address", deviceLog.getMacAddress());
            myCustomArray.add(element);
        }
        invokeTarget(myCustomArray.toString());
    }


    // TODO - MAKE AN EXTRENAL FILE FOR THE CONSTANTS ABOVE - 'event_type' etc..
}



package io.water.sdk_plugin.engine.channels.observers;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.AUTH_USER_UNAUTHENTICATED_CHANGED;

import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.water.water_io_sdk.network.interfaces.Callback401;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.network.utilities.constants.ConstantRequest;

import io.flutter.plugin.common.BinaryMessenger;
import io.water.sdk_plugin.engine.channels.observers.base.ChannelObserver;

public class UnauthenticatedObserver extends ChannelObserver<Boolean> implements Callback401 {

    public UnauthenticatedObserver(BinaryMessenger binaryMessenger, LifecycleOwner owner) {
        super(binaryMessenger, AUTH_CHANNEL, AUTH_USER_UNAUTHENTICATED_CHANGED, owner, new MutableLiveData<>());
        init();
    }

    private void init() {
        new WIONetwork.Settings().setListener401API(this);
    }

    @Override
    public void onChanged(Boolean aBoolean) {
        invokeTarget(aBoolean);
    }

    private long timeStampLastUpdate = 0;

    @Override
    public void on401Response(int codeResponse, String msgError, String typeRequest) {
        if (typeRequest.equals(ConstantRequest.REFRESH_FCM_TOKEN)) {
            Log.e("AMIT401", "Ignore " + codeResponse + ", " + msgError + ", " + typeRequest);
            return;
        }
        if (System.currentTimeMillis() - timeStampLastUpdate < (1000 * 60)) {
            return;
        }
        Log.e("AMIT401", codeResponse + ", " + msgError + ", " + typeRequest);
        timeStampLastUpdate = System.currentTimeMillis();
        onChanged(false);
    }
}


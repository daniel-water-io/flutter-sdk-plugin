package io.water.sdk_plugin.engine.channels.callbacks;

import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.PluginConfiguration;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;

import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CONFIG_PARAMS_METHOD;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.PLUGIN_CONFIG_CHANNEL;

public class PluginConfigCallbackChannel extends CallbackChannel {
    private final IOnConfigurationReady mListener;

    public PluginConfigCallbackChannel(BinaryMessenger binaryMessenger, IOnConfigurationReady listener) {
        super(binaryMessenger, PLUGIN_CONFIG_CHANNEL);
        mListener = listener;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case CONFIG_PARAMS_METHOD:
                handlePluginConfigurations((Map<String, Object>) methodCall.arguments);
                result.success(null);
                break;
        }
    }

    private void handlePluginConfigurations(Map<String, Object> args) {
        PluginConfiguration pluginConfiguration = new PluginConfiguration(args);

        if (mListener != null) {
            mListener.onConfigurationReady(pluginConfiguration);
        }
    }

    public interface IOnConfigurationReady {
        void onConfigurationReady(PluginConfiguration pluginConfiguration);
    }
}

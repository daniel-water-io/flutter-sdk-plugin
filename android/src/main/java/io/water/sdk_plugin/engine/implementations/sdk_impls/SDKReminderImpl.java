package io.water.sdk_plugin.engine.implementations.sdk_impls;

import android.content.Context;
import android.util.Log;

import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.reminder_local_push.entities.Reminder;
import com.water.water_io_sdk.reminder_local_push.entities.WIOReminderHandler;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import io.water.sdk_plugin.background.database.repositories.VitaminsRepository;
import io.water.sdk_plugin.background.entities.DeviceLogDB;
import io.water.sdk_plugin.utils.NotificationUtils;

public class SDKReminderImpl extends WIOReminderHandler {

    private static final String PUSH_REMOTE_CHANNEL = "Pill reminder";
    private static final String DESCRIPTION_PUSH = "Local message reminder";

    @Override
    public void onWIOReminderReceived(Context context, Reminder reminder) {
        if (isThisReminderAlreadyTaken(context, reminder)) {
            return;
        }

        String title = (String) SharedPrefsModuleHelper.getInstance().getStoreData(StoreType.STRING, "NOTIFICATION_TITLE");
        String bodyMessage = getRandomTextBody();

        new NotificationUtils(PUSH_REMOTE_CHANNEL, DESCRIPTION_PUSH)
                .showNotification(title, bodyMessage, context);

        AnalyticsEvent.Builder builder = new AnalyticsEvent.Builder()
                .setAction("sent")
                .setBody(bodyMessage)
                .setTime(System.currentTimeMillis())
                .setMessageCode(reminder.getBody())
                .setMessageType("notification")
                .setTitle("pill reminder")
                .build();

        new WIONetwork.Analytics().userNotifications(builder);

        AnalyticsEvent.Builder builderUserLog = new AnalyticsEvent.Builder()
                .setType("notification_reminder")
                .setData(reminder.getHour() + ":" + reminder.getMinutes())
                .setText("Notification sent")
                .setTime(System.currentTimeMillis())
                .setSource("man")
                .build();

        new WIONetwork.Analytics().userLog(builderUserLog);
    }

    private boolean isThisReminderAlreadyTaken(Context context, Reminder reminder) {
        ArrayList<Reminder> reminders = getCurrRemindersList();

        int currReminderIndex = -1;
        int prevReminderInMins = -1;
        int currReminderInMins = -1;
        int nextReminderInMins = -1;

        for (int i = 0; i < reminders.size(); i++) {
            if (reminder.getHourAndMinutesAsSecond() == reminders.get(i).getHourAndMinutesAsSecond()) {
                currReminderIndex = i;
                if (i == 0) {
                    prevReminderInMins = 0;
                } else {
                    prevReminderInMins = reminders.get(i - 1).getHourAndMinutesAsSecond();
                }

                if (i == reminders.size() - 1) {
                    nextReminderInMins = 23 * 60 + 59;
                } else {
                    nextReminderInMins = reminders.get(i + 1).getHourAndMinutesAsSecond();
                }

                currReminderInMins = reminders.get(i).getHourAndMinutesAsSecond();
                break;
            }
        }

        int startAnchorTimestamp = (currReminderInMins + prevReminderInMins) / 2;
        int endAnchorTimestamp = (currReminderInMins + nextReminderInMins) / 2;

        printAnchor(startAnchorTimestamp);
        printAnchor(endAnchorTimestamp);

        long startTt = anchorToTimestamp(startAnchorTimestamp);
        long endTt = anchorToTimestamp(endAnchorTimestamp);

        ArrayList<DeviceLogDB> pillTakenLogs = VitaminsRepository.getInstance().getAllPillTakenLogsList(
                startTt, endTt
        );

        return pillTakenLogs.size() > 0;
    }

    private ArrayList<Reminder> getCurrRemindersList() {
        SharedPrefsModuleHelper helper = SharedPrefsModuleHelper.getInstance();
        Reminder[] reminders = helper.getRemindersCmdList();

        ArrayList<Reminder> result = new ArrayList<>();
        for (int i = 0; i < reminders.length; i++) {
            if (reminders[i].getHourAndMinutesAsSecond() > 0) {
                result.add(reminders[i]);
            }
        }

        return result;
    }

    private void printAnchor(int anchorTimestamp) {
        String pp = anchorTimestamp / 60 + ":" + anchorTimestamp % 60;
    }

    private String getRandomTextBody() {
        int randMsgsLength = (int) SharedPrefsModuleHelper.getInstance()
                .getStoreData(StoreType.INTEGER, "RANDOM_MSGS_LEN");

        if (randMsgsLength <= 0) {
            return "";
        }

        int randomItemIndex = new Random().nextInt(randMsgsLength);

        return (String) SharedPrefsModuleHelper.getInstance()
                .getStoreData(StoreType.STRING, "RANDOM_MSG_ITEM_" + randomItemIndex);
    }

    private long anchorToTimestamp(int minsSingMidnight) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, minsSingMidnight % 60);
        calendar.set(Calendar.HOUR_OF_DAY, minsSingMidnight / 60);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

}

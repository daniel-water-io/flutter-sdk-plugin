package io.water.sdk_plugin.engine.channels.callbacks;

import com.water.water_io_sdk.reminder_local_push.entities.Reminder;
import com.water.water_io_sdk.reminder_local_push.utilities.ReminderManager;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;
import com.water.water_io_sdk.storage.prefs.storeSP.StoreType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.water.sdk_plugin.background.database.repositories.VitaminsRepository;
import io.water.sdk_plugin.background.entities.DeviceLogDB;
import io.water.sdk_plugin.engine.channels.callbacks.base.CallbackChannel;

import static io.water.sdk_plugin.background.database.repositories.VitaminsRepository.PILL_TAKEN_KEY;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.CANCEL_ONCE_LOCAL_PUSH_REMINDERS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.LOCAL_PUSH_NOTIFICATIONS_CHANNEL;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_LOCAL_PUSHS_RAND_MSGS;
import static io.water.sdk_plugin.engine.channels.ChannelsConsts.SET_LOCAL_PUSHS_REMINDERS;

public class LocalPushNotificationsCallbackChannel extends CallbackChannel {
    public LocalPushNotificationsCallbackChannel(BinaryMessenger binaryMessenger) {
        super(binaryMessenger, LOCAL_PUSH_NOTIFICATIONS_CHANNEL);
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case SET_LOCAL_PUSHS_REMINDERS:
                handleSetReminders((ArrayList<HashMap<String, Integer>>) methodCall.arguments, result);
                break;
            case SET_LOCAL_PUSHS_RAND_MSGS:
                handleSetRandomMsgs((HashMap<String, Object>) methodCall.arguments, result);
                break;
            case CANCEL_ONCE_LOCAL_PUSH_REMINDERS:
                handleCancelOnceLocalPushReminder((HashMap<String, Integer>) methodCall.arguments, result);
                break;
        }
    }

    private void handleCancelOnceLocalPushReminder(HashMap<String, Integer> reminderMap, MethodChannel.Result result) {
        int reminderHours = reminderMap.get("hours");
        int reminderMinutes = reminderMap.get("minutes");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, reminderHours);
        calendar.set(Calendar.MINUTE, reminderMinutes);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long reminderTimestamp = calendar.getTimeInMillis();
        DeviceLogDB fictiveLogEvent = new DeviceLogDB("C", reminderTimestamp, true);
        fictiveLogEvent.setExtraData3(PILL_TAKEN_KEY);
        long idRow = VitaminsRepository.getInstance().insertDeviceLogDB(fictiveLogEvent);

        result.success(null);
    }

    private void handleSetRandomMsgs(HashMap<String, Object> args, MethodChannel.Result result) {
        String title = (String) args.get("title");

        SharedPrefsModuleHelper.getInstance()
                .setStoreData(StoreType.STRING, title, "NOTIFICATION_TITLE");

        ArrayList<String> msgsList = (ArrayList<String>) args.get("msgs");

        SharedPrefsModuleHelper.getInstance()
                .setStoreData(StoreType.INTEGER, msgsList.size(), "RANDOM_MSGS_LEN");

        for (int i = 0; i < msgsList.size(); i++) {
            SharedPrefsModuleHelper.getInstance()
                    .setStoreData(StoreType.STRING,
                            msgsList.get(i),
                            "RANDOM_MSG_ITEM_" + i);
        }

        result.success(null);
    }

    private void handleSetReminders(ArrayList<HashMap<String, Integer>> args, MethodChannel.Result result) {
        handleSetDeviceReminders(args, result);
    }

    private void handleSetDeviceReminders(ArrayList<HashMap<String, Integer>> args, MethodChannel.Result result) {
        Reminder[] reminders;

        if (args != null) {
            reminders = new Reminder[args.size()];
            ReminderManager.getInstance().notification().removeAllReminder();

            for (int i = 0; i < args.size(); i++) {
                HashMap<String, Integer> reminderHM = args.get(i);
                reminders[i] = new Reminder(reminderHM.get("hours"), reminderHM.get("minutes"));
                reminders[i].setBody("message_reminder_" + i);
                ReminderManager.getInstance().notification().addReminder(reminders[i]);
            }
        }

        result.success(null);
    }
}

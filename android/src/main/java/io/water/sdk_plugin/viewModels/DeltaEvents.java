package io.water.sdk_plugin.viewModels;


public class DeltaEvents {
    private long lastTimestampHasError = -1;
    private final int deltaTrigger;
    private boolean mIsAlreadyReported = false;

    public DeltaEvents(int deltaTriggerMillisecond) {
        this.deltaTrigger = deltaTriggerMillisecond;
    }

    public boolean isAlreadyReported() {
        return mIsAlreadyReported;
    }

    public void setAlreadyReported(boolean alreadyReported) {
        mIsAlreadyReported = alreadyReported;
    }

    public void setLastTimestampHasError(long lastTimestampHasError) {
        this.lastTimestampHasError = lastTimestampHasError;
    }

    public boolean isReasonableProfits() {
        return (System.currentTimeMillis() - lastTimestampHasError) > (deltaTrigger);
    }

    public boolean isReasonableProfitsHalfTime() {
        return (System.currentTimeMillis() - lastTimestampHasError) > (deltaTrigger/2);
    }

}

package io.water.sdk_plugin;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.water.water_io_sdk.ble.enums.AppMode;
import com.water.water_io_sdk.network.entities.requests.AnalyticsEvent;
import com.water.water_io_sdk.network.networks.WIONetwork;
import com.water.water_io_sdk.network.utilities.AnalyticsEventHelper;
import com.water.water_io_sdk.storage.prefs.SharedPrefsModuleHelper;

import androidx.annotation.NonNull;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.embedding.engine.plugins.lifecycle.FlutterLifecycleAdapter;
import io.water.sdk_plugin.background.utilities.LifeCycleApp;
import io.water.sdk_plugin.background.utilities.LifeCycleAppForPush;
import io.water.sdk_plugin.engine.SDKEngine;
import io.water.sdk_plugin.engine.channels.callbacks.PluginConfigCallbackChannel;

public class SdkPlugin implements FlutterPlugin, ActivityAware {
    // public static SDKEngine engine;
    private FlutterPluginBinding pluginBinding;
    private ActivityPluginBinding activityBinding;
    private Application application;
    private Activity activity;
    // v2 embedding;
    private Lifecycle lifecycle;
    private LifeCycleObserver observer;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        pluginBinding = flutterPluginBinding;
        Log.v("DANIUELSFAAS", "onAttachedToEngine()");

        reportAppState(true);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        pluginBinding = null;

        reportAppState(false);
    }

    @Override
    public void onAttachedToActivity(ActivityPluginBinding binding) {
        activityBinding = binding;

        this.activity = activityBinding.getActivity();
        this.application = (Application) pluginBinding.getApplicationContext();

        storeActivityNameForClassInflation(activity);

        startConfigChannel();
    }

    private void startConfigChannel() {
        new PluginConfigCallbackChannel(pluginBinding.getBinaryMessenger(), this::initObserver);
    }

    private void initObserver(PluginConfiguration pluginConfiguration) {
        observer = new LifeCycleObserver(activity, pluginConfiguration);

        // V2 embedding setup for activity listeners.
        lifecycle = FlutterLifecycleAdapter.getActivityLifecycle(activityBinding);
        lifecycle.addObserver(observer);
    }

    private void storeActivityNameForClassInflation(Activity activity) {
        String activityClassName = activity.getClass().getName();

        SharedPreferences sharedPreferences = activity.getApplicationContext()
                .getSharedPreferences("PLUGIN_CONFIG_DATA", Context.MODE_PRIVATE);
        sharedPreferences
                .edit()
                .putString("MAIN_ACTIVITY_CLASS_NAME", activityClassName)
                .apply();
    }

    @Override
    public void onDetachedFromActivity() {
        tearDown();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        // onDetachedFromActivity();
    }

    @Override
    public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {
        // onAttachedToActivity(binding);
    }

    private void tearDown() {
        if (activityBinding != null) {
            activityBinding = null;
        }
        if (lifecycle != null && observer != null) {
            lifecycle.removeObserver(observer);
            lifecycle = null;
        }
        if (lifecycle != null) {
            lifecycle = null;
        }
        if (observer != null) {
            observer.mSdkEngine.onSdkEngineDestroy();
        }
        if (application != null && observer != null) {
            application.unregisterActivityLifecycleCallbacks(observer);
            observer = null;
            application = null;
        }
        if (observer != null) {
            observer = null;
        }
    }

    private class LifeCycleObserver
            implements Application.ActivityLifecycleCallbacks, DefaultLifecycleObserver {

        private final Activity thisActivity;
        private final PluginConfiguration mPluginConfiguration;
        public LifecycleOwner owner;
        boolean isOn = false;
        private SDKEngine mSdkEngine;

        public SDKEngine getSdkEngine() {
            return mSdkEngine;
        }

        public LifeCycleObserver(Activity activity, PluginConfiguration pluginConfiguration) {
            this.thisActivity = activity;
            mPluginConfiguration = pluginConfiguration;

            // ArrayList<DeviceLogDB> logDBS=
            // VitaminsRepository.getInstance().getAllMissedCloseLogsList();
            // for (int i = 0; i < logDBS.size(); i++) {
            // Log.v("MissssSm",logDBS.get(i).getId()+"");
            // }
        }

        @Override
        public void onCreate(@NonNull LifecycleOwner owner) {
            this.owner = owner;

            if (isOn)
                return;

            isOn = true;

            mSdkEngine = new SDKEngine(activity, owner, pluginBinding.getBinaryMessenger(), mPluginConfiguration);
            LifeCycleApp.getInstance().updateFlutterObserve(true, "onCreate");
            LifeCycleAppForPush.getInstance().updateAppVisibility(true);

            // reportAppState(true);
        }

        @Override
        public void onStart(@NonNull LifecycleOwner owner) {
            LifeCycleApp.getInstance().updateFlutterObserve(true, "onStart");
            this.owner = owner;
        }

        @Override
        public void onResume(@NonNull LifecycleOwner owner) {
            this.owner = owner;
            if (mSdkEngine != null)
                mSdkEngine.eventOnResume();
            LifeCycleAppForPush.getInstance().updateAppVisibility(true);
        }

        @Override
        public void onPause(@NonNull LifecycleOwner owner) {
            this.owner = owner;
        }

        @Override
        public void onStop(@NonNull LifecycleOwner owner) {
            LifeCycleApp.getInstance().updateFlutterObserve(false, "onStop");
            onActivityStopped(thisActivity);
            if (SharedPrefsModuleHelper.getInstance().getAppMode() == AppMode.VISIBILITY_ONLY)
                mSdkEngine.onSdkEngineDestroy();
            this.owner = owner;
        }

        @Override
        public void onDestroy(@NonNull LifecycleOwner owner) {
            LifeCycleApp.getInstance().updateFlutterObserve(false, "onDestroy");
            onActivityDestroyed(thisActivity);
            if (SharedPrefsModuleHelper.getInstance().getAppMode() != AppMode.VISIBILITY_ONLY)
                mSdkEngine.onSdkEngineDestroy();
            this.owner = owner;
        }

        @Override
        public void onActivityPreDestroyed(@NonNull Activity activity) {
            LifeCycleAppForPush.getInstance().updateAppVisibility(false);
            Log.d("TESTDTD", "onActivityPreDestroyed");
            // TODO test
        }

        @Override
        public void onActivityPostDestroyed(@NonNull Activity activity) {
            LifeCycleAppForPush.getInstance().updateAppVisibility(false);
            Log.d("TESTDTD", "onActivityPostDestroyed");
            // TODO test
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            LifeCycleApp.getInstance().updateFlutterObserve(true, "onActivityCreated");
            LifeCycleAppForPush.getInstance().updateAppVisibility(true);
        }

        @Override
        public void onActivityStarted(Activity activity) {
            LifeCycleApp.getInstance().updateFlutterObserve(true, "onActivityStarted");
            LifeCycleAppForPush.getInstance().updateAppVisibility(true);
        }

        @Override
        public void onActivityResumed(Activity activity) {
            LifeCycleAppForPush.getInstance().updateAppVisibility(true);
        }

        @Override
        public void onActivityPaused(Activity activity) {
            LifeCycleAppForPush.getInstance().updateAppVisibility(false);
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            LifeCycleAppForPush.getInstance().updateAppVisibility(false);
            LifeCycleApp.getInstance().updateFlutterObserve(false, "onActivityDestroyed");
            if (thisActivity == activity && activity.getApplicationContext() != null) {
                ((Application) activity.getApplicationContext())
                        .unregisterActivityLifecycleCallbacks(
                                this); // Use getApplicationContext() to avoid casting failures
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {
            LifeCycleAppForPush.getInstance().updateAppVisibility(false);
            LifeCycleApp.getInstance().updateFlutterObserve(false, "onActivityStopped");
        }
    }

    private void reportAppState(boolean isAppStart) {
        // TODO fix to the right application version

        AnalyticsEvent.Builder test = AnalyticsEventHelper.getJsonStateApp(isAppStart,
                SharedPrefsModuleHelper.getInstance().getVersionAppName());
        new WIONetwork.Analytics().userLog(test);
    }
}

// SEE ALSO -
// #
// https://flutter.dev/docs/development/packages-and-plugins/plugin-api-migration#uiactivity-plugin
// #
// https://github.com/flutter/plugins/blob/master/packages/image_picker/android/src/main/java/io/flutter/plugins/imagepicker/ImagePickerPlugin.java
// # Flutter project 'flutter_plugin_android_lifecycle' for native Android
// lifecycle helper...

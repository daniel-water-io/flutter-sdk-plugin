package io.water.sdk_plugin;

import com.water.water_io_sdk.ble.connection.procedures.abstractions.ProcedureAbstractFirstConnection;

import io.water.sdk_plugin.engine.implementations.procedures.SDKBritaProcedureCmds;
import io.water.sdk_plugin.engine.implementations.procedures.SDKHydrationProcedureCmds;
import io.water.sdk_plugin.engine.implementations.procedures.SDKMelodyProcedureCmds;

public enum ProceduresEnum {
    Brita(SDKBritaProcedureCmds.class),
    Hydration(SDKHydrationProcedureCmds.class),
    Vitamins(SDKMelodyProcedureCmds.class);

    private final Class<? extends ProcedureAbstractFirstConnection> procedure;

    ProceduresEnum(Class<? extends ProcedureAbstractFirstConnection> procedure) {
        this.procedure = procedure;
    }

    public Class<? extends ProcedureAbstractFirstConnection> getProcedure() {
        return procedure;
    }

    public ProcedureAbstractFirstConnection getProcedureInstance() {
        ProcedureAbstractFirstConnection firstConnection = null;
        try {
            firstConnection = procedure.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        return firstConnection;
    }
}

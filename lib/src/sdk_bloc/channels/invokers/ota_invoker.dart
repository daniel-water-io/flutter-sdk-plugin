import 'dart:async';

import './base/channel_invoker.dart';
import '../../channels/sdk_channels_consts.dart';

class OTAInvoker extends ChannelInvoker {
  OTAInvoker() : super(OTA_CHANNEL);

  Future<bool?> resetBootState() async {
    try {
      final isSucsess = await invokeTarget(DEVICE_SET_RESET_BOOT_STATE);
      return isSucsess;
    } catch (ex) {
      return false;
    }
  }

  Future<bool?> startOTA(String version) async {
    try {
      final isSucsess = await invokeTargetWithObj(START_OTA_METHOD, version);
      return isSucsess;
    } catch (ex) {
      return false;
    }
  }

  Future<bool?> startOTAInBoot() async {
    try {
      final isSucsess = await invokeTarget(START_OTA_BOOT_METHOD);
      return isSucsess;
    } catch (ex) {
      return false;
    }
  }
}

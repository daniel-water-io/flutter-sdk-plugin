import 'package:flutter/services.dart';

import './base/channel_invoker.dart';
import '../../channels/sdk_channels_consts.dart';

class PermissionsInvoker extends ChannelInvoker {
  PermissionsInvoker() : super(PERMISSIONS_CHANNEL);

  Future<bool> requestUserNotificationsOrBatteryOptPermissions() async {
    try {
      await invokeTarget(NOTIFICATIONS_OR_BATTERY_OPT_METHOD);
      return true;
    } catch (ex) {
      return false;
    }
  }

  Future<bool> requestChunkedPermissions() async {
    try {
      await invokeTarget(PERMISSIONS_CHUNK_METHOD);
      return true;
    } catch (ex) {
      return false;
    }
  }

  Future<String?> getLocationStatus() async {
    try {
      return await invokeTarget(GET_LOCATION_STATUS);
    } catch (ex) {
      return null;
    }
  }

  /// MISSING_GPS,
  /// MISSING_BLUETOOTH,
  /// SUCCESS
  Future<String?> getAdaptersStatus() async {
    try {
      return await invokeTarget(GET_STATUS_ADAPTERS);
    } catch (ex) {
      return null;
    }
  }

  Future<dynamic> requestAdaptersPermissions(String adaptersRequest) async {
    try {
      await invokeTargetWithObj(PERMISSIONS_ADAPTERS_METHOD, adaptersRequest);
      return true;
    } catch (ex) {
      return false;
    }
  }

  Future<bool> requestBluetoothPermissions() async {
    try {
      return await invokeTarget(PERMISSIONS_BLUETOOTH_METHOD);
    } catch (ex) {
      return false;
    }
  }

  Future<bool> requestDeniedChunkedPermissions() async {
    try {
      await invokeTarget(DENIED_PERMISSIONS_CHUNK_METHOD);
      return true;
    } on PlatformException catch (err) {
      return err.message != 'CHUNK_PERMISSIONS_ARE_DENIED';
    } catch (err) {
      return false;
    }
  }

  Future<String?> checkForMissingPermissions() async {
    try {
      return await invokeTarget(MISSING_PERMISSIONS_CHECK_METHOD);
    } on PlatformException catch (err) {
      return err.message;
    }
  }

  Future<bool> isLocationPermissionDenied() async {
    return await invokeTarget(CHECK_LOCATION_PERMISSION_STATUS);
  }

  Future<bool> isBluetoothPermissionDenied() async {
    return await invokeTarget(CHECK_BLUETOOTH_PERMISSION_STATUS);
  }

  Future<String?> getStatusPermissionOnly() async {
    return await invokeTarget(CHECK_IF_ALL_PERMISSION_ALLOWED_METHOD);
  }

  Future<bool?> requestLocationAlwaysPermissions() async {
    try {
      return await invokeTarget(LOCATION_ALWAYS_METHOD);
    } on PlatformException catch (err) {
      return err.message != 'LOCATION_ALWAYS_METHOD';
    } catch (err) {
      return false;
    }
  }

  Future<dynamic> requestLocationPermissions() async {
    try {
      return invokeTarget(LOCATION_METHOD);
    } on PlatformException catch (err) {
      print('[ERROR] requestLocationPermissions $err');
      return false;
    }
  }

  // DANIEL => this not working,
  Future<bool?> openAppSettings() async {
    try {
      return await invokeTarget(OPEN_LOCATION_PERMISSIONS_SETTINGS);
    } on PlatformException catch (err) {
      return err.message != 'OPEN_LOCATION_PERMISSIONS_SETTINGS';
    } catch (err) {
      return false;
    }
  }

  Future<bool?> wasTheUserAskedBeforeAboutLocationPremission() async {
    return await invokeTarget(USER_WAS_ASKED_ABOUT_LOCATION_PERMISSION_ALREADY);
  }

  Future<bool?> requestNotificationPermission() async {
    try {
      return await invokeTarget(REQUEST_ANDROID_NOTIFICATION_PERMISSION);
    } on PlatformException catch (err) {
      return err.code != 'DENIED PERMISSIONS DETECTED';
    } catch (err) {
      return false;
    }
  }

  Future<bool> isNotificationCanShow() async {
    try {
      return await invokeTarget(IS_NOTIFICATION_CAN_SHOW);
    } on PlatformException catch (err) {
      return false;
    } catch (err) {
      return false;
    }
  }

  Future<bool?> isNotificationEnable() async {
    try {
      return await invokeTarget(CHECK_IS_ANDROID_NOTIFICATION_PERMISSION_GRANTED);
    } on PlatformException catch (err) {
      return false;
    } catch (err) {
      return false;
    }
  }

  Future<bool?> isBluetoothAllowedAndroid12() async {
    try {
      return await invokeTarget(IS_BLUETOOTH_PERMISSION_GRANTED);
    } on PlatformException catch (err) {
      return err.code != 'DENIED PERMISSIONS DETECTED';
    } catch (err) {
      return false;
    }
  }

  Future<String?> openAppPermissionSettings() async {
    try {
      return await invokeTarget(OPEN_PERMISSION_APP_SETTINGS);
    } on PlatformException catch (err) {
      print(err);
      // return err.code != 'DENIED PERMISSIONS DETECTED';
      return Future.value('ff');
    } catch (err) {
      print(err);
      // return false;
    }
    return Future.value('ff');
  }
}

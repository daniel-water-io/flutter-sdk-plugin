import '../../base/flutter_channel.dart';

abstract class ChannelInvoker extends FlutterChannel {
  ChannelInvoker(String channelPath) : super(channelPath);

  Future invokeTarget(String targetMethod, [Map<String, dynamic>? args]) {
    return methodChannel.invokeMethod(targetMethod, args);
  }

  Future invokeTargetWithObj(String targetMethod, Object? args) {
    return methodChannel.invokeMethod(targetMethod, args);
  }
}

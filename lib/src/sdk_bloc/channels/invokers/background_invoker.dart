import '../../../entities/device_snapshot.dart';

import '../sdk_channels_consts.dart';
import './base/channel_invoker.dart';

class BackgroundInvoker extends ChannelInvoker {
  BackgroundInvoker() : super(BACKGROUND_CHANNEL);

  Future<List<DeviceSnapshot>> retrieveEventsFromBackground() async {
    List<dynamic>? eventsList = await invokeTarget(BACKGROUND_SYNC_ON_START_APP);
    List<DeviceSnapshot> result = [];

    if (eventsList == null) {
      return result;
    }

    for (int i = 0; i < eventsList.length; i++) {
      Map<String, dynamic> capStateSnapshot =
          Map<String, dynamic>.from(eventsList[i]);

      DeviceSnapshot snapshot = DeviceSnapshot.fromMap(capStateSnapshot);
      result.add(snapshot);
    }

    return result;
  }

  void clearAllBackgroundEventsDb() {
    invokeTarget(CLEAR_ALL_BACKGROUND_DB);
  }

  void clearAppData() {
    invokeTarget(CLEAR_APP_DATA);
  }
}

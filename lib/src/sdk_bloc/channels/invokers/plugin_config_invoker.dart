import './base/channel_invoker.dart';
import '../../channels/sdk_channels_consts.dart';

enum AppGenes { Hydration, Vitamins }
enum AppMode {
  FOREGROUND_AND_BACKGROUND,
  FOREGROUND_ONLY,
  VISIBILITY_ONLY
}

class PluginConfigInvoker extends ChannelInvoker {
  final AppGenes? appGenes;
  final bool? includePushNotifications;
  final bool? includeLocalReminders;
  final AppMode? appMode;

  PluginConfigInvoker({
    this.appGenes,
    this.includePushNotifications,
    this.includeLocalReminders,
    this.appMode,
  }) : super(PLUGIN_CONFIG_CHANNEL);

  Future<void> configPlufinConfigurations() async {
    return invokeTargetWithObj(CONFIG_PARAMS_METHOD, {
      'appGenes': appGenes.toString().replaceFirst('AppGenes.', ''),
      'includePushNotifications': includePushNotifications,
      'includeLocalReminders': includeLocalReminders,
      'appMode': appMode.toString().replaceFirst('AppMode.', ''),
    });
  }
}

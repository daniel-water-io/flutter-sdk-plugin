import '../../channels/sdk_channels_consts.dart';

import './base/channel_invoker.dart';

class PluginMessengerInvoker extends ChannelInvoker {
  PluginMessengerInvoker() : super(PLUGIN_MESSENGER_CHANNEL);

  void setStringInPluginStorage(String key, String value) {
    invokeTargetWithObj(
        SET_STRING_IN_PLUGIN_STORAGE, {'key': key, 'value': value});
  }

  // when needed - add the getStringInPluginStorage func..
}

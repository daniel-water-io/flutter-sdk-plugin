import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart' show PlatformException;
import 'package:http/http.dart';

import './base/channel_invoker.dart';
import '../../channels/sdk_channels_consts.dart';

class NetworkInvoker extends ChannelInvoker {
  int? userId;
  String? appType;
  int? userScore;
  String? scoreName;

  Completer networkCompleter = Completer();

  Completer missingPluginExceptionCompleter = Completer();

  Future get missingPluginExceptionFuture => missingPluginExceptionCompleter.future;

  Future get userIdFetchFuture => networkCompleter.future;

  Future get networkCompleterFuture => networkCompleter.future;

  static NetworkInvoker? _instance;

  static NetworkInvoker get instance {
    if (_instance == null) {
      _instance = NetworkInvoker._();
    }
    return _instance!;
  }

  NetworkInvoker._() : super(NETWORK_CHANNEL) {
    fixMissingPluginException();
  }

  void init() {
    retrieveAppType();
    retrieveUserId();
  }

  void fixMissingPluginException() async {
    await Future.delayed(Duration(milliseconds: 1500));
    if (!missingPluginExceptionCompleter.isCompleted) {
      missingPluginExceptionCompleter.complete();
    }
  }

  void reportUserLog(Map<String, dynamic> map) async {
    await missingPluginExceptionCompleter.future;
    String mapAsStr = json.encode(map);
    invokeTargetWithObj(REPORT_USER_LOG, mapAsStr);
  }

  void reportUserCrash(Map<String, dynamic> map) async {
    await missingPluginExceptionCompleter.future;
    String mapAsStr = json.encode(map);
    invokeTargetWithObj(REPORT_USER_CRASH, mapAsStr);
  }

  void reportSupportMsg(Map<String, dynamic> map) async {
    await missingPluginExceptionCompleter.future;
    String mapAsStr = json.encode(map);
    await invokeTargetWithObj(SEND_SUPPORT_MGS, mapAsStr);
  }

  void reportScreenChange(String screenName) async {
    await missingPluginExceptionCompleter.future;
    invokeTargetWithObj(REPORT_SCREEN_CHANGE, screenName);
  }

  Future retrieveAppType() async {
    await missingPluginExceptionCompleter.future;
    appType = await invokeTarget(GET_APP_TYPE);
    updateCompleterIfInitComplete();

    return networkCompleter.future;
  }

  Future retrieveUserId() async {
    await missingPluginExceptionCompleter.future;
    userId = await invokeTarget(GET_USER_ID);
    updateCompleterIfInitComplete();

    return networkCompleter.future;
  }

  void retrieveUserScore() async {
    //TODO: Please chnaged is later
    try {
      dynamic score = await invokeTarget(GET_USER_SCORE);
      final map = jsonDecode(score) as Map<String, dynamic>;

      this.userScore = map['score'];
      this.scoreName = map['score_name'];
    } on PlatformException catch (err) {
      print(err);
    }
  }

  void updateCompleterIfInitComplete() {
    if (userId != null && appType != null) {
      if (!networkCompleter.isCompleted) {
        networkCompleter.complete();
      }
    }
  }

  void reportUserHydration(String? eventSource, int? amount, int? timestamp, int? waterLevelStart, int? waterLevelEnd,
      String? drinkType) async {
    await missingPluginExceptionCompleter.future;
    final argsObj = {
      "event_source": eventSource, // man or cap
      "amount": amount,
      "timestamp": timestamp,
      "water_level_start": waterLevelStart,
      "water_level_end": waterLevelEnd,
      "drink_type": drinkType,
    };

    invokeTarget(REPORT_USER_HYDRATION, argsObj);
  }

  Future<String?> retrieveAppPrivacyPolicyUrl(bool isHeb) async {
    String? result = await invokeTargetWithObj(RETRIEVE_APP_PRIVACY_POLICY, isHeb);
    return result;
  }

  Future<dynamic> getBadges() async {
    await userIdFetchFuture;

    try {
      var result = await invokeTarget(GET_BADGES);
      return result;
    } catch (e) {
      return null;
    }
  }

  Future<String?> deleteUserAccount() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(DELETE_ACCOUNT);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getLastOTACapVersion(String? capVersion) async {
    try {
      return await invokeTargetWithObj(GET_LAST_OTA_VERSIONS, capVersion);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getTutorialLinks() async {
    try {
      return await invokeTarget(GET_TUTORIAL_LINKS);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> appToAppAuth(Map<String, String> parameters) async {
    try {
      return await invokeTargetWithObj(APP_TO_APP_AUTH, parameters);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getLastOTACapVersionInBoot(String? bootVersion, String? hardwareVersion) async {
    try {
      return await invokeTargetWithObj(
          GET_LAST_OTA_VERSIONS_IN_BOOT, {'boot_version': bootVersion, 'hardware_version': hardwareVersion});
    } on PlatformException catch (err) {
      print(err);
      return Future.value(null);
    }
  }

  Future<String?> retrieveAppTermsAndConditionsUrl(bool isHeb) async {
    String? result = await invokeTargetWithObj(RETRIEVE_APP_TREMS_AND_CONDITIONS, isHeb);
    return result;
  }

  Future<int?> getUserDailyGoal() async {
    int? result = await invokeTarget(BUILD_HYDRATION_GOAL);
    return result;
  }

  Future<String?> getFeed(int pageSize, {String? idTokenBefore, String? idTokenAfter}) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(GET_FEED, {
        'page_size': pageSize,
        'before': idTokenBefore,
        'after': idTokenAfter,
      });
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> retrieveAppTermsAndConditionsVersion() async {
    String? result = await invokeTarget(RETRIEVE_APP_TREMS_AND_CONDITIONS_VERSION);
    return result;
  }

  Future<int> getLatestAppVersion() async {
    int result = await invokeTarget(GET_LATEST_APP_VERSION);
    return result;
  }

  Future<int> getCalibrationFromFt2Result(String macAddress) async {
    try {
      final int offset = await invokeTargetWithObj(GET_CALIBRATION_FROM_FT2_RESULT, macAddress);
      return offset;
    } on PlatformException catch (err) {
      print(err);
      return -1;
    }
  }

  Future<String?> getNotificationsList(int pageSize, int pageIndex,
      {String? messageType, int? minimumTime, bool? includeSilent = false}) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        GET_NOTIFICATIONS_LIST,
        {
          'index': pageIndex,
          'amount': pageSize,
          'message_code': messageType,
          'minimum_date_time': minimumTime,
          'include_silent': includeSilent
        },
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map<String, dynamic>?> retrieveAppConfiguration() async {
    try {
      String configJson = await invokeTarget(RETRIEVE_APP_CONFIGURATIONS);

      Map<String, dynamic> result = Map<String, dynamic>.from(json.decode(configJson));

      return result;
    } catch (ex) {
      print(ex);
    }
    return null;
  }

  Future<String?> getUserLocation() async {
    try {
      return await invokeTarget(GET_LOCATION);
    } catch (ex) {
      print(ex);
    }
    return null;
  }

  Future<String?> getDrsHostCodeFromServer(String redirectUrl, String clientId) async {
    try {
      return await invokeTargetWithObj(
        GET_DRS_HOST_CODE,
        {'redirectUrl': redirectUrl, 'clientId': clientId},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool?> doesDrsUserSkillEnabled() async {
    try {
      return await invokeTarget(DOES_DRS_USER_SKILL_ENABLED);
    } on PlatformException catch (err) {
      print(err);
      return false;
    }
  }

  Future<bool> setDrsProcessData(Map<String, dynamic> data) async {
    try {
      await invokeTargetWithObj(SET_DRS_PROCESS_DATA, data);
      return true;
    } on PlatformException catch (err) {
      print(err);
      return false;
    }
  }

  void sendLogsFile() {
    invokeTarget(SEND_SDK_LOGS);
  }

  Future<String?> fetchCalibrationTable(String key) async {
    final url = Uri(host: 'https://dashboard.water-io.com', path: '/api/v1/GetBottleCalibrationMapping');
    try {
      Response response = await post(
        url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: json.encode(
          {"bottle_name": "BRITA$key"},
          // {"bottle_name": "BRITA02"},
        ),
      );

      if (response.body.contains('<html>')) {
        return null;
      }

      return response.body;
    } on PlatformException catch (err) {
      print(err);
      return null;
    } catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> fetchRemoteCalibrationTable(String key) async {
    await userIdFetchFuture;

    if (userId == null || userId! <= 0) {
      return null;
    }

    try {
      return await invokeTargetWithObj(FETCH_REMOTE_CALIBRATION_TABLE, key);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> fetchBottlesList() async {
    await userIdFetchFuture;

    if (userId == null || userId! <= 0) {
      return null;
    }

    try {
      return await invokeTarget(FETCH_BOTTLE_LIST);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getEncouragementSentence() async {
    await userIdFetchFuture;
    try {
      return await invokeTarget(GET_ENCOURAGEMENT_SENTENCE);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getCalendarDailyGoal(String date, String timeOffset) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        GET_CALENDAR_DAILY_GOAL,
        {'date': date, 'timeOffset': timeOffset},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getDailyHydration(String date, String timeOffset) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        GET_DAILY_HYDRATION,
        {'date': date, 'timeOffset': timeOffset},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> setNotificationReminder(String type, String interval) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        SET_NOTIFICATION_REMINDER,
        {'type': type, 'interval': interval},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> setTimeRangeHydrationDay(String startDay, String endDay) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        SET_TIME_RANGE_HYDRATION_DAY,
        {'startDay': startDay, 'endDay': endDay},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> setManualDailyGoal(bool isManual, int goal) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        SET_MANUAL_DAILY_GOAL,
        {'isManual': isManual, 'goal': goal},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getLastUserLocation() async {
    await userIdFetchFuture;

    try {
      var result = await invokeTarget(GET_LAST_USER_LOCATION);
      return jsonDecode(result);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool?> reportUserLocation(double lat, double lng) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(REPORT_USER_LOCATION, {'lat': lat, 'lng': lng});
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool?> disconnectFromExternalService(String service) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(DISCONNECT_EXTERNAL_SERVICE, service);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getExternalStatusService() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(STATUS_EXTERNAL_SERVICE);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getDailyHydrationGoal() async {
    await userIdFetchFuture;
    try {
      return await invokeTarget(GET_HYDRATION_DAILY_GOAL);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getDailyHydrationGoalDetails(String type, String timeOffset) async {
    await userIdFetchFuture;
    try {
      return await invokeTargetWithObj(
        GET_HYDRATION_DAILY_GOAL_DETAILS,
        {'type': type, 'timeOffset': timeOffset},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<List?> getAllWidgetData() async {
    try {
      final String data = await invokeTarget(GET_ALL_WIDGET_DATA);
      final List jsonString = jsonDecode(data);
      return jsonString;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getUrlLoginWithGarmin() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_URL_LOGIN_GARMIN);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getUrlLoginWithStrava() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_URL_LOGIN_STRAVA);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getUrlLoginWithOura() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_URL_LOGIN_OURA);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getUrlLoginWithWhoop() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_URL_LOGIN_WHOOP);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> addSportActivity(Map data) async {
    await userIdFetchFuture;

    try {
      var res = await invokeTargetWithObj(
        ADD_SPORT_ACTIVITY,
        data,
      );

      if (res == null) {
        print('++++ null');
      } else {
        print('++++ Success $res');
      }
      return res;
    } on PlatformException catch (err) {
      print(err);
      print('++++ error');

      return null;
    }

    print('++++ other');
  }

  Future<String?> addMultiSportActivity(Map data) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        ADD_MULTI_SPORT_ACTIVITY,
        data,
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool?> isScaleEnable() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_IS_SCALE_ENABLE);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool?> uploadEventToFirebase(Map data) async {
    await userIdFetchFuture;

    String jsonString = json.encode(data);

    try {
      return await invokeTargetWithObj(
        REPORT_FIREBASE_EVENT,
        jsonString,
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> addMultiExtraHydration(Map json) async {
    await userIdFetchFuture;

    try {
      var result = await invokeTargetWithObj(ADD_EXTRA_MULTI_HYDRATION, json);
      return jsonDecode(result);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> deleteExtraHydration(int eventTime, String drinkType) async {
    await userIdFetchFuture;

    try {
      var result = await invokeTargetWithObj(DELETE_EXTRA_HYDRATION, {
        'eventTime': eventTime,
        'drinkType': drinkType,
      });
      return jsonDecode(result);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> updateSportActivity(String id, Map data) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        UPDATE_SPORT_ACTIVITY,
        {'sport_id': id, 'data': data},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> deleteSportActivity(String id) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        DELETE_SPORT_ACTIVITY,
        id,
      );
    } on PlatformException catch (err) {
      if (err.details == 400) {
        return '{}';
      }
      print(err);
      return null;
    }
  }

  Future<String?> getSportActivities(int pageSize, int pageIndex, int startTime) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        GET_SPORT_ACTIVITIES,
        {'page_size': pageSize, 'page_index': pageIndex, 'start_time': startTime},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> deleteAllTodaySportActivity() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(
        DELETE_ALL_TODAY_SPORT_ACTIVITY,
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getAutoCompletePlace(String place, String token) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        GET_AUTO_COMPLETE_PLACE,
        {'place': place, 'token': token},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool?> disconnectWeatherCity() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(DISCONNECT_WEATHER_CITY);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  /// double lng, double lat, String placeId
  /// 1. Use when user select location by phone -> placeId = null
  /// 2. When iser select location by location name -> lat, lng = null
  Future<String?> getCityByGps(double lat, double lng) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        SELECT_WEATHER_CITY,
        {'lat': lat, 'lon': lng},
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getCityWeatherManually(String placeId) async {
    await userIdFetchFuture;
    try {
      return await invokeTargetWithObj(
        VERIFY_CITY,
        placeId,
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> getCurrentEnvironment() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_CURRENT_ENVIRONMENT);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> shareInFacebook(String imagePath) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        SHARE_IN_FACEBOOK,
        imagePath,
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> shareInWhatsApp(Map json) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(
        SHARE_IN_WHATSAPP,
        json,
      );
    } on PlatformException catch (err) {
      print(err);
      return err.message;
    }
  }

  Future<String?> restoreUserDataMigration() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(RESTORE_USER_DATA_MIGRATION);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> restoreUserData() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(
        RESTORE_USER_DATA,
      );
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<String?> storeUserData(Map json) async {
    await userIdFetchFuture;

    try {
      final String isSuccess = await invokeTargetWithObj(SET_STORE_USER_DATA, json);
      return isSuccess;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  void reportAttributes(String trigger) async {
    await missingPluginExceptionCompleter.future;
    invokeTargetWithObj(REPORT_ATTRIBUTES, trigger);
  }

  Future<String?> getUrlLoginWithTerra(String serviceType) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(GET_URL_LOGIN_TERRA, serviceType);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<List<String>?> getHolidays(int year) async {
    await userIdFetchFuture;

    try {
      final String? list = await invokeTargetWithObj(GET_HOLIDAYS, year);
      if (list != null) {
        Map<String, dynamic> json = jsonDecode(list);
        return List.from(json['holidays']);
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map<String, dynamic>?> getLeaderboard(Map<String, dynamic> leaderboardFilters) async {
    await userIdFetchFuture;

    try {
      final result = await invokeTargetWithObj(GET_LEADERBOARD, leaderboardFilters);
      if (result != null) {
        final Map<String, dynamic> json = jsonDecode(result);
        return json;
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  void reportFeedAnalytic(Map<String, dynamic> analyticLog) async {
    await userIdFetchFuture;

    try {
      String mapAsStr = json.encode(analyticLog);
      return await invokeTargetWithObj(REPORT_FEED_ANALYTIC, mapAsStr);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<List?> getUserGroups() async {
    await userIdFetchFuture;

    try {
      Map result = jsonDecode(await invokeTarget(GET_USER_GROUPS));
      if (result['groups'].isNotEmpty) {
        return result['groups'];
      }
    } on PlatformException catch (err) {
      print(err);
    }
    return null;
  }

  Future joinGroup(String groupId) async {
    await userIdFetchFuture;

    try {
      var result = await invokeTargetWithObj(JOIN_GROUP, groupId);
      Map json = jsonDecode(result);
      return json;
    } on PlatformException catch (err) {
      print(err);
      return err.message;
    }
  }

  Future removeUserFromGroup(int userIdToRemove, String groupId) async {
    await userIdFetchFuture;

    try {
      String result = await invokeTargetWithObj(REMOVE_USER_FROM_GROUP, {
        'user_id_to_remove': userIdToRemove,
        'group_id': groupId,
      });
      return result;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getGroupInfo(String groupId) async {
    await userIdFetchFuture;

    try {
      var result = await invokeTargetWithObj(GET_GROUP_INFO, groupId);
      return jsonDecode(result);
    } on PlatformException catch (err) {
      print(err);
    }
    return null;
  }

  Future createNewGroup(Map group) async {
    await userIdFetchFuture;

    try {
      var result = await invokeTargetWithObj(CREATE_NEW_GROUP, group);
      return jsonDecode(result);
    } on PlatformException catch (err) {
      print(err);
      return err.message;
    }
  }

  Future<Map?> editGroup(Map group) async {
    await userIdFetchFuture;

    try {
      var result = await invokeTargetWithObj(EDIT_GROUP, group);
      return jsonDecode(result);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<int?> getTotalGoalDays() async {
    await userIdFetchFuture;

    try {
      return await invokeTarget(GET_TOTAL_GOAL_DAYS);
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  void resendFailedLogs() async {
    await missingPluginExceptionCompleter.future;
    invokeTarget(RESEND_FAILED_LOGS);
  }

  void reportError(Map<String, dynamic> map) async {
    await missingPluginExceptionCompleter.future;
    String mapAsStr = json.encode(map);
    invokeTargetWithObj(REPORT_ERROR, mapAsStr);
  }

  Future<List?> getShortcuts() async {
    await userIdFetchFuture;
    try {
      var result = await invokeTarget(GET_SHORTCUTS);
      if (result == null) {
        return null;
      }
      Map json = jsonDecode(result);
      return json['app_shortcuts'];
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getReferAFriendCoupon() async {
    await userIdFetchFuture;
    try {
      var result = await invokeTarget(GET_REFER_FRIEND_COUPON);
      Map json = jsonDecode(result);
      return json;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool> sendCouponToMail(String email) async {
    await userIdFetchFuture;
    try {
      var result = await invokeTargetWithObj(CREDIT_COUPON, email);
      Map json = jsonDecode(result);
      String coupon = json['coupon_code'];
      return coupon.isNotEmpty;
    } on PlatformException catch (err) {
      print(err);
      return false;
    }
  }

  Future<Map?> getTotalUsages() async {
    await userIdFetchFuture;
    try {
      var result = await invokeTarget(GET_TOTAL_USAGES);
      Map json = jsonDecode(result);
      return json;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> updateShortcuts(Map data) async {
    await userIdFetchFuture;
    try {
      var result = await invokeTargetWithObj(UPDATE_SHORTCUTS, data);
      Map json = jsonDecode(result);
      return json;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getLoginHydration() async {
    await userIdFetchFuture;
    try {
      var result = await invokeTarget(GET_LOGIC_HYDRATION);
      if (result == null) {
        return null;
      }
      Map json = jsonDecode(result);
      return json;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getStreamToken() async {
    await userIdFetchFuture;

    try {
      String? result = await invokeTarget(GET_SOCIAL_TOKEN);
      if (result != null) {
        Map token = jsonDecode(result);
        return token;
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getAppToAppDetails(String clientId) async {
    await userIdFetchFuture;

    try {
      String? result = await invokeTargetWithObj(GET_A2A_DETAILS, clientId);
      if (result != null) {
        return jsonDecode(result)['app_details'];
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getAppVersions() async {
    await userIdFetchFuture;

    try {
      String? result = await invokeTarget(GET_APP_VERSIONS);
      if (result != null) {
        return jsonDecode(result);
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getWeeklyReport(int fromDate, int toDate) async {
    await userIdFetchFuture;

    try {
      String? result = await invokeTargetWithObj(GET_WEEKLY_REPORT, {
        'from_date': fromDate,
        'to_date': toDate,
      });
      if (result != null) {
        Map token = jsonDecode(result);
        return token;
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<Map?> getAppConfiguration() async {
    await userIdFetchFuture;

    try {
      String? result = await invokeTarget(GET_APP_CONFIGURATION);
      if (result != null) {
        Map token = jsonDecode(result);
        return token;
      }
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    }
  }

  Future<bool> subscribeToReminderTopic(Map subscription) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(SUBSCRIBE_TO_REMINDER_TOPIC, subscription);
    } on PlatformException catch (err) {
      print(err);
      return false;
    }
  }

  Future<bool> subscriptionTopic(Map values) async {
    await userIdFetchFuture;

    try {
      return await invokeTargetWithObj(SUBSCRIPTION_TOPIC, values);
    } on PlatformException catch (err) {
      print(err);
      return false;
    }
  }

  Future<String?> addPost(Map values) async {
    await userIdFetchFuture;

    try {
      final result = await invokeTargetWithObj(ADD_POST, values);
      return (jsonDecode(result)['post_id']).toString();
    } on PlatformException catch (err) {
      print(err);
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<String?> addComment(Map values) async {
    await userIdFetchFuture;

    try {
      final result = await invokeTargetWithObj(ADD_COMMENT, values);
      return (jsonDecode(result)['comment_id']).toString();
    } on PlatformException catch (err) {
      print(err);
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<String?> registrationWarranty(Map warranty) async {
    try {
      final result = await invokeTargetWithObj(REGISTRATION_WARRANTY, warranty);
      return null;
    } on PlatformException catch (err) {
      print(err);
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<void> answerFillingQuestion(Map answer) async {
    try {
      final result = await invokeTargetWithObj(FILLING_ANSWER, answer);
    } on PlatformException catch (err) {
      print(err);
    } catch (e) {}
  }
}

NetworkInvoker networkInvoker = NetworkInvoker.instance; //_();

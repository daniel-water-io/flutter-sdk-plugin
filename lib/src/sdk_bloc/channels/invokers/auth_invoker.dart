import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:device_info/device_info.dart';

import './base/channel_invoker.dart';
import '../sdk_channels_consts.dart';
import './network_invoker.dart';

class AuthInvoker extends ChannelInvoker {
  final Function onUserAuthSuccess;

  AuthInvoker(this.onUserAuthSuccess) : super(AUTH_CHANNEL);

  void commitSilentLogin() async {
    String? deviceId, email, password;
    deviceId = await getDeviceUniqueIdentifier();
    int randNum = DateTime.now().millisecondsSinceEpoch % 1000000;

    email = '$randNum$deviceId@demo-email.com';
    password = deviceId;

    bool registeredSuccessfully = await registerNewUser(email, password);
    if (!registeredSuccessfully) {
      loginExistingUser(email, password);
    }
  }

  Future<bool> registerNewUser(String email, String? password) async {
    try {
      dynamic registerResult = await invokeTarget(AUTH_REGISTER_METHOD, {'email': email, 'password': password});

      reportAuthToUserLog('New User Connect', true);
      Future userIdFetch = networkInvoker.retrieveUserId();

      updateUserAuthSuccess(userIdFetch);

      return true;
    } catch (err) {
      print('error registering new user => $err');
    }
    return false;
  }

  Future<String> loginExistingUser(String email, String? password) async {
    try {
      dynamic loginResult = await invokeTarget(AUTH_LOGIN_METHOD, {'email': email, 'password': password});

      reportAuthToUserLog('Login successfully', true);
      Future userIdFetch = networkInvoker.retrieveUserId();

      updateUserAuthSuccess(userIdFetch);
      return 'SUCCESS';
    } on PlatformException catch (e) {
      if (e.message != null) {
        try {
          Map json = jsonDecode(e.message!);
          return json['error_type'];
        } catch (r) {
          return 'UKNOWN_ERROR';
        }
      } else {
        return 'UKNOWN_ERROR';
      }
    } catch (err) {
      return 'UKNOWN_ERROR';
      // print('error login an existing user => $err');
    }
  }

  Future<bool> logout() async {
    try {
      bool loginResult = await invokeTarget(AUTH_LOG_OUT);
      //reportAuthToUserLog('Logout successfully', false);
      networkInvoker.userId = null;
      return true;
    } catch (err) {
      // print('error login an existing user => $err');
    }
    return false;
  }

  Future<bool> forgetPassword(String email) async {
    try {
      bool loginResult = await invokeTargetWithObj(AUTH_RESET_PASSWORD, email);
      reportAuthToUserLog('Logout successfully', false);
      return true;
    } catch (err) {
      // print('error login an existing user => $err');
    }
    return false;
  }

  Future<String?> loginWithSocial(Map<String, dynamic> json) async {
    try {
      String? loginResult = await invokeTargetWithObj(AUTH_SOCIAL_LOGIN, json);
      if (loginResult != null) {
        Map<String, dynamic> loginResultMap = jsonDecode(loginResult);
        final bool isNewUser = loginResultMap['is_new_user'] ?? false;
        if (isNewUser == true) {
          reportAuthToUserLog('New User Connect', true);
        } else {
          reportAuthToUserLog('Login successfully', true);
        }
      }
      Future userIdFetch = networkInvoker.retrieveUserId();

      updateUserAuthSuccess(userIdFetch);

      return loginResult;
    } catch (err) {
      print('error login an existing user => $err');
    }
    return null;
  }

  reportAuthToUserLog(String eventText, bool isLoggedIn) {
    networkInvoker.reportUserLog({
      'event_data': 0,
      'event_time': DateTime.now().millisecondsSinceEpoch,
      'event_type': isLoggedIn ? 'login_successfully' : 'logout_successfully',
      'event_text': eventText,
    });
  }

  Future<String?> updateUserProfileInServerByJson({required Map<String, dynamic> argObj}) async {
    try {
      List<String> keys = argObj.keys.toList();

      for (var key in keys) {
        final value = argObj[key];

        if (value == null) {
          argObj.remove(key);
        }
      }

      final String? response = await invokeTarget(AUTH_PROFILE_UPDATE, argObj);

      return response;
    } catch (err) {
      // print('error login an existing user => $err');
    }
    return null;
  }

  Future<String?> updateUserProfileInServer({
    String? email,
    String? nickName,
    String? firstName,
    int? age,
    required double weight,
    required double height,
    String? gender,
    String? phone,
    String? extra_data_1,
  }) async {
    assert(gender != 'M' || gender != 'F' || gender != 'O', 'The gender cannot be this chart');
    try {
      final Map<String, dynamic> argObj = {
        'email': email,
        'nick_name': nickName,
        'first_name': firstName,
        'age': age,
        'weight': weight.toInt(),
        'height': height.toInt(),
        'gender': gender, // gender == 'Male' ? 'M' : 'F',
        'mobile_phone': phone,
        'extra_data_1': extra_data_1,
      };

      List<String> keys = argObj.keys.toList();

      for (var key in keys) {
        final value = argObj[key];

        if (value == null) {
          argObj.remove(key);
        }
      }

      final String? response = await invokeTarget(AUTH_PROFILE_UPDATE, argObj);

      return response;
    } catch (err) {
      // print('error login an existing user => $err');
    }
    return null;
  }

  Future<String?> getDeviceUniqueIdentifier() async {
    String deviceName;
    String deviceVersion;
    String? identifier;

    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();

    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.toString();
        identifier = build.androidId; //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor; //UUID for iOS
      }
    } on PlatformException {
      print('Failed to get platform version');
    }

    return identifier;
    // return [deviceName, deviceVersion, identifier];
  }

  void updateUserAuthSuccess(Future userIdFetch) async {
    await userIdFetch;

    onUserAuthSuccess();
  }
}

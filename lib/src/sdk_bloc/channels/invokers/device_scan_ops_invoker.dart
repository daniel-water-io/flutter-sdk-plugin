import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'base/channel_invoker.dart';
import '../sdk_channels_consts.dart';
import '../observers/wizard_new_connections_observer.dart';
import '../../../entities/reminder.dart';
import '../../../entities/blink_type.dart';

enum ProcedureType { Hydration, Brita, Vitamins }

class DeviceScanOperationsInvoker extends ChannelInvoker {
  WizardNewConnectionsObserver? _wizardNewConnectionsObserver;
  final Function? onDeviceDataUpdate;

  DeviceScanOperationsInvoker({this.onDeviceDataUpdate}) : super(DEVICE_SCAN_INVOKE_CHANNEL) {
    _loadFromStorageCapVersion();
    retrieveDeviceData();
  }

  String? macAddress;
  String? capVersion;
  int? lastSyncDeviceTimestamp;

  Future<String?> startScan() async {
    try {
      await invokeTarget(DEVICE_SCAN_INVOKE_START_SCAN);
      return null; // null is returned when all good..
    } on FlutterError catch (ex) {
      return ex.message;
    } catch (e) {
      return e.toString();
    }
  }

  setPauseUseCap(bool isPause) {
    try {
      invokeTargetWithObj(DEVICE_SET_PAUSE_USE, isPause);
      return null; // null is returned when all good..
    } on FlutterError catch (ex) {
      return ex.message;
    } catch (e) {
      return e.toString();
    }
  }

  forgetCap() {
    invokeTarget(DEVICE_SCAN_CALLBACK_FORGET_CAP);
    _clearCapVersion();
  }

  registerStartScanWhenEnable() {
    invokeTarget(REGISTER_DEVICE_SCAN_WHEN_ENABLE);
  }

  stopScan() {
    invokeTarget(DEVICE_SCAN_INVOKE_STOP_SCAN);
  }

  Future<String?> startWizardScan(ProcedureType procedureType, Function(String?, bool) onNewDeviceConnection) async {
    try {
      await invokeTargetWithObj(
        DEVICE_SCAN_INVOKE_WIZARD_START_SCAN,
        procedureType.toString().replaceAll('ProcedureType.', ''),
      );

      _wizardNewConnectionsObserver = WizardNewConnectionsObserver(onNewDeviceConnection);

      retrieveDeviceData();
      return null;
    } on FlutterError catch (ex) {
      return ex.message;
    } catch (e) {
      return e.toString();
    }
  }

  Future<void> wizardScanYesMyCap() async {
    await retrieveDeviceData();

    invokeTarget(DEVICE_SCAN_WIZARD_YES_MY_CAP);
    _wizardNewConnectionsObserver = null;
  }

  void wizardScanNotMyCap(bool isAddToBlackList) {
    invokeTargetWithObj(DEVICE_SCAN_WIZARD_NOT_MY_CAP, isAddToBlackList);
  }

  Future<bool> setCalibration(int value) async {
    return await invokeTargetWithObj(DEVICE_SET_CALIBRATION, value);
  }

  Future<void> retrieveDeviceData() async {
    dynamic rawDeviceData = await invokeTarget(DEVICE_GET_DEVICE_DATA);

    Map<String, dynamic> deviceData = Map<String, dynamic>.from(rawDeviceData);

    macAddress = deviceData['mac_address'];
    capVersion = deviceData['cap_version'];
    lastSyncDeviceTimestamp = await retrieveLastDeviceSyncTimestamp();

    _saveCapVersion(deviceData);
    if (onDeviceDataUpdate != null) {
      onDeviceDataUpdate!();
    }

    return null;
  }

  void _saveCapVersion(Map jsonData) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    final String data = json.encode(jsonData);
    pref.setString('last.cap.version', data);
  }

  void _loadFromStorageCapVersion() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String? capVersionStorage = pref.getString('last.cap.version');
    if (capVersionStorage == null) {
      return;
    }

    Map<String, dynamic> deviceData = Map<String, dynamic>.from(json.decode(capVersionStorage));
    macAddress = deviceData['mac_address'];
    capVersion = deviceData['cap_version'];
    lastSyncDeviceTimestamp = await retrieveLastDeviceSyncTimestamp();
  }

  void _clearCapVersion() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('last.cap.version');
  }

  void commitBlinkSequence(BlinkType type) {
    invokeTargetWithObj(DEVICE_COMMIT_BLINK_SEQUENCE, type.blinkIndex);
  }

  void setRemindersOnDevice(List<Reminder> remindersList) {
    List<Map<String, int>>? res = remindersList.map((reminder) => reminder.toJson()).toList();

    if (res.length > 3) {
      throw Exception("Max 3 reminder elements are permitted at this time");
    }

    invokeTargetWithObj(DEVICE_SET_REMINDERS, res);
  }

  Future<bool> runSingleMeasurement() async {
    try {
      bool isSuccess = await invokeTarget(RUN_SINGLE_MEASUREMENT);
      return isSuccess;
    } on FlutterError {
      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> checkCapSync(int baseGoal, int extraDailyGoal, int manGoal, int hydrationMeasurement) async {
    try {
      return await invokeTargetWithObj(DEVICE_CHECK_CAP_SYNC, {
        'base_goal': baseGoal,
        'extra_goal': extraDailyGoal,
        'man_goal': manGoal,
        'hydration_measurement': hydrationMeasurement
      });
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> restorePairedToCap(String macAddress) async {
    try {
      return await invokeTargetWithObj(RESTORE_PAIRED_TO_CAP, macAddress);
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  void setResetBootState() {
    invokeTarget(DEVICE_SET_RESET_BOOT_STATE);
  }

  void setLedColor(String hexColor, int duration) {
    invokeTargetWithObj(
      DEVICE_SET_LED_COLOR,
      {'color': hexColor, 'duration': duration},
    );
  }

  void simulateLedPattern(String hexColor, int pattern) {
    invokeTargetWithObj(
      SIMULATE_REMINDER_PATTERN,
      {'color': hexColor, 'pattern': pattern},
    );
  }

  Future<bool> simulateVibration(int duration) async {
    try {
      return await invokeTargetWithObj(SIMULATION_VIBRATION, duration);
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  void setSilentMode(bool isOn) {
    invokeTargetWithObj(DEVICE_SET_SILENT_MODE, isOn);
  }

  void setVibration(bool isOn) {
    invokeTargetWithObj(DEVICE_SET_VIBRATION, isOn);
  }

  void setHydrationStatusIndicator(bool isOn) {
    invokeTargetWithObj(ENABLE_HYDRATION_STATUS_INDICATOR, isOn);
  }

  void setCapDisconnect() {
    invokeTarget(DEVICE_SET_DISCONNECT);
  }

  void setCapReminderColor(String hexColor) {
    invokeTargetWithObj(DEVICE_SET_REMINDER_COLOR, hexColor);
  }

  void setCapDailyGoal(int goal) {
    invokeTargetWithObj(DEVICE_SET_DAILY_GOAL, goal);
  }

  void setCapManuallyHydration(double amount) {
    invokeTargetWithObj(DEVICE_SET_MANUALLY_HYDRATION, amount);
  }

  Future<String?> getMultiConfigurations() async {
    try {
      return await invokeTarget(DEVICE_GET_MULTI_CONFIGURATIONS);
    } on FlutterError {
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<bool?> setMultiConfiguration(Map configuration) async {
    try {
      return await invokeTargetWithObj(DEVICE_SET_MULTI_CONFIGURATIONS, configuration);
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> setHydrationLevel(int amount) async {
    try {
      return await invokeTargetWithObj(DEVICE_SET_HYDRATION_LEVEL, amount);
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> setManuallyHydration(int amount) async {
    try {
      return await invokeTargetWithObj(DEVICE_SET_MANUALLY_HYDRATION, amount);
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> setExtraDailyGoal(int goal) async {
    try {
      return await invokeTargetWithObj(DEVICE_SET_EXTRA_DAILY_GOAL, goal);
    } on FlutterError {
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<String?> getCapHydration() async {
    try {
      return await invokeTarget(DEVICE_GET_HYDRATION);
    } on FlutterError catch (ex) {
      print(ex.message);
      return null;
    } catch (e) {
      print('data not sent');
      return null;
    }
  }

  // Future<String?> getLastCapConfigurations() async {
  //   try {
  //     return await invokeTarget(GET_LAST_CAP_CONFIGURATIONS);
  //   } on FlutterError catch (ex) {
  //     print(ex.message);
  //     return null;
  //   } catch (e) {
  //     print('data not sent');
  //     return null;
  //   }
  // }

  Future<String?> getLastDbHydrations(int index) async {
    try {
      return await invokeTargetWithObj(GET_LAST_DB_HYDRATION, index);
    } on FlutterError catch (ex) {
      print(ex.message);
      return null;
    } catch (e) {
      print('data not sent');
      return null;
    }
  }

  Future<int?> getExtraDailyGoal() async {
    try {
      return await invokeTarget(DEVICE_GET_EXTRA_DAILY_GOAL);
    } on FlutterError catch (ex) {
      print(ex.message);
      return null;
    } catch (e) {
      print('data not sent');
      return null;
    }
  }

  Future<int?> getBaseGoal() async {
    try {
      return await invokeTarget(DEVICE_GET_BASE_GOAL);
    } on FlutterError catch (ex) {
      print(ex.message);
      return null;
    } catch (e) {
      print('data not sent');
      return null;
    }
  }

  void updateDailyUsageTime(int fromHour, int fromMinutes, int toHour, int toMinutes, bool isConfiguration) {
    Map<String, dynamic> dailyUsageTime = Map<String, dynamic>();
    dailyUsageTime['FROM_HOUR'] = fromHour;
    dailyUsageTime['TO_HOUR'] = toHour;
    dailyUsageTime['FROM_MINUTES'] = fromMinutes;
    dailyUsageTime['TO_MINUTES'] = toMinutes;
    dailyUsageTime['IS_CONFIGURATION'] = isConfiguration;

    invokeTargetWithObj(DEVICE_UPDATE_DAILY_USAGE, dailyUsageTime);
  }

  void updateDeviceRssiConfig(bool activate, int rssi) {
    if (rssi < -100 || rssi > -40) {
      throw 'error in rssi value - not in range of [-100,-40]';
    }

    invokeTargetWithObj(
      DEVICE_UPDATE_RSSI_CONFIG,
      {'activate': activate, 'rssi': rssi},
    );
  }

  Future<int?> retrieveLastDeviceSyncTimestamp() async {
    int? result = await invokeTarget(DEVICE_LAST_SYNC_TIMESTAMP);
    return result;
  }
}

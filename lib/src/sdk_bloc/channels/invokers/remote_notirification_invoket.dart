import 'package:sdk_plugin/src/sdk_bloc/channels/invokers/base/channel_invoker.dart';
import 'package:sdk_plugin/src/sdk_bloc/channels/sdk_channels_consts.dart';

class RemoteNotificationInvoker extends ChannelInvoker {
  RemoteNotificationInvoker() : super(PUSH_NOTIFICATIONS_CHANNEL);

  Future<bool?> isNotificationTokenUpdated() async {
    try {
      final isSuccess = await invokeTarget(IS_NOTIFICATION_TOKEN_UPDATED);
      return isSuccess;
    } catch (ex) {
      return null;
    }
  }

  // when needed - add the getStringInPluginStorage func..
}

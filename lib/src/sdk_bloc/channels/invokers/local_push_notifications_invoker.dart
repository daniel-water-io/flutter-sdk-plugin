import './base/channel_invoker.dart';
import '../sdk_channels_consts.dart';
import '../../../entities/reminder.dart';

class LocalPushNotificationsInvoker extends ChannelInvoker {
  LocalPushNotificationsInvoker() : super(LOCAL_PUSH_NOTIFICATIONS_CHANNEL);

  void setLocalPushsReminders(List<Reminder> remindersList) {
    List<Map<String, int>>? res =
        remindersList.map((reminder) => reminder.toJson()).toList();

    invokeTargetWithObj(SET_LOCAL_PUSHS_REMINDERS, res);
  }

  void setLocalPushsRandomMsgs(String title, List<String> msgs) {
    invokeTargetWithObj(
        SET_LOCAL_PUSHS_RAND_MSGS, {'title': title, 'msgs': msgs});
  }

  void cancelLocalPushOnce(Reminder reminder) {
    invokeTargetWithObj(CANCEL_ONCE_LOCAL_PUSH_REMINDERS, reminder.toJson());
  }
}

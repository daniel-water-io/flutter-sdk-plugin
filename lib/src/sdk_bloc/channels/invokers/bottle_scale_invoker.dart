import './base/channel_invoker.dart';
import '../sdk_channels_consts.dart';

class BottleScaleInvoker extends ChannelInvoker {
  BottleScaleInvoker() : super(BOTTLE_SCALE_CHANNEL);

  Future<bool?> startScan() async {
    try {
      final isSuccess = await invokeTarget(START_SCAN_SCALE_METHOD);
      return isSuccess;
    } catch (ex) {
      return false;
    }
  }

  Future<bool?> stopScan() async {
    try {
      final isSuccess = await invokeTarget(STOP_SCAN_SCALE_METHOD);
      return isSuccess;
    } catch (ex) {
      return false;
    }
  }
}

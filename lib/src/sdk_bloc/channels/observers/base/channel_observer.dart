import 'package:flutter/services.dart';
import '../../base/flutter_channel.dart';

abstract class ChannelObserver extends FlutterChannel {
  late String targetMethod;

  ChannelObserver(String channelPath, String targetMethod)
      : super(channelPath) {
    this.targetMethod = targetMethod;

    methodChannel.setMethodCallHandler(methodHandlerCallHandler);
  }

  invokeTarget(Map<String, dynamic> args) {
    methodChannel.invokeMethod(targetMethod, args);
  }

  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall);
}

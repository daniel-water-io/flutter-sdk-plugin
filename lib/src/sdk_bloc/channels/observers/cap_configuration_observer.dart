import 'dart:convert';

import 'package:flutter/services.dart';

import '../sdk_channels_consts.dart';
import 'base/channel_observer.dart';

class CapConfigurationsObserver extends ChannelObserver {
  Function(Map<String, dynamic>) onConfigurationChanged;

  CapConfigurationsObserver(this.onConfigurationChanged)
      : super(CAP_CONFIGURATIONS_CHANNEL, CAP_CONFIGURATIONS_CHANGED_METHOD);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    if (methodCall.arguments == null) {
      return null;
    }
    final Map<String, dynamic> map = json.decode(methodCall.arguments);

    onConfigurationChanged(map);
    return null;
  }
}

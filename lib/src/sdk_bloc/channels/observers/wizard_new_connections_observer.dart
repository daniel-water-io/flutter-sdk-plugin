import 'package:flutter/services.dart';
import 'base/channel_observer.dart';
import '../sdk_channels_consts.dart';

class WizardNewConnectionsObserver extends ChannelObserver {
  Function(String?, bool) onNewDeviceConnectionCallback;

  WizardNewConnectionsObserver(this.onNewDeviceConnectionCallback)
      : super(DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION_CHANNEL, DEVICE_SCAN_WIZARD_ON_POTENTIAL_CAP_CONNECTION);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    Map? json = methodCall.arguments;
    if (json == null) {
      return;
    }

    final String macAddress = json['macAddress'];
    final bool isBootMode = json['isBootMode'];
    print('FLUTTER_ASASAS $macAddress $isBootMode');
    onNewDeviceConnectionCallback(macAddress, isBootMode);
    return null;
  }
}

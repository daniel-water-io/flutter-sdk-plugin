import '../sdk_channels_consts.dart';
import 'base/channel_observer.dart';
import 'package:flutter/services.dart';

class PushNotificationObserver extends ChannelObserver {
  Function(Map?) onUserClickOnUrlCallback;
  // This callback for ios
  Function(bool) isFinishedRegistered;

  PushNotificationObserver(this.onUserClickOnUrlCallback, this.isFinishedRegistered)
      : super(PUSH_NOTIFICATIONS_CHANNEL, NETWORK_USER_CLICK_URL);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    if (methodCall.method == NOTIFICATION_TOKEN_UPDATED) {
      isFinishedRegistered(methodCall.arguments);
      return;
    }
    final Map? data = methodCall.arguments;
    onUserClickOnUrlCallback(data);
    return null;
  }
}

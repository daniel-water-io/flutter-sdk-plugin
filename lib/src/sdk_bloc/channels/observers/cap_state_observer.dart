import 'dart:convert';

import 'package:flutter/services.dart';

import 'base/channel_observer.dart';
import '../sdk_channels_consts.dart';

import '../../../entities/device_snapshot.dart';

class CapStateObserver extends ChannelObserver {
  Function(List<DeviceSnapshot>) onStateChangedCallback;
  Function(int?) onBatteryLevelChangedCallback;
  Function(String?) onCapVersionDidUpdated;

  CapStateObserver(this.onStateChangedCallback, this.onBatteryLevelChangedCallback, this.onCapVersionDidUpdated)
      : super(CAP_STATE_CHANNEL_PATH, CAP_STATE_CHANNEL_TARGET_METHOD);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    if (methodCall.method == CAP_STATE_BATTREY_STATE_METHOD) {
      onBatteryLevelChangedCallback(methodCall.arguments);
      return null;
    }

    if (methodCall.method == CAP_STATE_VERSION_METHOD) {
      onCapVersionDidUpdated(methodCall.arguments);
      return null;
    }

    final String logsArrayString = methodCall.arguments;
    final List logs = json.decode(logsArrayString);
    final List<DeviceSnapshot> snapshots = logs.map((e) => DeviceSnapshot.fromMap(e)).toList();

    onStateChangedCallback(snapshots);

    return null;
  }
}

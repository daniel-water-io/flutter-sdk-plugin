import 'package:flutter/services.dart';

import 'base/channel_observer.dart';
import '../sdk_channels_consts.dart';

class AuthObserver extends ChannelObserver {
  final Function(bool) onUserAuthenticated;

  AuthObserver({required this.onUserAuthenticated})
      : super(AUTH_CHANNEL, AUTH_USER_UNAUTHENTICATED_CHANGED);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    final bool isUserAuthenticated = methodCall.arguments;
    onUserAuthenticated(isUserAuthenticated);
    return null;
  }
}

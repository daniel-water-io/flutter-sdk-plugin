import '../sdk_channels_consts.dart';
import './base/channel_observer.dart';
import '../../../entities/scale_bottle_state.dart';
import '../../../entities/scale_bottle_value.dart';

import 'package:flutter/services.dart';

class BottleScaleObserver extends ChannelObserver {
  Function(ScaleBottleState state) onStateChangedCallback;
  Function(ScaleBottleValue entry) onChangedValueCallback;

  BottleScaleObserver(this.onStateChangedCallback, this.onChangedValueCallback)
      : super(BOTTLE_SCALE_CHANNEL, CHANGED_SCALE_STATE);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    print('RECEIVED - BottleScaleObserver state => ${methodCall.toString()}');

    if (methodCall.method == CHANGED_SCALE_STATE) {
      onStateChangedCallback(_mapValue(methodCall.arguments));
      return null;
    }

    if (methodCall.method == CHANGED_SCALE_VALUE) {
      onChangedValueCallback(ScaleBottleValue.fromMap(methodCall.arguments));
      return null;
    }
  }

  ScaleBottleState _mapValue(String state) {
    switch (state) {
      case 'unavailable':
        return ScaleBottleState.unavailable;
      case 'available':
        return ScaleBottleState.available;
      case 'stopScan':
        return ScaleBottleState.stopScan;
      case 'scaning':
        return ScaleBottleState.scaning;
      case 'willConnect':
        return ScaleBottleState.willConnect;
      case 'didConnect':
        return ScaleBottleState.didConnect;
      case 'connectFail':
        return ScaleBottleState.connectFail;
      case 'didDiscoverCharacteristics':
        return ScaleBottleState.didDiscoverCharacteristics;
      case 'didDisconnect':
        return ScaleBottleState.didDisconnect;
      case 'didValidationPass':
        return ScaleBottleState.didValidationPass;
      case 'failedValidation':
        return ScaleBottleState.failedValidation;
      case 'unauthorized':
        return ScaleBottleState.unauthorized;
    }

    return ScaleBottleState.unavailable;
  }
}

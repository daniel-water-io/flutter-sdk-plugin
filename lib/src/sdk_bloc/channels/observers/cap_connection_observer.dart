import 'package:flutter/services.dart';

import './base/channel_observer.dart';
import '../sdk_channels_consts.dart';

import '../../../entities/connection_snapshot.dart';

class CapConnectionObserver extends ChannelObserver {
  Function(ConnectionSnapshot) onStateChangedCallback;

  CapConnectionObserver(this.onStateChangedCallback)
      : super(CAP_CONNECTION_STATUS_CHANNEL, CAP_CONNECTION_STATUS_METHOD);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    Map<String, dynamic> connectionStateSnapshot =
        Map<String, dynamic>.from(methodCall.arguments);

    ConnectionSnapshot connectionState =
        ConnectionSnapshot.fromMap(connectionStateSnapshot);
    onStateChangedCallback(connectionState);
    print('RECEIVED - Connection state => ${connectionState.toString()}');
  }
}

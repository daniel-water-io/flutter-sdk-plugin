import '../sdk_channels_consts.dart';
import 'base/channel_observer.dart';
import 'package:flutter/services.dart';

class NetworkObserver extends ChannelObserver {
  late Function(String?) onUserClickOnUrlCallback;

  NetworkObserver() : super(NETWORK_CHANNEL, NETWORK_USER_CLICK_URL);

  @override
  Future<dynamic> methodHandlerCallHandler(MethodCall methodCall) async {
    final String? url = methodCall.arguments;
    onUserClickOnUrlCallback(url);
    return null;
  }
}

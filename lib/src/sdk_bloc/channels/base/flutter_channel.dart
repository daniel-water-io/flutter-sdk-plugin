import 'package:flutter/services.dart';

abstract class FlutterChannel {
  String? channelPath;
  late MethodChannel methodChannel;

  FlutterChannel(String channelPath) {
    this.channelPath = channelPath;
    this.methodChannel = MethodChannel(channelPath);
  }
}

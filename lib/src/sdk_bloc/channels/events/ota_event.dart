import 'dart:async';
import 'package:flutter/services.dart';

import '../sdk_channels_consts.dart';
import '../../../entities/ota_state_event.dart';

class OTAEvent {
  static const _eventChannel = const EventChannel(OTA_STATE_EVENT);

  Stream<OTAStateEvent> state = _eventChannel
      .receiveBroadcastStream()
      .asBroadcastStream()
      .map((event) => OTAStateEvent.fromJSON(event));
}

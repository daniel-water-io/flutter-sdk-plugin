import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:rxdart/rxdart.dart';
import 'package:sdk_plugin/demo_hydration.dart';
import 'package:sdk_plugin/src/sdk_bloc/channels/observers/cap_configuration_observer.dart';

import 'channels/events/ota_event.dart';

import 'channels/invokers/ota_invoker.dart';

import 'channels/invokers/device_scan_ops_invoker.dart';
import 'channels/invokers/remote_notirification_invoket.dart';
import 'channels/observers/auth_observer.dart';
import 'channels/observers/cap_connection_observer.dart';
import 'channels/observers/cap_state_observer.dart';
import 'channels/observers/newtwork_observer.dart';

import 'channels/invokers/auth_invoker.dart';
import 'channels/invokers/network_invoker.dart';
import 'channels/invokers/permissions_invoker.dart';
import 'channels/invokers/plugin_config_invoker.dart';
import 'channels/invokers/local_push_notifications_invoker.dart';
import 'channels/invokers/background_invoker.dart';
import 'channels/invokers/plugin_messenger_invoker.dart';

import '../entities/reminder.dart';
import '../entities/device_snapshot.dart';
import '../entities/connection_snapshot.dart';
import '../entities/ota_state_event.dart';
import '../entities/blink_type.dart';
import 'channels/observers/push_notification_observer.dart';

export 'channels/invokers/plugin_config_invoker.dart';
export 'channels/invokers/device_scan_ops_invoker.dart';
export 'channels/invokers/network_invoker.dart';

import 'channels/invokers/bottle_scale_invoker.dart';
import 'channels/observers/bottle_scale_observer.dart';
import '../entities/scale_bottle_state.dart';
import '../entities/scale_bottle_value.dart';

class SdkBloc {
  final bool applySilentLogin;

  late DeviceScanOperationsInvoker _deviceScanOpsInvoker;
  late AuthInvoker _authInvoker;
  PermissionsInvoker? _permissionsInvoker;
  late LocalPushNotificationsInvoker _localPushNotificationsInvoker;
  late BackgroundInvoker _backgroundInvoker;
  late PluginMessengerInvoker _pluginMessengerInvoker;

  BehaviorSubject<List<DeviceSnapshot>> _deviceSnapshotSubject = BehaviorSubject<List<DeviceSnapshot>>();

  BehaviorSubject<Map?> _pushNotificationSnapshotSubject = BehaviorSubject<Map?>();

  BehaviorSubject<bool?> _pushNotificationTokenRegisteredSnapshotSubject = BehaviorSubject<bool?>();

  BehaviorSubject<int?> _deviceBatteryLevelSubject = BehaviorSubject<int?>.seeded(null);

  BehaviorSubject<ConnectionSnapshot> _connectionSnapshotSubject = BehaviorSubject<ConnectionSnapshot>();

  BehaviorSubject<String?> _capVersionSubject = BehaviorSubject<String?>();

  BehaviorSubject<bool?> _authDidChangedSubject = BehaviorSubject<bool?>.seeded(null);

  BehaviorSubject<Map?> _capConfigurationChangedSubject = BehaviorSubject<Map?>.seeded(null);

  BehaviorSubject<int> _calendarChangeSubject = BehaviorSubject<int>.seeded(0);

  Stream<int> get calendarChangeStream => _calendarChangeSubject.stream;

  NetworkObserver? networkObserver;

  // Getters to Sinks
  Function(List<DeviceSnapshot>) get _updateCapState => _deviceSnapshotSubject.sink.add;

  Function(ConnectionSnapshot) get _updateConnectionState => _connectionSnapshotSubject.sink.add;

  Stream<Map?> get capConfigurationChangedStream => _capConfigurationChangedSubject.stream;

  Stream<bool?> get pushNotificationTokenRegisteredStream => _pushNotificationTokenRegisteredSnapshotSubject.stream;

  PublishSubject<String?> _userClickOnUrlSubject = PublishSubject<String?>();

  Stream<String?> get userClickOnUrlStream => _userClickOnUrlSubject.stream;

  Stream<Map?> get pushNotificationStream => _pushNotificationSnapshotSubject.stream.asBroadcastStream();

  Stream<String?> get getcapVersionStream => _capVersionSubject.stream;

  // Getter to Observables
  Stream<bool?> get authDidChangedStream => _authDidChangedSubject.stream;

  Stream<int?> get getDeviceBatteryLevelStream => _deviceBatteryLevelSubject.stream;

  Stream<List<DeviceSnapshot>> get getDeviceSnapshotStream => _deviceSnapshotSubject.stream;

  Stream<ConnectionSnapshot> get getConnectionSnapshotStream => _connectionSnapshotSubject.stream;

  int? get lastBatteryLevel => _deviceBatteryLevelSubject.value;

  final OTAEvent _otaManager = OTAEvent();

  Stream<OTAStateEvent> get getOtaStateStream => _otaManager.state;

  String? get getAppType => networkInvoker.appType;

  int? get getUserId => networkInvoker.userId;

  int? get getUserScore => networkInvoker.userScore;

  String? get getScoreName => networkInvoker.scoreName;

  bool isDeviceConnectedNow = false;

  String? get getDeviceMacAddress => _deviceScanOpsInvoker.macAddress;

  String? get getCapVersion => _deviceScanOpsInvoker.capVersion;

  bool get isAttachedToDevice => _deviceScanOpsInvoker.macAddress?.isNotEmpty ?? false;

  int? get lastDeviceSyncTimestamp => _deviceScanOpsInvoker.lastSyncDeviceTimestamp;

  Completer _isAppInitialledCompleter = Completer();

  Future get isAppInitialledFuture => _isAppInitialledCompleter.future;

  SdkBloc({
    this.applySilentLogin = false,
    AppGenes? appGenes,
    bool includePushNotifications = false,
    bool includeLocalReminders = false,
    AppMode appMode = AppMode.FOREGROUND_ONLY,
  }) {
    configPluginSepcificConfigurationAndInitListners(
      appGenes,
      includePushNotifications,
      includeLocalReminders,
      appMode,
    );
  }

  void generateFakeDeviceEvent(DeviceSnapshot snapshot) {
    _deviceSnapshotSubject.sink.add([snapshot]);
  }

  void generateFakeDeviceEventArray(List<DeviceSnapshot> snapshot) {
    _deviceSnapshotSubject.sink.add(snapshot);
  }

  void configPluginSepcificConfigurationAndInitListners(
    AppGenes? appGenes,
    bool withPushNotif,
    bool withLocalRem,
    AppMode appModeRunning,
  ) async {
    final configInvoker = PluginConfigInvoker(
      appGenes: appGenes,
      includePushNotifications: withPushNotif,
      includeLocalReminders: withLocalRem,
      appMode: appModeRunning,
    );
    await configInvoker.configPlufinConfigurations();
    await initBeforeSuperInit();
    initAllInvokers();
    initChannelsListeners();
    _isAppInitialledCompleter.complete();
    commitSilentLoginIfNeeded();
    if (appModeRunning == AppMode.FOREGROUND_AND_BACKGROUND) {
      syncEventsFromBackgroundIfAny();
    }
    syncBatteryStatusFromLog();
    _listenToCapConfigurations();
  }

  void syncBatteryStatusFromLog() {
    getDeviceSnapshotStream
        //.where((capState) =>
        //  capState.extraData2 == null && capState.eventType == 'A')
        .listen((e) {
      for (var capState in e) {
        if (capState.extraData2 == null && capState.eventType == 'A') {
          if (Platform.isIOS) {
            _deviceBatteryLevelSubject.sink.add(capState.measurement);
          } else {
            _deviceBatteryLevelSubject.sink.add(capState.extraData);
          }
        }
      }
    });
  }

  void initAllInvokers() {
    _deviceScanOpsInvoker = DeviceScanOperationsInvoker(onDeviceDataUpdate: onDeviceDataUpdate);
    _authInvoker = AuthInvoker(() => onUserAuthSuccess(true));
    _permissionsInvoker = PermissionsInvoker();
    _localPushNotificationsInvoker = LocalPushNotificationsInvoker();
    _backgroundInvoker = BackgroundInvoker();
    _pluginMessengerInvoker = PluginMessengerInvoker();
  }

  void onDeviceDataUpdate() {
    if (_deviceScanOpsInvoker.macAddress == null || _deviceScanOpsInvoker.macAddress!.length < 2) {
      _connectionSnapshotSubject.sink.add(ConnectionSnapshot.empty());
    }

    // _capVersionSubject.sink.add(_deviceScanOpsInvoker.capVersion);
  }

  void onUserAuthSuccess(bool isFirstLogin) {}

  Future initBeforeSuperInit() => Future.value(0);

  void commitSilentLoginIfNeeded() async {
    await networkInvoker.networkCompleterFuture;

    if (applySilentLogin && getUserId == null) {
      _authInvoker.commitSilentLogin();
    } else {
      onUserAuthSuccess(false);
    }
  }

  Future refreshCapVersion() async {
    await _deviceScanOpsInvoker.retrieveDeviceData();
    //_capVersionSubject.sink.add(_deviceScanOpsInvoker.capVersion);
    print('New version ${_deviceScanOpsInvoker.capVersion}');
    return null;
  }

  void _listenToCapConfigurations() {
    CapConfigurationsObserver((p0) => _capConfigurationChangedSubject.sink.add(p0));
  }

  void startListenToUrlCallbackEvent() {
    _userClickOnUrlSubject.sink.add('');
    networkObserver = NetworkObserver();
    networkObserver!.onUserClickOnUrlCallback = _onUserLinkWasClicked;
  }

  void _onUserLinkWasClicked(String? link) {
    networkObserver = null;
    _userClickOnUrlSubject.sink.add(link);
  }

  void initChannelsListeners() {
    CapStateObserver(_updateCapState, (value) {
      return _deviceBatteryLevelSubject.sink.add(value);
    }, (String? version) async {
      _capVersionSubject.sink.add(version);
      await Future.delayed(Duration(milliseconds: 500));
      _deviceScanOpsInvoker.retrieveDeviceData();
    });

    CapConnectionObserver((ConnectionSnapshot snapshot) {
      isDeviceConnectedNow = snapshot.isConnected == true;
      _updateConnectionState(snapshot);
    });
    PushNotificationObserver((value) => _pushNotificationSnapshotSubject.sink.add(value),
        (value) => _pushNotificationTokenRegisteredSnapshotSubject.sink.add(value));
    AuthObserver(onUserAuthenticated: (bool auth) => _authDidChangedSubject.sink.add(auth));
  }

  void syncEventsFromBackgroundIfAny() async {
    List<DeviceSnapshot> missedEvents = await _backgroundInvoker.retrieveEventsFromBackground();

    if (missedEvents.isEmpty) {
      return;
    }

    // for (DeviceSnapshot snapshot in missedEvents) {
    //   _updateCapState(snapshot);
    // }

    _updateCapState(missedEvents);
  }

  // returned error string - null if all ok..
  Future<String?> startScanningForDevices(bool autoHandlePermissions) async {
    String? scanError = await _deviceScanOpsInvoker.startScan();

    if (scanError == null) {
      return null;
    }

    if (!autoHandlePermissions) {
      return scanError;
    }

    if (scanError.contains('CHUNK_PERMISSIONS_ARE_MISSING')) {
      bool permissionsGranted = await askForChunkedPermissions();

      if (!permissionsGranted) {
        return scanError;
      }
    } else if (scanError.contains('MODULE_IS_OFF')) {
      String? modulsStatus = await getAdaptersStatus();
      switch (modulsStatus) {
        case 'MISSING_GPS':
          bool isAllowed = await askForAdaptersPermissions('MISSING_GPS');
          if (!isAllowed) {
            _deviceScanOpsInvoker.registerStartScanWhenEnable();
            return null;
          }
          break;
        case 'MISSING_BLUETOOTH':
          bool isAllowed = await askForAdaptersPermissions('MISSING_BLUETOOTH');
          if (!isAllowed) {
            _deviceScanOpsInvoker.registerStartScanWhenEnable();
            return null;
          }
          break;
        case 'SUCCESS':
          return scanError;
      }
    } else if (scanError.contains('CHUNK_PERMISSIONS_ARE_DENIED')) {
      // bool permissionsGranted = await askForDeniedChunkedPermissions();

      // if (!permissionsGranted) {
      return scanError;
      // }
    }

    String? recoursive = await startScanningForDevices(autoHandlePermissions);

    return recoursive;
  }

  setPauseUseCap(bool isPause) {
    if (isPause) {
      _deviceBatteryLevelSubject.value = null;
      _capVersionSubject.value = null;
      _deviceScanOpsInvoker.capVersion = null;
      _deviceScanOpsInvoker.macAddress = null;
    }

    if (Platform.isIOS) {
      _deviceScanOpsInvoker.setPauseUseCap(isPause);
    } else {
      //TODO Daniel finish it in Android
    }
  }

  void forgetCap() {
    _deviceScanOpsInvoker.forgetCap();
    _deviceBatteryLevelSubject.value = null;
    _capVersionSubject.value = null;
    _deviceScanOpsInvoker.capVersion = null;
    _deviceScanOpsInvoker.macAddress = null;
  }

  void stopScan() {
    // good for both - wizard and normal scan :)
    _deviceScanOpsInvoker.stopScan();
  }

  Future<String?> startWizardScanningForDevices(
      ProcedureType procedureType, Function(String?, bool) onNewDeviceConnection, bool autoHandlePermissions) async {
    String? scanError = await _deviceScanOpsInvoker.startWizardScan(procedureType, onNewDeviceConnection);

    if (scanError == null) {
      return null;
    }

    if (!autoHandlePermissions) {
      return scanError;
    }

    if (scanError.contains('CHUNK_PERMISSIONS_ARE_MISSING')) {
      bool permissionsGranted = await askForChunkedPermissions();

      if (!permissionsGranted) {
        return scanError;
      }
    } else if (scanError.contains('MODULE_IS_OFF')) {
      String? modulsStatus = await getAdaptersStatus();
      switch (modulsStatus) {
        case 'MISSING_GPS':
          await askForAdaptersPermissions('MISSING_GPS');
          break;
        case 'MISSING_BLUETOOTH':
          await askForAdaptersPermissions('MISSING_BLUETOOTH');
          break;

        case 'SUCCESS':
          return scanError;
      }
    } else if (scanError.contains('CHUNK_PERMISSIONS_ARE_DENIED')) {
      print('scan $scanError');
      return scanError;
    }

    String? recoursive =
        await startWizardScanningForDevices(procedureType, onNewDeviceConnection, autoHandlePermissions);

    return recoursive;
  }

  Future<void> wizardScanYesMyCap() {
    return _deviceScanOpsInvoker.wizardScanYesMyCap();
  }

  void simulateLedPattern(String hexColor, int pattern) {
    return _deviceScanOpsInvoker.simulateLedPattern(hexColor, pattern);
  }

  void wizardScanNotMyCap(bool isAddToBlackList) {
    _deviceScanOpsInvoker.wizardScanNotMyCap(isAddToBlackList);
  }

  Future<bool> setCalibration(int value) async {
    return _deviceScanOpsInvoker.setCalibration(value);
  }

  void reportUserLog(Map<String, dynamic> report) {
    networkInvoker.reportUserLog(report);
  }

  void reportUserCrash(Map<String, dynamic> crash) {
    networkInvoker.reportUserCrash(crash);
  }

  void reportSupportMsg(Map<String, dynamic> report) {
    networkInvoker.reportSupportMsg(report);
  }

  Future<dynamic> getBadges() async {
    return await networkInvoker.getBadges();
  }

  Future<String?> deleteUserAccount() async {
    return await networkInvoker.deleteUserAccount();
  }

  Future<bool> requestUserNotificationsOrBatteryOptPermissions() {
    return _permissionsInvoker!.requestUserNotificationsOrBatteryOptPermissions();
  }

  void cancelLocalPushOnce(Reminder reminder) {
    _localPushNotificationsInvoker.cancelLocalPushOnce(reminder);
  }

  Future<bool> askForChunkedPermissions() {
    return _permissionsInvoker!.requestChunkedPermissions();
  }

  Future<String?> getAdaptersStatus() {
    return _permissionsInvoker!.getAdaptersStatus();
  }

  Future<dynamic> askForAdaptersPermissions(String adaptersRequest) {
    return _permissionsInvoker!.requestAdaptersPermissions(adaptersRequest);
  }

  void fakePushNotificaiton(Map data) {
    _pushNotificationSnapshotSubject.sink.add(data);
  }

  Future<bool> requestBluetoothPermissions() async {
    return _permissionsInvoker!.requestBluetoothPermissions();
  }

  Future<dynamic> requestLocationPermissions() {
    return _permissionsInvoker!.requestLocationPermissions();
  }

  Future<bool> askForDeniedChunkedPermissions() {
    return _permissionsInvoker!.requestDeniedChunkedPermissions();
  }

  Future<String?> retrieveAppPrivacyPolicyUrl(bool isHeb) {
    return networkInvoker.retrieveAppPrivacyPolicyUrl(isHeb);
  }

  Future<String?> retrieveAppTermsAndConditionsUrl(bool isHeb) {
    return networkInvoker.retrieveAppTermsAndConditionsUrl(isHeb);
  }

  Future<Map<String, dynamic>?> retrieveAppConfiguration() {
    return networkInvoker.retrieveAppConfiguration();
  }

  Future<String?> retrieveAppTermsAndConditionsVersion() {
    return networkInvoker.retrieveAppTermsAndConditionsVersion();
  }

  Future<int> getLatestAppVersion() {
    return networkInvoker.getLatestAppVersion();
  }

  Future<int> getCalibrationFromFt2Result(String macAddress) async {
    return await networkInvoker.getCalibrationFromFt2Result(macAddress);
  }

  Future<String?> fetchBottlesList() {
    return networkInvoker.fetchBottlesList();
  }

  Future<Map?> getTutorialLinks() async {
    final String? request = await networkInvoker.getTutorialLinks();

    if (request == null) {
      return null;
    }
    final map = json.decode(request);
    return map;
  }

  Future<String?> appToAppAuth(Map<String, String> parameters) async {
    final String? response = await networkInvoker.appToAppAuth(parameters);
    if (response == null) {
      return null;
    }
    return response;
  }

  Future<int?>? retrieveLastDeviceSyncTimestamp() {
    if (!isAttachedToDevice) {
      return null;
    }

    return _deviceScanOpsInvoker.retrieveLastDeviceSyncTimestamp();
  }

  Future<bool?> isNotificationTokenUpdated() async {
    return RemoteNotificationInvoker().isNotificationTokenUpdated();
  }

  Future<Map?> getLastOTACapVersion() async {
    if (!isAttachedToDevice || getCapVersion!.length < 2) {
      return null;
    }

    final response = await networkInvoker.getLastOTACapVersion(getCapVersion);

    if (response == null) {
      return null;
    }
    final map = json.decode(response);

    return map;
  }

  Future<String?> getLastOTACapVersionInBoot(String? bootVersion, String? hardwareVersion) async {
    if (!isAttachedToDevice) {
      return Future.value(null);
    }

    return networkInvoker.getLastOTACapVersionInBoot(bootVersion, hardwareVersion);
  }

  Future<Map?> getFeed(int pageSize, {String? idTokenBefore, String? idTokenAfter}) async {
    final request = await networkInvoker.getFeed(pageSize, idTokenBefore: idTokenBefore, idTokenAfter: idTokenAfter);

    if (request == null) {
      return null;
    }
    final map = json.decode(request);
    return map;
  }

  Future<bool?> disconnectWeatherCity() async {
    return networkInvoker.disconnectWeatherCity();
  }

  Future<bool?> isScaleEnable() async {
    return networkInvoker.isScaleEnable();
  }

  Future<void> resendFailedLogs() async {
    return networkInvoker.resendFailedLogs();
  }

  Future<bool?> startOTA(String version) async {
    if (!isAttachedToDevice) {
      return false;
    }

    final _otaInvoker = OTAInvoker();
    return await _otaInvoker.startOTA(version);
  }

  /// The function made to iOS to know status of adapters & permission status
  /// Response status is
  /// [adapterIsOff] - User turn off the location adapter on device
  /// [notDetermined] - User has not yet made a choice with regards to this application
  /// [restricted] - // This application is not authorized to use location services.  Due
  /// to active restrictions on location services, the user cannot change
  /// this status, and may not have personally denied authorization
  /// [denied] - User has explicitly denied authorization for this application, or
  /// location services are disabled in Settings.
  /// [authorizedAlways] - authorizedAlways
  /// [authorizedWhenInUse] - authorizedWhenInUse
  /// [unknown] - unknown
  Future<String?> getLocationStatus() async {
    if (_permissionsInvoker != null) {
      return _permissionsInvoker!.getLocationStatus();
    } else {
      _permissionsInvoker = PermissionsInvoker();
      return _permissionsInvoker!.getLocationStatus();
    }
  }

  // PATTERN
  // REMINDER COLOR
  // UI ELEMENT
  // DAR
  // Daily goal
  // Future<Map?> getLastCapConfigurations() async {
  //   try {
  //     final String? request = await _deviceScanOpsInvoker.getLastCapConfigurations();
  //     if (request == null) {
  //       return null;
  //     }
  //     final map = json.decode(request);
  //     return map;
  //   } catch (e) {
  //     print(e);
  //     return null;
  //   }
  // }

  Future<List?> getLastDbHydrations(int index) async {
    try {
      final request = await _deviceScanOpsInvoker.getLastDbHydrations(index);
      if (request == null) {
        return null;
      }
      final List list = json.decode(request);
      return list;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<bool?> startBootOTA() async {
    final _otaInvoker = OTAInvoker();
    return await _otaInvoker.startOTAInBoot();
  }

  Future<bool?> reportUserLocation(double lat, double lng) async {
    return networkInvoker.reportUserLocation(lat, lng);
  }

  /// [json] look like this
  /// {
  /// "hydration_list": [
  /// { "drink_type": "wine", "amount": 500, "event_time": 1682237494275 },
  /// { "drink_type": "wine",     "amount": 500,     "event_time": 1682237494295 }]
  /// }
  Future<Map?> addMultiExtraHydration(Map json) async {
    final res = await networkInvoker.addMultiExtraHydration(json);
    _calendarChangeSubject.sink.add(-1);
    return res;
  }

  Future<Map?> getLastUserLocation() async {
    return networkInvoker.getLastUserLocation();
  }

  Future<Map?> deleteExtraHydration(int eventTime, String drinkType) async {
    final res = await networkInvoker.deleteExtraHydration(eventTime, drinkType);
    _calendarChangeSubject.sink.add(eventTime);
    return res;
  }

  Future<String?>? fetchFilteredCalibrationTable() {
    if (!isAttachedToDevice) {
      return null;
    }

    return networkInvoker.fetchCalibrationTable(getCapVersion!.substring(getCapVersion!.length - 2));
  }

  Future<String?> fetchRemoteCalibrationTable(String remoteBottleKey) {
    if (remoteBottleKey == null) {
      return Future.value(null);
    }

    return networkInvoker.fetchRemoteCalibrationTable(remoteBottleKey);
  }

  Future<Map?> getSportActivities(int pageSize, int pageIndex, int startTime) async {
    try {
      final request = await networkInvoker.getSportActivities(pageSize, pageIndex, startTime);
      if (request == null) {
        return null;
      }
      final map = json.decode(request);
      return map;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Map?> getHydration(int pageSize, int pageIndex, int startTime) async {
    try {
      // final request = await networkInvoker.getHydration(pageSize, pageIndex, startTime);
      // if (request == null) {
      //   return null;
      // }
      // final map = json.decode(request);
      return demoHydrations;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Map?> addSportActivity(Map data) async {
    final request = await networkInvoker.addSportActivity(data);
    if (request == null) {
      return null;
    }
    final map = json.decode(request);
    final startTime = data['start_time_in_seconds'];
    if (startTime != null && startTime is int) {
      _calendarChangeSubject.sink.add(startTime * 1000);
    }
    return map;
  }

  Future<Map?> addMultiSportActivity(Map data) async {
    final request = await networkInvoker.addMultiSportActivity(data);
    if (request == null) {
      return null;
    }
    final map = json.decode(request);
    return map;
  }

  Future<Map?> updateSportActivity(String id, Map data) async {
    final request = await networkInvoker.updateSportActivity(id, data);
    if (request == null) {
      return null;
    }
    final map = json.decode(request);
    return map;
  }

  Future<bool?> uploadEventToFirebase(Map data) async {
    final event = await networkInvoker.uploadEventToFirebase(data);
    if (event == null) {
      return null;
    }
    return event;
  }

  Future<Map?> deleteSportActivity(String id) async {
    final request = await networkInvoker.deleteSportActivity(id);
    if (request == null) {
      return null;
    }
    _calendarChangeSubject.sink.add(-1);
    final map = json.decode(request);
    return map;
  }

  Future<Map?> getAppConfiguration() async {
    return networkInvoker.getAppConfiguration();
  }

  Future<Map?> deleteAllTodaySportActivity() async {
    try {
      final request = await networkInvoker.deleteAllTodaySportActivity();
      if (request == null) {
        return null;
      }
      final map = json.decode(request);
      return map;
    } catch (e) {
      print(e);
      return null;
    }
  }

  /// [pageSize] page amount
  /// [pageIndex] for paginagain
  /// [messageType] options is: APP_REMINDER DAILY_WEATHER DEFAULT MONTHLY_WEATHER UPDATE_DAILY_GOAL WEEKLY_REPORT

  Future<Map?> getNotificationsList(int pageSize, int pageIndex,
      {String? messageType, int? minimumTime, bool? includeSilent = false}) async {
    final String? request = await networkInvoker.getNotificationsList(pageSize, pageIndex,
        messageType: messageType, minimumTime: minimumTime, includeSilent: includeSilent);
    if (request == null) {
      return null;
    }

    final map = json.decode(request);
    return map;
  }

  void setStringInPluginStorage(String key, String value) {
    _pluginMessengerInvoker.setStringInPluginStorage(key, value);
  }

  void retrieveUserScore() {
    networkInvoker.retrieveUserScore();
  }

  void reportUserHydration({
    String? eventSource,
    int? amount,
    int? timestamp,
    int? waterLevelStart,
    int? waterLevelEnd,
    String? drinkType,
  }) {
    networkInvoker.reportUserHydration(eventSource, amount, timestamp, waterLevelStart, waterLevelEnd, drinkType);
  }

  Future<Map?> getWeeklyReport(int fromDate, int toDate) async {
    return networkInvoker.getWeeklyReport(fromDate, toDate);
  }

  Future<Map?> updateUserProfileInServerByJson({required Map<String, dynamic> argObj}) async {
    final String? response = await _authInvoker.updateUserProfileInServerByJson(argObj: argObj);

    if (response != null) {
      final Map? map = json.decode(response);
      return map;
    }

    return null;
  }

  Future<Map?> updateUserProfileInServer({
    String? email,
    String? nickName,
    String? firstName,
    int? age,
    required double weight,
    required double height,
    String? gender,
    String? phone,
    String? extra_data_1,
  }) async {
    final String? response = await _authInvoker.updateUserProfileInServer(
      email: email,
      nickName: nickName,
      firstName: firstName,
      age: age,
      weight: weight,
      height: height,
      gender: gender,
      phone: phone,
      extra_data_1: extra_data_1,
    );

    if (response != null) {
      final Map? map = json.decode(response);
      return map;
    }

    return null;
  }

  void commitBlinkSequence(int type) {
    if (!isAttachedToDevice || getCapVersion!.length < 2) {
      return;
    }
    final newType = blinkTypeFromInt(type);
    _deviceScanOpsInvoker.commitBlinkSequence(newType);
  }

  Future<int?> getBaseGoal() async {
    return _deviceScanOpsInvoker.getBaseGoal();
  }

  Future<bool> setManuallyHydration(int amount) async {
    return _deviceScanOpsInvoker.setManuallyHydration(amount);
  }

  Future<bool?> setExtraDailyGoal(int goal) async {
    return _deviceScanOpsInvoker.setExtraDailyGoal(goal);
  }

  Future<bool> setHydrationLevel(int amount) async {
    return _deviceScanOpsInvoker.setHydrationLevel(amount);
  }

  Future<Map?> getCapHydration() async {
    final String? hydration = await _deviceScanOpsInvoker.getCapHydration();

    if (hydration == null) {
      return null;
    }
    final Map? map = json.decode(hydration);

    return map;
  }

  Future<int?> getExtraDailyGoal() async {
    return _deviceScanOpsInvoker.getExtraDailyGoal();
  }

  void setRemindersOnDevice(List<Reminder> remindersList) {
    _deviceScanOpsInvoker.setRemindersOnDevice(remindersList);
  }

  void setLedColor(String hexColor, int duration) {
    _deviceScanOpsInvoker.setLedColor(hexColor, duration);
  }

  void setResetBootState() {
    final ota = OTAInvoker();
    ota.resetBootState();
  }

  Future<bool> simulateVibration(int duration) async {
    return await _deviceScanOpsInvoker.simulateVibration(duration);
  }

  void setCapDisconnect() {
    _deviceScanOpsInvoker.setCapDisconnect();
  }

  Future<bool> runSingleMeasurement() async {
    return await _deviceScanOpsInvoker.runSingleMeasurement();
  }

  Future<bool> registerNewUser(String email, String? password) async {
    final isSucsess = await _authInvoker.registerNewUser(email, password);

    if (isSucsess) {
      await networkInvoker.retrieveUserId();
    }
    return isSucsess;
  }

  Future<String> loginExistingUser(String email, String? password) async {
    final result = await _authInvoker.loginExistingUser(email, password);

    if (result == 'SUCCESS') {
      await networkInvoker.retrieveUserId();
    }
    return result;
  }

  Future<bool> logout() async {
    return await _authInvoker.logout();
  }

  Future<bool> loginWithSocial(Map<String, dynamic> jsonData) async {
    final String? data = await _authInvoker.loginWithSocial(jsonData);

    if (data == null) {
      return false;
    }
    await networkInvoker.retrieveUserId();
    return networkInvoker.userId != null;
  }

  Future<String?> getCurrentEnvironment() async {
    final String? result = await networkInvoker.getCurrentEnvironment();
    return result;
  }

  Future<String?> shareInFacebook(String imagePath) async {
    final String? result = await networkInvoker.shareInFacebook(imagePath);
    return result;
  }

  Future<String?> shareInWhatsApp(String msg, String imagePath) async {
    Map json = {'msg': msg, 'imagePath': imagePath, 'isWhatsAppBusiness': false};
    String? result = await networkInvoker.shareInWhatsApp(json);
    if (result != null && result.contains('No Activity found')) {
      json['isWhatsAppBusiness'] = true;
      result = await networkInvoker.shareInWhatsApp(json);
    }
    return result;
  }

  Future<Map?> restoreUserDataMigration() async {
    final String? data = await networkInvoker.restoreUserDataMigration();

    if (data == null) {
      return null;
    }
    final Map? map = json.decode(data);
    return map;
  }

  Future<Map?> restoreUserData() async {
    final String? data = await networkInvoker.restoreUserData();

    if (data == null) {
      return null;
    }
    final Map? map = json.decode(data);
    return map;
  }

  Future<bool> storeUserData(Map json) async {
    final String? hasSaved = await networkInvoker.storeUserData(json);
    if (hasSaved != null) {
      return true;
    }
    return false;
  }

  Future<bool> forgetPassword(String email) async {
    return _authInvoker.forgetPassword(email);
  }

  // void setCapReminderColor(String hexColor) {
  //   _deviceScanOpsInvoker.setCapReminderColor(hexColor);
  // }

  // void setCapDailyGoal(int goal) {
  //   _deviceScanOpsInvoker.setCapDailyGoal(goal);
  // }

  void setCapManuallyHydration(double amount) {
    _deviceScanOpsInvoker.setCapManuallyHydration(amount);
  }

  // void setVibration(bool isOn, bool isAutoDisconnect) {
  //   _deviceScanOpsInvoker.setVibration(isOn);
  // }

  void setSilentMode(bool isOn) {
    _deviceScanOpsInvoker.setSilentMode(isOn);
  }

  void updateDailyUsageTime(int fromHour, int fromMinutes, int toHour, int toMinutes, bool isConfiguration) {
    _deviceScanOpsInvoker.updateDailyUsageTime(fromHour, fromMinutes, toHour, toMinutes, isConfiguration);
  }

  Future<bool?> setMultiConfiguration(Map configuration) async {
    return _deviceScanOpsInvoker.setMultiConfiguration(configuration);
  }

  Future<Map?> getMultiConfigurations() async {
    final String? data = await _deviceScanOpsInvoker.getMultiConfigurations();

    if (data == null) {
      return null;
    }
    final Map? map = json.decode(data);

    return map;
  }

  void setLocalRemindersPushNotifications(List<Reminder> remindersList) {
    _localPushNotificationsInvoker.setLocalPushsReminders(remindersList);
  }

  void setLocalPushsRandomMsgs(String title, List<String> msgs) {
    _localPushNotificationsInvoker.setLocalPushsRandomMsgs(title, msgs);
  }

  void reportScreenChange(String screenName) {
    networkInvoker.reportScreenChange(screenName);
  }

  void reportAttributes(String trigger) {
    networkInvoker.reportAttributes(trigger);
  }

  void updateDeviceRssiConfig(bool activate, int rssi) {
    _deviceScanOpsInvoker.updateDeviceRssiConfig(activate, rssi);
  }

  void sendLogsFile() {
    networkInvoker.sendLogsFile();
  }

  void clearAllBackgroundEventsDb() {
    _backgroundInvoker.clearAllBackgroundEventsDb();
  }

  Future<bool> checkCapSync(int baseGoal, int extraDailyGoal, int manGoal, int hydrationMeasurement) async {
    final bool isSuccuss =
        await _deviceScanOpsInvoker.checkCapSync(baseGoal, extraDailyGoal, manGoal, hydrationMeasurement);
    return isSuccuss;
  }

  Future<bool> restorePairedToCap(String macAddrees) async {
    return await _deviceScanOpsInvoker.restorePairedToCap(macAddrees);
  }

  Future<bool> isLocationPermissionDenied() async {
    if (_permissionsInvoker != null) {
      return await _permissionsInvoker!.isLocationPermissionDenied();
    } else {
      _permissionsInvoker = PermissionsInvoker();
      return _permissionsInvoker!.isLocationPermissionDenied();
    }
  }

  Future<bool> isBluetoothPermissionDenied() {
    if (_permissionsInvoker != null) {
      return _permissionsInvoker!.isBluetoothPermissionDenied();
    } else {
      _permissionsInvoker = PermissionsInvoker();
      return _permissionsInvoker!.isBluetoothPermissionDenied();
    }
  }

  Future<String?> getStatusPermissionOnly() {
    if (_permissionsInvoker != null) {
      return _permissionsInvoker!.getStatusPermissionOnly();
    } else {
      _permissionsInvoker = PermissionsInvoker();
      return _permissionsInvoker!.getStatusPermissionOnly();
    }
  }

  Future<String?> checkForMissingPermissions() {
    return _permissionsInvoker!.checkForMissingPermissions();
  }

  Future<String?> getDrsHostCodeFromServer(String redirectUrl, String clientId) {
    return networkInvoker.getDrsHostCodeFromServer(redirectUrl, clientId);
  }

  Future<bool?> doesDrsUserSkillEnabled() {
    return networkInvoker.doesDrsUserSkillEnabled();
  }

  Future<bool> setDrsProcessData(Map<String, dynamic> data) {
    return networkInvoker.setDrsProcessData(data);
  }

  Future<bool?> requestLocationAlwaysPermissions() {
    return _permissionsInvoker!.requestLocationAlwaysPermissions();
  }

  Future<bool?> openApplicationsSettings() {
    return _permissionsInvoker!.openAppSettings();
  }

  Future<int?> getUserDailyGoal() async {
    return networkInvoker.getUserDailyGoal();
  }

  Future<bool?> isBlue() {
    return _permissionsInvoker!.wasTheUserAskedBeforeAboutLocationPremission();
  }

  Future<bool?> wasTheUserAskedBeforeAboutLocationPremission() {
    return _permissionsInvoker!.wasTheUserAskedBeforeAboutLocationPremission();
  }

  Future<bool> isNotificationCanShow() {
    return _permissionsInvoker!.isNotificationCanShow();
  }

  Future<bool?> requestNotificationPermission() {
    return _permissionsInvoker!.requestNotificationPermission();
  }

  Future<bool?> isNotificationEnable() {
    return _permissionsInvoker!.isNotificationEnable();
  }

  Future<bool?> isBluetoothAllowedAndroid12() {
    return _permissionsInvoker!.isBluetoothAllowedAndroid12();
  }

  Future<String?> showSettingScreen() async {
    return await _permissionsInvoker!.openAppPermissionSettings();
  }

  Future<String?> getEncouragementSentence() async {
    return networkInvoker.getEncouragementSentence();
  }

  Future<String?> getCalendarDailyGoal(String date) async {
    final offset = DateTime.now().timeZoneOffset.inHours.toString();
    return networkInvoker.getCalendarDailyGoal(date, offset);
  }

  Future<List?> getAllWidgetData() async {
    return networkInvoker.getAllWidgetData();
  }

  Future<String?> getDailyHydration(String date) async {
    final offset = DateTime.now().timeZoneOffset.inHours.toString();
    return networkInvoker.getDailyHydration(date, offset);
  }

  Future<String?> setNotificationReminder(String type, String interval) async {
    return networkInvoker.setNotificationReminder(type, interval);
  }

  Future<String?> setTimeRangeHydrationDay(String startDay, String endDay) async {
    return networkInvoker.setTimeRangeHydrationDay(startDay, endDay);
  }

  Future<Map?> setManualDailyGoal(bool isManual, int goal) async {
    final String? goalMap = await networkInvoker.setManualDailyGoal(isManual, goal);
    if (goalMap == null) {
      return null;
    }
    final map = json.decode(goalMap);
    return map;
  }

  Future<String?> getUrlLoginWithGarmin() async {
    return networkInvoker.getUrlLoginWithGarmin();
  }

  Future<String?> getUrlLoginWithStrava() async {
    return networkInvoker.getUrlLoginWithStrava();
  }

  Future<String?> getUrlLoginWithWhoop() async {
    return networkInvoker.getUrlLoginWithWhoop();
  }

  Future<String?> getUrlLoginWithOura() async {
    return networkInvoker.getUrlLoginWithOura();
  }

  Future<bool?> disconnectFromExternalService(String service) async {
    return networkInvoker.disconnectFromExternalService(service);
  }

  Future<Map?> getExternalStatusService() async {
    final request = await networkInvoker.getExternalStatusService();
    final map = json.decode(request ?? "");
    return map;
  }

  Future<Map?> getDailyHydrationGoal() async {
    final response = await networkInvoker.getDailyHydrationGoal();
    if (response == null) {
      return null;
    }
    final map = json.decode(response);
    return map;
  }

  Future<Map?> getReferAFriendCoupon() async {
    return networkInvoker.getReferAFriendCoupon();
  }

  Future<bool> sendCouponToMail(String email) async {
    return await networkInvoker.sendCouponToMail(email);
  }

  Future<Map?> getTotalUsages() async {
    return networkInvoker.getTotalUsages();
  }

  Future<Map?> getDailyHydrationGoalDetails(String type) async {
    final offset = DateTime.now().timeZoneOffset.inHours.toString();
    final request = await networkInvoker.getDailyHydrationGoalDetails(type, offset);
    if (request == null) {
      return null;
    }
    final map = json.decode(request);
    return map;
  }

  Future<String?> getCityWeatherByGps(double lat, double lng) async {
    return networkInvoker.getCityByGps(lat, lng);
  }

  Future<Map?> getCityWeatherManually(String placeId) async {
    final String? response = await networkInvoker.getCityWeatherManually(placeId);
    final Map? map = response == null ? null : json.decode(response);
    return map;
  }

  Future<Map?> getAutoCompletePlace(String place, String token) async {
    final request = await networkInvoker.getAutoCompletePlace(place, token);
    final map = request == null ? {} : json.decode(request);
    return map;
  }

  Future<Map?> getUserLocation() async {
    try {
      final request = await networkInvoker.getUserLocation();
      return json.decode(request ?? "");
    } catch (e) {
      print('getUserLocation $e');
    }
    return null;
  }

  void clearAppData() {
    _backgroundInvoker.clearAppData();
  }

  Future<bool> startScanScale() async {
    return await BottleScaleInvoker().startScan() == true;
  }

  Future<bool> stopScanScale() async {
    return await BottleScaleInvoker().stopScan() == true;
  }

  void listenToBottleScaleEvent(Function(ScaleBottleState) state, Function(ScaleBottleValue) value) async {
    BottleScaleObserver(state, value);
  }

  void resetAppData() {
    // Add
    _pushNotificationSnapshotSubject.value = null;
    _deviceBatteryLevelSubject.value = null;
    _connectionSnapshotSubject.value = ConnectionSnapshot.empty();
    _capVersionSubject.value = null;
    _authDidChangedSubject.value = null;
    _deviceScanOpsInvoker.capVersion = null;
    _deviceScanOpsInvoker.lastSyncDeviceTimestamp = null;
    _deviceScanOpsInvoker.macAddress = null;
    networkInvoker.userId = null;
    networkInvoker.userScore = null;
    networkInvoker.scoreName = null;
    _capConfigurationChangedSubject.value = null;
    _pushNotificationTokenRegisteredSnapshotSubject.value = null;
  }

  dispose() {
    _pushNotificationSnapshotSubject.close();
    _deviceSnapshotSubject.close();
    _connectionSnapshotSubject.close();
    _deviceBatteryLevelSubject.close();
    _capVersionSubject.close();
    _userClickOnUrlSubject.close();
    _authDidChangedSubject.close();
    _calendarChangeSubject.close();
    _capConfigurationChangedSubject.close();
    _pushNotificationTokenRegisteredSnapshotSubject.close();
  }
}

class DeviceSnapshot {
  final String? eventType;
  final int? timestamp;
  final int? measurement;
  final String? capMacAddr;
  final int? extraData;
  final String? extraData2;
  final bool fromBackground;

  DeviceSnapshot(this.eventType, this.timestamp, this.measurement,
      this.capMacAddr, this.extraData, this.extraData2, this.fromBackground);

  DeviceSnapshot.fromMap(Map<String, dynamic> map)
      : this(
            map['event_type'],
            map['timestamp'],
            map['measurement'],
            map['cap_mac_address'],
            map['extra_data'],
            map['extra_data2'],
            map['fromBackground'] ?? false);

  @override
  String toString() {
    return '$eventType $timestamp $measurement $capMacAddr $extraData $extraData2 $fromBackground';
  }
}

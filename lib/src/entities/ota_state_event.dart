enum OTAState { START, END, UPLOADING, UNKNOW, CAP_STUCK_IN_BOOT }

class OTAStateEvent {
  final OTAState state;
  final dynamic value;
  final dynamic additionalValue;

  OTAStateEvent.fromJSON(Map json)
      : this.state = _mapString(json['state']),
        this.value = json['value']['value'],
        this.additionalValue = json['value']['additional_value'];

  String get name =>
      state.toString().split('.').last + (value > 0 ? ' $value' : '');

  static OTAState _mapString(String? state) {
    switch (state) {
      case 'START':
        return OTAState.START;
      case 'END':
        return OTAState.END;
      case 'UPLOADING':
        return OTAState.UPLOADING;
      case 'CAP_STUCK_IN_BOOT':
        return OTAState.CAP_STUCK_IN_BOOT;
    }
    return OTAState.UNKNOW;
  }
}

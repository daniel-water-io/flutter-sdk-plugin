class ConnectionSnapshot {
  final String? macAddress;
  final bool? isConnected;
  final int timestamp;

  ConnectionSnapshot(this.macAddress, this.isConnected, this.timestamp);

  ConnectionSnapshot.empty()
      : this(
          '',
          false,
          DateTime.now().millisecondsSinceEpoch,
        );

  ConnectionSnapshot.fromMap(Map<String, dynamic> map)
      : this(
          map['macAddress'],
          map['isConnected'],
          DateTime.now().millisecondsSinceEpoch,
        );

  @override
  String toString() {
    return '$macAddress isConnected - $isConnected ${DateTime.fromMillisecondsSinceEpoch(timestamp).toString()}';
  }
}

class ScaleBottleValue {
  final String mac;
  final int cid;
  final int vid;
  final int pid;
  final int weight;
  final int weightPoint;
  final String weightUnit;
  final int serialNumber;

  ScaleBottleValue(
    this.mac,
    this.cid,
    this.vid,
    this.pid,
    this.weight,
    this.weightPoint,
    this.weightUnit,
    this.serialNumber,
  );

  factory ScaleBottleValue.fromMap(Map map) {
    return ScaleBottleValue(
      map['mac'] ?? '',
      map['cid']?.toInt() ?? 0,
      map['vid']?.toInt() ?? 0,
      map['pid']?.toInt() ?? 0,
      map['weight']?.toInt() ?? 0,
      map['weightPoint']?.toInt() ?? 0,
      map['weightUnit'] ?? '',
      map['serialNumber'] ?? 0,
    );
  }
}

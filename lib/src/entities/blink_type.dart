enum BlinkType {
  DEFAULT,
  GOOD_STATUS,
  BAD_STATUS,
}

BlinkType blinkTypeFromInt(int index) {
  if (index == null) {
    return BlinkType.DEFAULT;
  } else if (index == 0) {
    return BlinkType.BAD_STATUS;
  } else if (index == 1) {
    return BlinkType.GOOD_STATUS;
  }
  return BlinkType.DEFAULT;
}

extension BlinkTypeExtension on BlinkType {
  int? get blinkIndex {
    switch (this) {
      case BlinkType.DEFAULT:
        return null;
      case BlinkType.BAD_STATUS:
        return 0;
      case BlinkType.GOOD_STATUS:
        return 1;
    }
    return null;
  }
}

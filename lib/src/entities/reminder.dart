class Reminder {
  final int hours;
  final int minutes;

  Reminder(this.hours, this.minutes);

  Map<String, int> toJson() {
    return {'hours': hours, 'minutes': minutes};
  }
}

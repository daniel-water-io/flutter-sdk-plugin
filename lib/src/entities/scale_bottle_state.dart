enum ScaleBottleState {
  unavailable,
  available,
  stopScan,
  scaning,
  willConnect,
  didConnect,
  connectFail,
  didDiscoverCharacteristics,
  didDisconnect,
  didValidationPass,
  failedValidation,
  unauthorized
}
